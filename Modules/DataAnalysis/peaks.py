#! /usr/bin/env python

import Gnuplot, sys

class discrete(object):

    def __init__(self, filename):
        self.filename = filename

    def findall(self, threshold, display=True, cols=(0,1)):
        if not 0.0 < threshold <= 1.0:
            raise PeaksError("Threshold must be on the interval (0, 1] ")
        else:
            self.thres = float(threshold)
        if len(cols) != 2:
            raise PeaksError("Two and only two columns can be specified")
        else:
            self.cols = cols

        #compute peaks (2 loops)
        noselect = True     #not selected
        nofirst = False     #not first loop
        while noselect:
            try:
                #Initial variables
                self.ymax = 0.0
                self.xmax = 0.0
                self.peaks = []

                #If the display is set and not the first time 
                #Get new threshold if not the first time
                if display and nofirst:
                    print("\nRe-Calculating peaks...")
                    self.thres = float(raw_input("Please Enter the Peak Threshold:"))
                    print("Peaks Threshold: {}".format(self.thres))

                #Main Computation, get peaks
                self.xmax, self.ymax = self._findmax()
                self._compare()

                #If Display is true, run gnuplot subroutines
                if display:
                    self._gnuplot(self.cols, self.peaks)

                    correct = raw_input("Is this Correct:")
                    if correct == 'Y' or correct == 'y':
                        noselect = False
                    else:
                        nofirst = True
                else:
                    noselect = False
            except  KeyboardInterrupt:
                print "Exiting peak finding..."
                sys.exit(0)

        return self.peaks[:]


    def _findmax(self):
        max_yval = 0.0
        max_xval = 0.0
        with open(self.filename, 'r') as f:
            for lines in f:
                x, y = self._splitvals(lines)
                if y > max_yval:
                    max_yval = y
                    max_xval = x
        return max_xval, max_yval

    def _compare(self):
        line_count = 0
        with open(self.filename, 'r') as f:
            for lines in f:
                x, y = self._splitvals(lines)
                line_count += 1
                if y >= self.thres*self.ymax:
                    self.peaks.append([x, y, line_count])

    def _splitvals(self, lines):
        temp = [float(i) for i in lines.strip().split()]
        x = temp[self.cols[0]]
        y = temp[self.cols[1]]
        return x, y

    def _gnuplot(self, cols, args):
        """
        Prints a gnuplot of the peaks and the file. 

        @parm: cols tuple
        @param: list of tuples
        """
        g = Gnuplot.Gnuplot(persist=1)
        plots = Gnuplot.File(self.filename, using='{}:{}'.format(cols[0]+1, cols[1]+1), \
                              with_='lp ls 7 pt 7 ps 0.5')

        g('set pointsize 2')
        g.plot(plots, args)

class PeaksError(Exception):
    
    def __init__(self, msg):
        print msg

