#! /usr/bin/env python

class dlist(list):
    """
    Implements a dynamic (perl-like) list data-type. This list will
    automatically create new indices when assigning to previously 
    non-existent indices. Provides an efficient way to average large 
    data files. Elements in-between new indices and old indices 
    will be filled with None.
    

    Note: Negative index access and assignment is allowed only only
    for preexisting indices
    """

    def __init__(self):
        list.__init__(self)

    def __setitem__(self, i ,v):

        #Highest list index
        self._length = len(self)
        if self._length == 0:
            self._index = -1 #Only set if list is undefined
        elif self._length > 0:
            self._index = self._length - 1

        #Special case, assigning to 0th element of a undefined list 
        if self._length == 0 and i == 0:
            self.append(v)
        #Positive indexes 
        elif i > 0:
        #Indexes above the highest index (fill in-between with None)
            if i > self._index:
                #Loop from one *above* the highest index until  
                #just one *before* the requested element
                for j in range(self._index + 1, i):
                    self.append(None)
                self.append(v)
            #For existing indices, replace
            else:
                list.__setitem__(self, i, v)
        #Negative Indexes (No Dynamics extensions)
        elif i < 0:
            if i < -self._length:
                raise IndexError
            else:
                list.__setitem__(self, i, v)
