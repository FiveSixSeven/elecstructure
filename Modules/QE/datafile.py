#! /usr/bin/python

import re
import sys

class File(object):
    """
    Class for Quantum-Espresso files that need to be read into lists as data (floats or ints).
    Lines with leading #'s will be ignored.
    """

    def __init__(self, filename):
        self.filename = filename
        self.f = open(filename, 'r')

    def fgetall(self):
        """
        Read in the total file as a float array
        """
        array = []
        for line in self.f:
            if not re.match('^#+', line):
                array.append([float(i) for i in re.split('\s+', line.strip())])
        return array

    def igetall(self):
        """
        Read in the total file as a int array
        """
        array = []
        for line in self.f:
            if not re.match('^#+', line):
                array.append([int(i) for i in re.split('\s+', line.strip())])
        return array

    def close(self):
        self.f.close()


class PosFile(File):
    """
    Class for Quantum-Espresso Position files
    """

    def __init__(self, filename, units='Bohr', *num):
        """
        @param: filename is the position file name
        @param: *num is a tuple of the number of each species this must be in the correct order (as in the
        input file"""
        File.__init__(self, filename)
        self.step = 0
        self.time = 0
        if units not in ('Angstrom', 'Bohr'):
            print "Error, units for the Position File can only be Bohr or Angstrom"
            sys.exit(1)
        else:
            self.units = units
            self.ao = 0.52917721092
        #Set Species
        self.nspecies = []
        for val in num:
            self.nspecies.append(int(val))

    def gettime(self):
        """ Return the current time during an iter, 0 to start """
        return self.time

    def getstep(self):
        """ Return the current step number during an iter, 0 to start """
        return self.step

    def fgetall(self):
        """
        Read in the total (Position) file as a float array
        Overwirtes the previsous file
        """
        array = []
        for line in self.f:
            if not re.match('^#+', line):
                if self.units == 'Angstrom':
                    array.append([self.ao * float(i) for i in re.split('\s+', line.strip())])
                else:
                    array.append([float(i) for i in re.split('\s+', line.strip())])
        #Re-open the file for future use
        File.__init__(self, self.filename)
        return array

    def __iter__(self):
        return self

    def next(self):
        """
        This will iterate through the QE position file, find the beginning of a configuration by
        identifying the <Step Time> line (which will only have two elements) then cycle through the
        atoms associated with that configuration and return a list (one element for each nspecies) of
        2 dimensional lists (a "list of list of list") for each iteration.
        pos[species-num][atom-number][xyz-coor]
        """

        #ith line
        rline = self.f.readline()
        if not rline: # If line returns blank line
            #re-open the file so that we can loop through it again
            File.__init__(self, self.filename)
            #Raise exception to stop the iteration
            raise StopIteration

        #split up the lines
        line = re.split('\s+', rline.strip())

        #start of a new configuration set
        if len(line) == 2:
            self.step, self.time = [float(i) for i in line[:]]

            #Return value "list of list of list"
            iterpos = []
            for ns in self.nspecies:
                pos = []
                for na in range(1, ns + 1):
                    #Read the next line, append the next line
                    if self.units == 'Angstrom':
                        pos.append([self.ao * float(i) for i in self.f.readline().strip().split()])
                    else:
                        pos.append([float(i) for i in self.f.readline().strip().split()])
                iterpos.append(pos)

            return iterpos
