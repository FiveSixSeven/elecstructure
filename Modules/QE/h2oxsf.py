#! /usr/bin.env python

import os
import sys
import commands
import numpy
import re
import xsf

#TODO Correct HBonds so that it will allow for solvated ions
# need to correct the self.other_num to be a list

class Xsf(xsf.Xsf):
    """
    Subclass of the xsf.Xsf Class. This is specifically deigned for water simulations
    with small amount of solvated ions

    Initialing Instance:
        filename: Name of the of the pre-existing *.xsf file
        olabel (optional): Secondary name given to the Oxygen (OO, etc ...)
        hlabel (optional): Secondary name given to the Hydrogen (HH, etc ...)

    Methods:

    """

    def __init__(self, filename, olabel=None, hlabel=None):
        """
        Initial the Instance, by calling base class.
        Calculate the number of oxygens, hydrogens, and other atoms
        """
        xsf.Xsf.__init__(self, filename)

        #Find all atom types
        self.onum = 0
        self.hnum = 0
        self.other_num = 0
        for na in self.atypes:
            if na == 'O' or na == 8 or na == eval(olabel):
                self.onum += 1
            elif na == 'H' or na == 1 or na == eval(hlabel):
                self.hnum += 1
            else:
                self.other_num += 1

    def build(self, outfile, cid=None, shell=None, wrap=False, isoprint=True, isostrip=False, solcheck=True):
        """
        Replacement for the xsf module Wrapper method for constructing the a new xsf file.
        This will trim the atomic cooridates if shell (and cid) are present)
        If the isosurface is present it will automatically be printed by default (isoprint).

        Arguments:
            outfile: New Xsf File name
            cid: ID Integer of the atom that you wish to center
            shell: Number of Solvation shell around cid to consider,
                cid Must be detefined for this to be considered.
                Determined by HBonds, a fortran program
            wrap: Boolean for perodic boundary condition wrap,
                not needed in xcrysden
            isoprint: Boolean for printing the isosurface if
                it exists

        """
        # Note: The cid must be defined for the shell program to activiated
        # FIXME clean this up, write better comments
        atoms = self.atoms[:]
        atypes = self.atypes[:]
        if cid is not None:
            if shell is not None:
                #determine the solvation shell, new cid number
                atoms, atypes, cid = self.solshell(shell, cid, check=solcheck)
                if self.isocheck and isoprint:
                    if isostrip:
                        isosurface = isosurface_strip(cid)
                    else:
                        isosurface = self.shiftiso(direction=None, cid=cid, point=None)
            else:
                if self.isocheck and isoprint:
                    isosurface = self.shiftiso(direction=None, cid=cid, point=None)
            #determine what the shift vector is
            dr = self.center - atoms[cid]
        else:
            #No Shift
            dr = numpy.array([0.0, 0.0, 0.0])
            isosurface = self.shiftiso(direction=None, cid=cid, point=None)

        #If avaiable convert the 3D isosurface into a 1D list
        if self.isocheck and isoprint:
            tot = self.isodim[0]*self.isodim[1]*self.isodim[2]
            isosurface = isosurface.reshape((tot), order="FORTRAN")

        print "Atomic Shift vector: ", '   '.join([str(i) for i in dr])
        atoms = atoms + dr

        if self.isocheck and isoprint:
            with open(outfile, 'w') as fo:
                #self._xsf_print(fo, atoms, isosurface)
                self._xsf_print(fo, atoms, atypes, isosurface)
        else:
            with open(outfile, 'w') as fo:
                self._xsf_print(fo, atoms, atypes, None)


    def solshell(self, shell, cid, check=True):
#TODO Make a oshell hsehll and othershell
        """
        Run the HBond executable

        Return: (Important, there are always 3 returns!)
            atoms: coordinate list of all atoms in the solvation shells
            atypes: a list of the atom types, in the new order
            new cid: The cid integer will need to be changed correponding the old cid
                position in the new list

        Note: The Fortran Program expects a 1-?? index for the atoms where as this fortran class
        uses a 0-?? index for the slef.stoms attribute
        """
        #Return values
        atoms = []
        atypes = []
        new_cid = 0 #Should always be zero (cid set to center of solvation shell)

        #----------------------------------------------------
        #HBonds executable
        #----------------------------------------------------
        curdir = os.path.realpath(os.path.abspath(__file__))
        curdir = os.path.dirname(curdir)
        hbonds = os.path.join(curdir, 'HBonds', 'HBonds.x')
        if os.path.isfile(hbonds) and os.access(hbonds, os.X_OK):
            print "Executable Exists: {}\n".format(hbonds)
        else:
            print "User Error => Executable Does Not Exit : {}".format(hbonds)
            sys.exit(1)

        solshell = [cid + 1]      #Solvation shell O numbers, Not indexes
        #----------------------------------------------------

        #----------------------------------------------------
        # temp position and cell file (MUST be in Bohr)
        #----------------------------------------------------
        ao = 0.52917721092
        posfile = os.path.join(os.curdir, 'pos.temp')
        with open(posfile, 'w') as fpos:
            fpos.write('     1   0.0\n')
            for na in self.atoms:
                fpos.write(' '.join(['{:15.10f}'.format(i/ao) for i in na]) + '\n')

        celfile = os.path.join(os.curdir, 'cel.temp')
        with open(celfile, 'w') as fcel:
            fcel.write('     1   0.0\n')
            for nv in self.lvecs:
                fcel.write(' '.join(['{:15.10f}'.format(i/ao) for i in nv]) + '\n')
        #----------------------------------------------------

        #----------------------------------------------------
        # HBonds Input template
        #----------------------------------------------------
        #TODO add ntsp
        input_template = """
        &system
        filePos     =  '{posfile:}'
        fileCel     =  '{celfile:}'
        step        =  1
        ntsp        =  2
        Ospecies    =  1
        Hspecies    =  2
        nsp(1)      =  {self.onum:}
        nsp(2)      =  {self.hnum:}
        rcut        =  1.1655
        Hbond_cut   =  3.4
        Hbond_angle =  145.0
        /
        """.format(**vars())
        #----------------------------------------------------

        cursol = solshell[:]    #Atoms in the current solvation shell, start with just O
        nsol   = 0              #current solvation shell

        #----------------------------------------------------
        # Nearest Oxygens (nearest-O)
        #
        # format: o-index x-coor y-coor z-coor
        #----------------------------------------------------
        #loop over all of the solvation shells
        while 1:

            #Note: this is a lagging loop, values computed before their loop
            if nsol == shell:
                break
            else:
                #setup for the next solvations shell
                nsol += 1

            nextsol = []        #Solvation shell for the next loop (becomes curshell)
            for atom in cursol:

                #Run the Hbond Program
                cmd = 'echo \"{:s}\" | {:s} {}'.format(input_template, hbonds, atom)
                status, output = commands.getstatusoutput(cmd)
                if status:
                    print "ERROR: HBonds returned non-zero exit status ", status

                #read the output (nearest-O) from Hbonds
                #The First Column of the nearest-O are those Oxygen's hydrogen bonded
                # to the current atom
                with open('nearest-O.dat') as f:
                    for lines in f:
                        #Skip comment lines
                        if re.match('^#', lines):
                            continue
                        else:
                            #Do not add repeats from peridoc boundary conditions
                            if int(lines.split()[0]) not in solshell:
                                solshell.append(int(lines.split()[0]))
                            nextsol.append(int(lines.split()[0]))

            #replace cursol for next loop
            cursol = nextsol[:]

            print "Solvation Shell {} Oxygens:".format(nsol), ' '.join([str(i) for i in solshell])

            #check to be sure all atoms have been included within this solvation shell
            if check:
                solshell = self._solshell_check(cid, solshell)

        print "\nNearest Oxygens:", ' '.join([str(i) for i in solshell])

        #----------------------------------------------------
        # Nearest Hydrogens (Href)
        #
        #  format: index Href1 Href2 Href3 Href4
        #    Zeros mean NO reference
        #
        #----------------------------------------------------
        with open('Href.dat', 'r') as fhref:
            href = fhref.readlines()
            href.pop(0)

        #loop over the current Oxygen-Only Solvation shell
        #use Href.dat to find the corresponding hydrogens
        for no in solshell[:]:
            #Remove all entries with zero
            temp = [int(i) for i in href[no-1].split()[1:]]
            trimed_temp = []
            for i,v in enumerate(temp[:]):
                if not v == 0:
                    trimed_temp.append(v)
        #FIXME This assumes that the oxygens are first on the list
        #FIXME MAKE multiple lists
            #Append teh solvation shell list
            for nh in [int(i)+self.onum for i in trimed_temp]:
                #Do not add repeats from peridoc boundary conditions
                if nh not in solshell:
                    solshell.append(nh)
        print "Solvation Shell:", ' '.join([str(i) for i in solshell]), '\n'


        #convert solshell to atoms
        for na in solshell:
            atoms.append(self.atoms[na-1])
            atypes.append(self.atypes[na-1])

        atoms = numpy.array(atoms)
        return atoms, atypes, new_cid


    def _solshell_check(self, cid, shell, fraction=1.0):
        """
        Note the solvation shell indexes are the true values (the start at 1)
        """
        added_index = []
        def pbc(x, length):
            for i in range(3):
                x[i] = x[i] - int(round(x[i]/length[i][i]))*length[i][i]
            return x

        max_r2 = 0.0
        for na in shell:
            dr = self.atoms[na-1] - self.atoms[cid]
            dr = pbc(dr, self.lvecs)
            r2 = sum(dr**2)
            if r2 > max_r2:
                max_r2 = r2
        print "Checking for Non-HBonded Atoms inside Max:", max_r2

        ncount=0
        for na in range(self.onum):
            #skip if the atom is in the solvation shell already
            #Using the fortran indexes
            if ncount+1 in shell:
                #print "found"
                ncount += 1
                continue
            else:
                dr = pbc(self.atoms[na] -self.atoms[cid], self.lvecs)
                r2 = sum(dr**2)
                #print r2
                if r2 < fraction*max_r2:
                    print " Extra-atom found:", ncount + 1
                    added_index.append(ncount+1)

                ncount+=1
        return shell + added_index

    def _isosurface_strip(self, cid):
        """
        shift and strip the isosurface
        """

        #shif the isosurface
        isosurface = self.shiftiso(direction=None, cid=cid, point=None)
