#! /usr/bin/env python

import re, numpy

class Xsf(object):

    #TODO Remove the all-included self.stoms and add self.oatoms, self.hatoms, self.otheratom
    def __init__(self, filename):
        self.filename = filename
        self.lvecs = []  #will be numpy array
        self.center = [] #will be numpy array
        self.nat   = 0
        self.atypes = []
        self.atoms = []  #will be numpy array
        self.isocheck   = False
        self.isodim = []
        self.isoint = []
        self._initopen()

    def _initopen(self):
        """
        Preform an initial open and reading of the xsf file. Extract the supercell lattice vectors,
        number of atoms, atom position and atom types.  Also determine whether or not a DataGrid 
        exists and what the dimensions and intervals are. This will NOT read in the isosurface
        as it's normally too large.
        """
        #REGEX
        p_vec = re.compile('PRIMVEC')
        p_prim = re.compile('PRIMCOORD')
        p_data = re.compile('BEGIN_BLOCK_DATAGRID_3D')

        #Open file and loop through
        with open(self.filename) as f:
            while True:
                line = f.readline().strip()
                if not line:
                    break
                else:
                    #------------------------------------------------------
                    #Find the Lattice vectors, and the center of the object
                    #------------------------------------------------------
                    if p_vec.search(line):
                        for i in range(3):
                            self.lvecs.append([float(i) for i in f.readline().strip().split()])
                        self.lvecs = numpy.array(self.lvecs)
                        #TODO Make the center calculation for general, Assumes cubic shape
                        for i in range(3):
                            self.center.append(self.lvecs[i][i]/2.0)
                        self.center = numpy.array(self.center)

                    #------------------------------------------------------
                    #Find the number of atoms, atom type and atom positions
                    #------------------------------------------------------
                    elif p_prim.search(line):
                        line = f.readline().strip().split()
                        self.nat = int(line[0])
                        for i in range(self.nat):
                            line = f.readline().strip().split()
                            self.atypes.append(line[0])
                            self.atoms.append([float(i) for i in line[1:]])
                        self.atoms = numpy.array(self.atoms)

                    #------------------------------------------------------
                    #Check if the isosurface is present
                    #------------------------------------------------------
                    elif p_data.search(line):
                        self.isocheck = True
                        #Read lines until isosurface dimension are found 
                        #(May be 1-2 lines with DATAGRID_3D_UNKOWN, etc ...)
                        while True:
                            line = f.readline()
                            if not re.search('[A-Z]+', line):
                                break
                        self.isodim = ([float(i) for i in line.strip().split()])
                        self.isodim = numpy.array(self.isodim)
                        for i in range(3):
                            #TODO add check to make sure this is a cuic supercell
                            self.isoint.append(self.lvecs[i][i]/self.isodim[i])
                        self.isoint = numpy.array(self.isoint)

        
    def shiftatoms(self, direction=None, cid=None, point=None, wrap=False):
        """
        Shift all atoms in a tuple direction. If the direction tuple is given ignore the 
        cid integer (center ID). The center integer will shift a specified atom to the center of the
        supercell. Id direction and cid not specified, point may be used to shift a particular 
        point to the center of the supercell

        Wrap is set to False by default becasue xcrysden will automatic wrap our positions

        Return Shifted atoms.

        !!!Please note: This currently only designed for Simple cubic systems!!!

        """
        #TODO add point
        if direction is not None:
            #TODO
            pass
        elif cid is not None:
            #determine what the shift vector is
            dr = self.center - self.atoms[cid]
        #TODO Add Wrap
        else:
            #No Shift
            dr = numpy.array([0.0, 0.0, 0.0])

        print "Atomic Shift vector: ", '   '.join([str(i) for i in dr])
        return self.atoms + dr

    def shiftiso(self, direction=None, cid=None, point=None):
        """
        Shift all isopoints in a tuple direction. If the direction tuple is given ignore the 
        center integer. The cid (center ID) will shift a specified atom to the center of the
        supercell. Id direction and cid not specified, point may be used to shift a particular 
        point to the center of the supercell
        
        Return Shifted isosurface (large).

        !!!Please note: This currently only designed for Simple cubic systems!!!
        """
        #This will work for a QE produced isosurface
        piso = re.compile('DATAGRID_3D_UNKNOWN')
        pend = re.compile('END_DATAGRID_3D')
        iso = []
        with open(self.filename, 'r') as fiso:
            while True:
                line =  fiso.readline()
                if not line:
                    break

                if piso.search(line):
                    #Read extra lines
                    for i in range(5):
                        line = fiso.readline()

                    #construct 1-D list for the isosurface
                    line = fiso.readline()
                    while not pend.search(line):
                        for i in [float(i) for i in line.strip().split()]:
                           iso.append(i) 
                        line = fiso.readline()
                    iso = numpy.array(iso)

        #TODO add point
        if direction is not None:
            #TODO
            pass

        elif cid is not None:
            #Determine what the shift vector is and how many intervals it contains
            dr = self.center - self.atoms[cid]
            dr_isoint = dr/self.isoint
            dr_isoint = [round(dr_isoint[i]) for i in range(3)]
            dr_isoint = numpy.array(dr_isoint, dtype=int)

        else:
            dr_isoint = [0, 0, 0]

        print "Isosurface Shift intervals: ", '   '.join([str(i) for i in dr_isoint])
        #convert 1-D list of isosurface into a FORTRAN 3D Numpy array
        iso = iso.reshape(self.isodim, order='FORTRAN')
        for i in range(3):
                iso = numpy.roll(iso, dr_isoint[i], axis=i)
        return iso



    def build(self, outfile, direction=None, cid=None, point=None, wrap=False, isoprint=True):
        """
        Wrapper method for constructing the a new xsf file. If the isosurface is 
        present it will automatically be printed by default (isoprint).

        Arguments:
            outfile: New Xsf File name
            direction: Direction Tuple to shift all atoms
            cid: ID Integer of the atom that you wish to center
            point: tuple of the point you wish to center (Angstrom)
            wrap: Boolean for perodic boundary condition wrap, 
                not needed in xcrysden
            isoprint: Boolean for printing the isosurface if
                it exists

            Note: the default order is direction, cid, point


        """
        atoms = self.shiftatoms(direction, cid, point, wrap)
        atypes = self.atypes
        #With isosurface present
        if self.isocheck and isoprint:
            isosurface = self.shiftiso(direction, cid, point)
            tot = self.isodim[0]*self.isodim[1]*self.isodim[2]
            isosurface = isosurface.reshape((tot), order="FORTRAN")
            with open(outfile, 'w') as fo:
                self._xsf_print(fo, atoms, atypes, isosurface)

        #Without isosurface present
        else:
            with open(outfile, 'w') as fo:
                self._xsf_print(fo, atoms, atypes, None)


    def _xsf_print(self, fo, atoms, atypes, isosurface):
        """
        Given the atoms and the isosurface, print the xsf file.
        Isosurface will not be used if self.isocheck = False.
        """

        #Atomic Positions
        fo.write("CRYSTAL\nPRIMVEC\n")
        for i in self.lvecs:
            fo.write(' '.join(['{:15.10f}'.format(j) for j in i]) + '\n')
        fo.write("PRIMCOORD\n\t{}\t1\n".format(len(atoms)))
        for i,j in zip(atypes, atoms):
            fo.write(str(i) + " " + " ".join(['{:15.10f}'.format(k) for k in j]) + '\n')

        if self.isocheck and isosurface is not None:
            #DataGrid
            fo.write("BEGIN_BLOCK_DATAGRID_3D\n3D_PWSCF\nDATAGRID_3D_UNKNOWN\n")
            fo.write("     "+"    ".join(["{}".format(int(i)) for i in self.isodim]) + '\n')
            #TODO Read in the origin
            fo.write("0.000000  0.000000  0.000000\n")
            for i in self.lvecs:
                fo.write(' '.join(['{:15.10f}'.format(j) for j in i]) + '\n')
            ncount = 0
            for i in isosurface:
                fo.write(" {:10.6E} ".format(i))
                ncount += 1
                if ncount % 6 == 0:
                    fo.write("\n")
            fo.write("END_DATAGRID_3D\nEND_BLOCK_DATAGRID_3D")

