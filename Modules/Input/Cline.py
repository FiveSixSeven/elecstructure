#! /usr/bin/env python

import sys, getopt, re

class Args(object):
    """
    Easy integration with the getopt Module

    Input:
        1) cline    -   Argument list (argv.sys[1:])
        2) info     -   A dictionary of lists for the short-name, long-name, and default value (or None)
                         ex: { dir   : [ 'd:', 'dir='  , '/home'] }  argument with a parameter
                         ex: { print : [ ''  , 'print' , 'False'] }  argument without a parameter (True/False)
                         ex: { files : [ 'f:', ''      , 'None' ] }
                        A ':' or a '=' will cause the script to look for a parameter

        3) usage    -   Usage String 
    """
    def __init__(self, cline, info, usage):
        self.info       = info
        self.values     = {key : self.info[key][2] for key in self.info.keys()} 
        self.short_opts = self._get_short_opts(info)
        self.long_opts  = self._get_long_opts(info)
        self.usage      = usage

        cline = self._correct_cline(cline)
        self._parse(cline) 




    def _correct_cline(self, cline):
        """
        Correct the issue with passing strings (using " or ') as parameters to options in the commands line
        """
        corr_cline = []
        while True:
            #Next elemnet
            try:
                element = cline.pop(0)
            except:
                break

            #Check if " or ' is in the element
            if re.search('\'|\"', element):
                combine = element + ' '
                #Loop until end of string
                while True:
                    try:
                        element = cline.pop(0)
                    except:
                        print "ERROR: Unclosed sting in Command Line"
                        sys.exit(1)
                    combine += str(element) + ' ' 
                    if re.search('(\'|\")$', str(element)):
                        corr_cline.append(combine)
                        break
            else:
                corr_cline.append(element)

        #finally remove all " amd '
        for index, element in enumerate(corr_cline):
            corr_cline[index] = re.sub('(\"|\')', '', element)
            
        return corr_cline
        

    def _parse(self, cline):
        #Parse the command line
        try:
            #self.long_opts will always have at least --help
            self.tot_opts, self.tot_args = getopt.getopt(cline, self.short_opts, self.long_opts)
        except getopt.GetoptError as err:
            # print, error, usage, and then exit
            print '\n' + str(err) 
            print self.usage
            sys.exit(2)

        #Loop Over the keys from info
        for key in self.info.keys():

            #to determine if the agrument has a parameter to be read-in
            parameter = False

            #Short Values tags
            if ':' in self.info[key][0]: 
                parameter = True
            stag = '-' + self.info[key][0].replace(":", "")
            #Long values tags (parameter should be true for BOTH long and short)
            #if '=' in self.info[key][1] or not self.info[key][1]:
            if '=' in self.info[key][1]:
                parameter = True
            ltag = '--' + self.info[key][1].replace("=", "")

            for opt, arg in self.tot_opts:
                #Print the Help Message (Always added in get_short/long_opt(
                if opt == '-h' or opt == '--help':
                    print self.usage
                    sys.exit(0)
                #find the tag
                if opt == stag or opt == ltag:
                        
                    #Check to see if a parameter needs to read-in
                    if parameter:
                        self.values[key] = arg
                    else:
                        #If not parameter is to be read set to True
                        self.values[key] = True

        return
                        
                 

    def get_opt(self, tag):
        """
        Get a Single Variable value

        Input:
            tag - variable name, Must be the same as those declared in 'info' keys
        """
        try:
            return self.values[tag]
        except KeyError as ierr:
            print " CLI Error:", ierr
            sys.exit[1]



    def get_opts(self):
        """
        Return all variables and there values as dictionary
        """
        return self.values


    def get_args(self):
        """
        Return all arguments beyond the options
        """
        return self.tot_args



    def convert_ints(self, keys):
        """
        Given a group of keys convert the values in self.values into integers
        """
        try:
            for key in keys:
                self.values[key] = int(self.values[key])
        except KeyError as ierr:
            print (ierr)
            sys.exit(2)



    def convert_floats(self, keys):
        """
        Given a group of keys convert the values in self.values into floats
        """
        try:
            for key in keys:
                self.values[key] = float(self.values[key])
        except KeyError as ierr:
            print (ierr)
            sys.exit(2)



    def convert_complexes(self, keys):
        """
        Given a group of keys convert the values in self.values into complex
        """
        try:
            for key in keys:
                self.values[key] = complex(self.values[key])
        except KeyError as ierr:
            print (ierr)
            sys.exit(2)



    def _get_short_opts(self, vals):
        """
        Get all short options
        """
        string = ''
        for key in vals:
            string += vals[key][0]
        #add h always
        if 'h' in string:
            print "CLI Error: help Must be defined as h"
            sys.exit(2)
        string += 'h'
        return string


    def _get_long_opts(self, vals):
        """
        Get all short options
        """
        string = []
        for key in vals:
            if vals[key][1]:
                string.append(vals[key][1])
        #add 'help' always
        if 'help' in string:
            print "CLI Error: help Must be defined as h"
            sys.exit(2)
        string.append('help')
        return string

def __test():
    """
    RUN TESTs
    """
    print "\nCheck short Read-In... \nInital value: Hello"
    variables = { 'test' : ['t:', 'test=', 'Hello'] }
    #IMPORTANT cline MUST be a list (sys.arvg[1;])
    cline = ' -t Goodbye '.split()
    print 'cline', ' '.join(cline)
    test = Args(cline, variables, 'Usage Message')
    print "short options:", test.short_opts
    print "long options:", test.long_opts
    print "Variable Now:", test.get_opt('test')

    print "\nCheck long read-in... \ninital value: hello"
    variables = { 'test' : ['t:', 'test=', 'hello'] }
    #important cline must be a list (sys.arvg[1;])
    cline = '--test=goodbye'.split()
    print 'cline', ' '.join(cline)
    test = Args(cline, variables, 'usage message')
    print "short options:", test.short_opts
    print "long options:", test.long_opts
    print "variable now:", test.get_opt('test')

    print "\nCheck long read-in with strings... \ninital value: hello"
    variables = { 'test' : ['t:', 'test=', 'hello'],
                  'check': ['c', ''      , False]
                }
    #important cline must be a list (sys.arvg[1;])
    cline = '--test="goodbye my love " -c'.split()
    print cline
    print 'cline', ' '.join(cline)
    test = Args(cline, variables, 'usage message')
    print "short options:", test.short_opts
    print "long options:", test.long_opts
    print "variable now:", test.get_opt('test')
    print "variable now:", test.get_opt('check')

    print "\nCheck short non-parameter... \ninital value: False"
    variables = { 'test' : ['t', 'test', False] }
    #important cline must be a list (sys.arvg[1;])
    cline = '-t'.split()
    print 'cline', ' '.join(cline)
    test = Args(cline, variables, 'usage message')
    print "short options:", test.short_opts
    print "long options:", test.long_opts
    print "variable now:", test.get_opt('test')

    print "\nCheck short non-parameter... \ninital value: False"
    variables = { 'test' : ['t', 'test', False] }
    #important cline must be a list (sys.arvg[1;])
    cline = '--test'.split()
    print 'cline', ' '.join(cline)
    test = Args(cline, variables, 'usage message')
    print "short options:", test.short_opts
    print "long options:", test.long_opts
    print "variable now:", test.get_opt('test')

    print "\nCheck Multiple short Read-In... \nInital value: Hello"
    variables = { 'test' : ['t:', 'test=', 'Hello'] }
    #IMPORTANT cline MUST be a list (sys.arvg[1;])
    cline = ' -t Goodbye '.split()
    print 'cline', ' '.join(cline)
    test = Args(cline, variables, 'Usage Message')
    print "short options:", test.short_opts
    print "long options:", test.long_opts
    print "Variable Now:", test.get_opt('test')

    print "\nCheck Multiple long read-in... \ninital value: hello goodbye"
    variables = { 
        'hi'  : ['g:', '', 'hello'],
        'bye' : ['b:', '', 'goodbye'],
    }
    #important cline must be a list (sys.arvg[1;])
    cline = '-g yoo -b later'.split()
    print 'cline', ' '.join(cline)
    test = Args(cline, variables, 'usage message')
    print "short options:", test.short_opts
    print "long options:", test.long_opts
    print "variable now:", ' '.join(test.get_opts().values())

    print"\nHelp Check and exit"
    cline = '--help'.split()
    test = Args(cline, variables, 'Usage Message')

if __name__ == '__main__':
    #Run Test
    __test()
