#! /usr/bin/env python

import re

class namelist(object):
    """
    """

    def __init__(self, filename, check=True, convert=True, **kwargs):

        #Initial  Class Variables
        self.vals = {}                  #Main input variables (Dict)
        self._filename = filename       #Input File
        self._init = kwargs             #Defined, Required, variables with default values (Dict)
        self.check = check              #Check that the Required variables have a assigned value
        self.convert = convert          #Convert into correct data-type

        #Regular expressions
        self.__pint  = re.compile('^-?[0-9]+$')
        self.__preal = re.compile('^-?[\d\.]+[eE]?[+-]?[0-9]*$') 
        self.__pbool = re.compile('^(True|False)$', re.IGNORECASE)
        self.__pstr  = re.compile('^\w+$')

        #Open file
        self._open()


    def _open(self):
        """
        Open the current file and parse, look for Fortran type namelist constructs:
        variable = value[,]
        """
        #Regex for the namelist
        p = re.compile('([\w_\(\)]+)\s*=(.*)')
        #Open and read the inputfile
        with open(self._filename, 'r') as f:
            for lines in f:
                m = p.search(lines.strip())
                if m:
                    #strip whitespace
                    raw = m.group(2).strip()
                    #remove possible ending comma
                    raw = raw.strip(',')
                    #remove possible single quotes
                    raw = raw.strip("'")
                    #remove possible double quotes
                    raw = raw.strip('"')

                    #Assign to self.vals (remove remaining whitespace)
                    self.vals[m.group(1).strip()] = raw
        if self.check and self._init:   
            self.var_check(self._init, twoway=True)
        if self.convert:
            self._type_check()

    def var_check(self, check, twoway=False):
        """ 
        A check of pre-defined variables and read-in variables.
        Check to see if all of the read-in variables have a value other then None.
        Also check to be sure that no extra read-in variables

        Input:
           1) a dictionary/list of all files that should be accounted for
        """
        #Make sure that all pre-defined variables (check) are accounted for in read-in variables (self.vals)
        try:
            for val in check:
                if val not in self.vals:
                    raise NamelistError(val)
        except NamelistError as ierr:
            print  "\nNamelistError exception raised:",
            print  "Required Variable '{}' Not Defined in {}".format(ierr.val, self._filename)

        #Make sure that all read-in variables (@self.vals) were declared in pre-defined variables (@check), by
        #default this is not checked (@twoway)
        if twoway:
           try:
               for val in self.vals:
                   if val not in check:
                       raise NamelistError(val)
           except NamelistError as ierr:
               print  "\nNamelistError exception raised:",
               print  "Read-in Variable '{}' Not Defined during instance creation {}"\
                       .format(ierr.val, self._filename)

    def _type_check(self):
        """
        Loop through all the keys in the self.vals dictionary
        convert to appropriate data-type. For pre-defined
        list, convert all the elements of the list
        """
        for val in self.vals:
            #If this particular read-in variable was pre-defined as a list
            #loop over all the elements split on whitespace 
            if self._init and isinstance(self._init[val], list):
                #make a copy of the string currently in self.vals[val]
                tempstr = self.vals[val]
                self.vals[val] = []
                for subval in tempstr.split():
                    self.vals[val].append(self._conversion(subval))
            #If the element is not a pre-defined list of a single element
            #convert into an appropriate 
            else:
                self.vals[val] = self._conversion(self.vals[val])


    def _conversion(self, val):
        """
        Test for data type, return converted value
        """
        if self.__pint.search(val):
            return int(val)
        elif self.__preal.search(val):
            return float(val)
        elif self.__pbool.search(val):
            return bool(val)
        elif self.__pstr.search(val):
            return str(val)
        else:
            #This will keep the value as string
            #TODO remove starting/ending single/double quotes
            return val

                
    def getall(self):
        """
        Returns a dictionary of the read-in variables
        """
        return self.vals

    def get(self, val):
       """
       Returns a particular value
       """
       try:
          return self.vals[val]
       except IndexError:
          print "User Error -> {} Not defined in input variables"
          sys.exit(1)

    def __getitem__(self, val):
       """
       Returns a particular value, through indexing
       """
       return self.get(val)

    def __setitem__(self, key, item):
       self.vals[key] = item

    def __str__(self):
       return self.getall()

    def __repr__(self):
       #FIXME correct this small bug
       return self.getall()

class NamelistError(Exception):
    """
    Exception for required variables not found in namelist
    """
    def __init__(self, val):
        self.val = val
