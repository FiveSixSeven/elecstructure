#! /usr/bin/env python

import os, sys

eig = []; dos = []
ldos = 0; le = 0
total = 0

for dir in sys.argv[1:]:
   path = os.path.join(os.curdir, dir, 'ldos.dat')
   try:
      with open(path) as f:
         print "File:", dir
         lcount=0
         for line in f:
            if not line:
               continue
            (e,v) = [float(i) for i in line.strip().split()]
            #first time through, set initials
            if total == 0:
               eig.append(e)
               dos.append(v)
            else:
               #Check for different energy scales
               if eig[lcount] != e:
                  print "\nERROR: Eigen Engery scales are different!!\n"
                  sys.exit(1)
               dos[lcount] += v

            lcount += 1
      total += 1

   except IOError:
      print "Skipping", dir

with  open('ldos-avg.dat', 'w') as fo:
   for e,d in zip(eig, dos):
      fo.write("{:20.10f} {:20.10f}".format(e, d/total) + '\n')

print "\n Total LDOS:", total
print " Output: ldos-avg.dat\n"

