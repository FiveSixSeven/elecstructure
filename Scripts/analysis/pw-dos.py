#! /usr/bin/env python
"""
This script will run all PW dos calculations in a glob of directories

Command Line Arugments:
   - Dir-Names
"""

import os, sys, re
script_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(script_dir)
sys.path.append(os.path.join(script_dir, os.pardir, 'main'))

def main():
    print "\n  DOS-PW calculation script\n\n"

    #some important variables
    script_dir = os.path.dirname(os.path.realpath(__file__))
    dos_exe = os.path.join(script_dir, os.pardir, 'Src', 'QE-PrintKS-Serial', 'bin', 'dos.x')
    maindir = os.getcwd()
    savedir = '.save'

    print "   Save Directories: {}".format(savedir)

    dirs = [ i for i in sys.argv[1:] ]
    #--------------------------------
    #Loop through the directory
    #--------------------------------
    for curdir in dirs:
        try:
            os.chdir(curdir)
            print "\nDirectory:", curdir
            
            for name in os.listdir(os.curdir):
                p = re.compile('(.*){}'.format(savedir))
                m = p.search(name)
                if m:
                    prefix = m.group(1)

            #some internal dos calculations
            dos_input = """
            &inputpp
               prefix   =  '{}',
               outdir   =  './'
               fildos   =  'dos.dat',
               Emin     =  -50,
               Emax     =  10,
               DeltaE   =  0.01,
               degauss  =  7.0e-3,
               ngauss   =  0
            /
            """.format(prefix)

            with open('dos.in', 'w') as fo:
                fo.writelines(dos_input)

            os.system('{} < dos.in'.format(dos_exe))
                  
            os.chdir(maindir)
        except OSError:
            print "Warning:", curdir, "is Not a Directory"
    #--------------------------------


if __name__ == '__main__':
    main()
