#! /usr/bin/env python

#---------------------------------------------
#Imports
#---------------------------------------------
import os, sys, getopt, re, glob, imp, time, operator
import numpy as np
script_dir = os.path.dirname(os.path.realpath(__file__))
package_dir = os.path.join(script_dir, os.pardir, os.pardir)
sys.path.append(os.path.join(script_dir, os.pardir, 'main'))
import ElecStructClass as ESC
cl  = imp.load_source('Cline', os.path.join(package_dir, 'Modules', 'Input', 'Cline.py'))
#---------------------------------------------

def main(argv):

    print "\n Print-XSF Files script\n"

    #Must be in Angstrom
    ao = 0.52917721092 

    #Command Line options, clargs (bands)
    inv, clargs = startup(argv)

    #Move to the correct directory
    try:
        os.chdir(inv['dir'])
        print "\n  Directory:", inv['dir']
    except OSError:
        print " Warning:", inv['dir'] , "is Not a Directory"
        sys.exit(1)
        
    #Set the  correct number of Bands
    if len(clargs) > 0:
        print "  Number of Bands:   " + ' '.join(clargs)
        bands = [int(i) for i in clargs]
    else:
        print "  Number of Bands:   All"
        bands = None

    #---------------------------------------------
    #Get the save directory
    #---------------------------------------------
    p = re.compile('(\w+)_\d+\.save')
    lastdir = ''
    #pre-defined save directory
    if inv['outdir']:
        m = p.search(inv['outdir'])
        if m:
            lastdir = inv['outdir']
        else:
            print "ERROR: Predefined outdir {} must be in the *_<NUM>.save format!".format(outdir)
            sys.exit(1)
    #Youngest save dir
    else:
        found = []
        for name in os.listdir(os.curdir):
            m = p.search(name)
            if m and os.path.isdir(name):
                found.append(name)
        lastdir = get_youngest_file(found)

    if not lastdir:
        print "ERROR: No outdir found!"
        sys.exit(1)
    else:
        m = p.search(lastdir)
        prefix = m.group(1)
        print "  Save Directory: {}".format(lastdir)
    #---------------------------------------------


    #---------------------------------------------
    # Open the data-file.xml file
    #---------------------------------------------
    try: 
        xsf = ESC.xml(os.path.join(os.curdir, lastdir, 'data-file.xml'))
    except:
        print "ERROR: data-file.xml does not exist"
        sys.exit(1)

    #Convert xml files to ints/floats
    try:
        grid = np.array([ int(i) for i in xsf.fft ])
        totfft = reduce(lambda x, y: x*y, grid)

        aprim = [ ]
        for line in xsf.aprim:
            aprim.append([ float(i)*ao for i in line.split()])

        atoms = []
        for line in xsf.atoms:
            temp = line.split()
            atoms.append([ temp[0], float(temp[1])*ao, float(temp[2])*ao, float(temp[3])*ao ])

    except NameError:
        print "ERROR: data-file.xml incorrect format!"
        sys.exit(1)
    #---------------------------------------------


    #---------------------------------------------
    # Loop over the Bands
    #---------------------------------------------
    print "\n  Printing Bands..."
    for num in xrange(int(xsf.nbnd)):

        #Check number of bands
        if bands is not None and (num + 1) not in bands:
            continue

        try:
            f = open('./KS_{}.dat'.format(num+1))
        except:
            print "   Warning: Unable to open KS_{}.dat".format(num+1)
            continue

        #Data-Type for the KS.dat binary file (single state)
        data = np.fromfile(f, np.dtype([('f1','<i4'), ('f2','{}<c16'.format(totfft)), ('f3','<i4') ]), count=1)
        psi = data['f2'][0]
        psi2 = psi * np.conj(psi)
        psi2 = psi2.real


        #Check the integrated charge
        charge = np.sum(psi2)
        print "   Reading Band,", num + 1, \
                "Integrated Charge: {:6.3f}".format(charge/reduce(lambda x,y: x*y, grid))


        with open('KS_{}.xsf'.format(num + 1), 'w') as fo:
            fo.write("CRYSTAL\nPRIMVEC\n")
            for line in aprim:
                fo.write(' '.join(['{:12.6f}'.format(i) for i in line]) + '\n')
            fo.write('PRIMCOORD\n')
            fo.write('      {}      1\n'.format(xsf.nat))
            for line in atoms:
                fo.write(line[0])
                fo.write(' '.join(['{:12.6f}'.format(i) for i in line[1:]]) + '\n')
            fo.write('BEGIN_BLOCK_DATAGRID_3D\n')
            fo.write('3D_PWSCF\n')
            fo.write('DATAGRID_3D_UNKNOWN\n')
            fo.write(' '.join(xsf.fft) + '\n')
            fo.write(" 0.0000 0.0000 0.0000\n")
            for line in aprim:
                fo.write(' '.join(['{:12.6f}'.format(i) for i in line]) + '\n')
            np.savetxt(fo, psi2, fmt='%10.5E')
            fo.write('END_DATAGRID_3D\n')
            fo.write('END_BLOCK_DATAGRID_3D\n')
            
        f.close()
    print "\n  Finished Printing XSF Files"
    #---------------------------------------------



def startup(argv):
    """
    Read in Command Line options and arguments, set them up and retrun a dictionary
    """

    usage = """
    Usage:

        XSF Print-out
        This will convert raw KS-*.dat Files into *.xsf files using data from the outdir
        
        Optional:
        ------------
        -d          <NUM> : Main Directory [Current directory]
        --outdir    <NUM> : Save-Directory [search for youngest *_<NUM>.save]

        ARG               : Bands (If not present ALL Bands will be reproduced)
        """
    
    arguments = {
        'dir'       : ['d' , '', './'],
        'outdir'    : [''  , 'outdir=', ''],
    }

    read = cl.Args(argv, arguments, usage)
    result = read.get_opts()
    clargs = read.get_args()
    
    return result, clargs


def get_oldest_file(files, _invert=False):
    """ Find and return the oldest file of input file names.
    Only one wins tie. Values based on time distance from present.
    Use of `_invert` inverts logic to make this a youngest routine,
    to be used more clearly via `get_youngest_file`.
    """
    gt = operator.lt if _invert else operator.gt
    # Check for empty list.
    if not files:
        return None
    # Raw epoch distance.
    now = time.time()
    # Select first as arbitrary sentinel file, storing name and age.
    oldest = files[0], now - os.path.getctime(files[0])
    # Iterate over all remaining files.
    for f in files[1:]:
        age = now - os.path.getctime(f)
        if gt(age, oldest[1]):
            # Set new oldest.
            oldest = f, age
    # Return just the name of oldest file.
    return oldest[0]



def get_youngest_file(files):
    return get_oldest_file(files, _invert=True)

 


if __name__ == '__main__':
    main(sys.argv[1:])
