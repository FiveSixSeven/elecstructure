#! /bin/bash

#Bash wrapper script for the print-ks.py and realproj.py scripts.

#----------------------------------
#INPUTS
#1) print-ks.py $PRINKS_OPT -d <Loop-thru-dirs> $PRINTKS_BANDS
#2) realproj.py $REALPROJ_OPT <Loop-thru-dirs>
#----------------------------------
PRINTKS_OPT='--submit="aprun -n 24"'
PRINTKS_BANDS=""
REALPROJ_OPT='-s Cl -i 1 -r 1.0 --submit="aprun -n 24"'
#----------------------------------

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function check_error {
   if [ $? -ne 0 ]; then
      echo "Errors Encoutnered in $1 exiting"
      exit 2
   fi
}

#----------------------------------
# Command Line
#----------------------------------
usage()
{
cat << EOF

Bash wrapper script for the print-ks.py and realproj.py scripts.
To modify options for either indivdual python script you must modify this bash script

Usage:

ARGS:       List, or glob of Directory to loop through

OPTIONS:
   -s       Save Kohn Sham States, large amount of memory! [No Save]
   -h       Help
EOF
}

#Save the KS States
SAVE=0

while getopts “sh” OPTION
do
     case $OPTION in
         h)
             usage
             shift
             exit 1
             ;;
         s)
             SAVE=1
             shift
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

if [ $SAVE -eq '0' \]; then
   echo " All KS*.dat Files will be deleted"
else
   echo " All KS*.dat Files will be SAVE, High Memory"
fi
#******************************************************************************
# Main Loop
#******************************************************************************
for dir in $@; do

   echo "*****************************************************************"
   echo "   $dir                               "
   echo "*****************************************************************"

   echo "-----------"
   python $DIR/print-ks.py $PRINTKS_OPT -d $dir $PRINTKS_BANDS
   check_error "Print-KS"

   echo "-----------"
   python $DIR/realproj.py $REALPROJ_OPT $dir
   check_error "Real-Proj"

   #remove the KS.dat file to save space
   if [ $SAVE -eq '0' ]; then
      rm $dir/KS*dat
   fi

done
#******************************************************************************
