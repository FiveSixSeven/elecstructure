#! /usr/bin/env python
"""
This script will run all CP dos calculations in a glob of directories
"""

import os, sys, getopt, re, time, operator, imp
script_dir = os.path.dirname(os.path.realpath(__file__))
package_dir = os.path.join(script_dir, os.pardir, os.pardir)
sys.path.append(os.path.join(script_dir, os.pardir, 'main'))
cl  = imp.load_source('Cline', os.path.join(package_dir, 'Modules', 'Input', 'Cline.py'))
import ElecStructClass as ESC

def main(argv):
    print "\n DOS-CP calculation script\n\n"

    #some important variables
    dos_exe = os.path.join(script_dir, os.pardir, os.pardir, 'Src', 'CP-DOS', 'cp-dos.x')
    maindir = os.getcwd()

    #Command Line options, arguments
    inv, dirs = startup(argv)

    tempfile = '.ldos'
    #--------------------------------
    #Loop through the directory
    #--------------------------------
    for curdir in dirs:
        try:
            print "\n   Directory:", curdir
            os.chdir(curdir)
        except OSError:
            print "Warning:", curdir, "is Not a Directory"
            
        #Get the save directory
        p = re.compile('(\w+)_\d+\.save')
        lastdir = ''
        #pre-defined save directory
        if inv['outdir']:
            m = p.search(inv['outdir'])
            if m:
                lastdir = inv['outdir']
            else:
                print "ERROR: outdir {} must be in the *_<NUM>.save format!".format(inv['outdir'])
                sys.exit(1)
        #Youngest save dir
        else:
            found = []
            for name in os.listdir(os.curdir):
                m = p.search(name)
                if m and os.path.isdir(name):
                    found.append(name)
            lastdir = get_youngest_file(found)

        if not lastdir:
            print "   No outdir found, skipping directory"
            os.chdir(maindir)
            continue

        m = p.search(str(lastdir))
        prefix = m.group(1)
        print "   Save Directory: {}".format(lastdir)
        try: 
            dos = ESC.xml(os.path.join(os.curdir, lastdir, 'data-file.xml'))
        except:
            print "   data-file.xml does not exist, skipping directory"
            os.chdir(maindir)
            continue


        #Check to see if the final step in *.eig and data-file.xml match
        #There are legal cases where they woudl not
        with open('{}.eig'.format(prefix)) as fe:
           ncount = 0
           for line in fe:
              if re.search('STEP:', line):
                 ncount = line.split()[1]
           if int(ncount) != int(dos.step):
              print "   Note: Final step in data-file.xml {0} and {1}.eig {2} Do Not match. Using {2}"\
                                                                    .format(dos.step, prefix, ncount)
                 

        #some internal dos calculations
        dos_input = """
        &indos
           dosfile  =  'dos.dat',
           eigfile  =  '{0}.eig',
           Emin     =  {1},
           Emax     =  {2},
           DeltaE   =  {3},
           degauss  =  {4}
           step     =  {5},
           nbnd     =  {6},
           wk       =  {7}
        /
        """.format(prefix, inv['estart'], inv['estop'], inv['de'], \
                   inv['degauss'], ncount, dos.nbnd, inv['wk'])

        with open('dos.in', 'w') as fo:
            fo.writelines(dos_input)

        os.system('{} < dos.in'.format(dos_exe))
              
        os.chdir(maindir)
    #--------------------------------


def startup(argv):
    """
    Read in Command Line options and arguments, set them up and retrun a dictionary
    """

    usage = """

    Run the real-space porjections executable.
    Must Be started after the Modified QE-5.0.2 cp.x executable (print-ks.py).

    Usage:

    Optional:
    ------------
        --outdir   <NUM>    : Save-Directory Number [youngest]
        --output   <NAME>   : Output File ['./dos.dat']
        --wk       <NUM>    : Occupation Number [2.0]
    Plotting:
        --estart   <NUM>    : Starting Energy (eV) [-30]
        --estop    <NUM>    : Ending Energy (eV) [10]
        --de       <NUM>    : Energy Interval (eV) [0.01]
        --degauss  <NUM>    : Gaussian parameter for broadening [0.007]
         
        ARGS                : List, glob, single directory
    """
    
    arguments = {
        'outdir'    : [''  , 'outdir=', ''],
        'output'    : [''  , 'output=', './dos.dat'],
        'wk'        : [''  , 'wk=', 2.0],
        'estart'    : [''  , 'estart=', -30.0],
        'estop'     : [''  , 'estop=' ,  10.0],
        'de'        : [''  , 'de='    ,  0.01],
        'degauss'   : [''  , 'degauss=' ,  0.007],
    }

    read = cl.Args(argv, arguments, usage)
    read.convert_floats("wk estart estop de degauss".split())
    result = read.get_opts()
    dirs = read.get_args()
    
    return result, dirs


def get_oldest_file(files, _invert=False):
    """ Find and return the oldest file of input file names.
    Only one wins tie. Values based on time distance from present.
    Use of `_invert` inverts logic to make this a youngest routine,
    to be used more clearly via `get_youngest_file`.
    """
    gt = operator.lt if _invert else operator.gt
    # Check for empty list.
    if not files:
        return None
    # Raw epoch distance.
    now = time.time()
    # Select first as arbitrary sentinel file, storing name and age.
    oldest = files[0], now - os.path.getctime(files[0])
    # Iterate over all remaining files.
    for f in files[1:]:
        age = now - os.path.getctime(f)
        if gt(age, oldest[1]):
            # Set new oldest.
            oldest = f, age
    # Return just the name of oldest file.
    return oldest[0]



def get_youngest_file(files):
    return get_oldest_file(files, _invert=True)

 


if __name__ == '__main__':
    main(sys.argv[1:])
