#! /usr/bin/env python

#Imports
#---------------------------------------------
import os
import sys
import re
import imp
import time
import operator
import numpy as np
script_dir = os.path.dirname(os.path.realpath(__file__))
package_dir = os.path.join(script_dir, os.pardir, os.pardir)
sys.path.append(os.path.join(script_dir, os.pardir, 'main'))
import ElecStructClass as ESC
inp = imp.load_source('inputfile', os.path.join(package_dir, 'Modules', 'Input', 'inputfile.py'))
cl = imp.load_source('Cline', os.path.join(package_dir, 'Modules', 'Input', 'Cline.py'))

#---------------------------------------------

def main(argv):

    projreal_exe = os.path.join(package_dir, 'Src', 'CP-RealProj', 'realproj.x')
    print "\n lDOS-CP calculation script"

    #Main Directory
    maindir = os.getcwd()

    #Command Line options, arguments
    inv, dirs = startup(argv)

    tempfile = './ldos'
    #**********************************************************************
    #Loop through the directories
    #**********************************************************************
    for curdir in dirs:
        try:
            os.chdir(curdir)
            print "\n QE Output Directory:", curdir
        except OSError:
                    print " Warning:", curdir, "is Not a Directory"

        #Find the Correct Save directory
        p = re.compile('(\w+)_\d+\.save')
        lastdir = ''
        if inv['savedir']:
            m = p.search(inv['savedir'])
            if m:
                lastdir = inv['savedir']
            else:
                print "ERROR: savedir {} must be in the *_<NUM>.save format!".format(inv['savedir'])
                sys.exit(1)
        else:
            found = []
            for name in os.listdir(os.curdir):
                m = p.search(name)
                if m and os.path.isdir(name):
                    found.append(name)
            lastdir = get_youngest_file(found)

        if not lastdir:
            print "   No savedir found, skipping directory"
            os.chdir(maindir)
            continue

        m = p.search(lastdir)
        prefix = m.group(1)
        try:
            ldos = ESC.xml(os.path.join(os.curdir, lastdir, 'data-file.xml'))
            print " Save Directory: {}".format(lastdir)
        except:
            print "   data-file.xml does not exist, skipping directory"
            os.chdir(maindir)
            continue

        #Process xml results
        try:
            nstep = int(ldos.step)
            ntyp = int(ldos.ntyp)
            nbnd = int(ldos.nbnd)
            grid = np.array([int(i) for i in ldos.fft])
            atoms = ldos.atoms
            species = ldos.species
        except NameError:
            print "  Warning: data-file.xml incorrect format, skipping directory"
            os.chdir(maindir)
            continue

        #Species tag processing
        tags = []
        for name in species:
            tags.append(name[0])
        if inv['tspecies'] not in tags:
            print " Warning: Targert species not found in", ' '.join(tags), ", skipping directory"
            os.chdir(maindir)
            continue
        else:
            tag_index = tags.index(inv['tspecies']) + 1

        #Get Number of atoms for each species
        nums = [0] * ntyp
        for line in atoms:
            for index, tag in enumerate(tags):
                if re.search("^{}".format(tag), line):
                    nums[index] += 1

        #set bstop
        if inv['bstop'] == 0:
            inv['bstop'] = nbnd

        #Check to see if the final step in *.eig and data-file.xml match
        #There are legal cases where they woudl not
        with open('{}.eig'.format(prefix)) as fe:
            ncount = 0
            for line in fe:
                if re.search('STEP:', line):
                    ncount = line.split()[1]
            if int(ncount) != int(ldos.step):
                print " Note: Final step in data-file.xml {0} and {1}.eig {2} Do Not match. Using {2}"\
                      .format(ldos.step, prefix, ncount)

        with open(tempfile + '.in', 'w') as f:
            f.write('&input\n')
            f.write('nstep       = {}\n'.format(ncount))
            f.write('tspecies    = {}\n'.format(tag_index))
            f.write('tindex      = {}\n'.format(inv['tnum']))
            f.write('radius      = {}\n'.format(inv['rcut']))
            f.write('ntyp        = {}\n'.format(ntyp))
            for index, val in enumerate(nums):
                f.write('nsp({})      = {}\n'.format(index + 1, val))
            for index, name in enumerate(tags):
                f.write("sptag({})    = '{}'\n".format(index + 1, name))
            f.write('nbnd        = {}\n'.format(nbnd))
            f.write('bstart      = {}\n'.format(inv['bstart']))
            f.write('bstop       = {}\n'.format(inv['bstop']))
            for index, val in enumerate(grid):
                f.write("grid({})     = {}\n".format(index + 1, val))
            f.write('de          = {}\n'.format(inv['de']))
            f.write('estart      = {}\n'.format(inv['estart']))
            f.write('estop       = {}\n'.format(inv['estop']))
            f.write('degauss     = {}\n'.format(inv['degauss']))
            f.write('wk          = {}\n'.format(inv['wk']))
            f.write("output      = './{}'\n".format(inv['output']))
            f.write("eigfile     = './{}'\n".format(prefix + '.eig'))
            f.write("posfile     = './{}'\n".format(prefix + '.pos'))
            f.write("celfile     = './{}'\n".format(prefix + '.cel'))
            f.write("fileroot    = '{}'\n".format(inv['froot']))
            f.write("fileext     = '{}'\n".format(inv['fext']))
            f.write("/")

        cmd = '{} {} < {} > {}'.format(inv['submit'],
                                                projreal_exe, tempfile + '.in', tempfile + '.out')
        print cmd
        os.system(cmd)
        os.chdir(maindir)

        print ""

def startup(argv):
    """
    Read in Command Line options and arguments, set them up and retrun a dictionary
    """

    usage = """

    Run the real-space porjections executable.
    Must Be started after the Modified QE-5.0.2 cp.x executable (print-ks.py).

    Usage:

    Required:
    ------------
        -i         <NUM>    : Tagret Atom Index (index starts at 1)
        -s         <NAME>   : Tagret Species Name (O, H, ...)
        -r         <NUM>    : Cutoff Radius of integration sphere (Bohr)

    Optional:
    ------------

        OUTDIRs             : outdir from Quantum Espresso List, glob, single directory

        --savedir   <NUM>    : Save-Directory Number [youngest]
        --wk       <NUM>    : Occupation Number [2.0]
        --bstart   <NUM>    : Starting Band [1]
        --bstop    <NUM>    : Ending Band [nbnd]
        --output   <NAME>   : Output File ['./ldos.dat']
        --froot    <NAME>   : Kohn-Sham File rootname ['./KS_']
        --fext     <NAME>   : Kohn-Sham File extension ['.ext']
        --submit   <NAME>   : Submission (aprun -n 4, mpirun -n 4, ..) []
    Plotting:
        --estart   <NUM>    : Starting Energy (eV) [-30]
        --estart   <NUM>    : Ending Energy (eV) [10]
        --de       <NUM>    : Energy Interval (eV) [0.01]
        --degauss  <NUM>    : Gaussian parameter for broadening [0.03]
    """

    arguments = {
        'tnum'      : ['i:', '', 0],
        'tspecies'  : ['s:', '', 0],
        'rcut'      : ['r:', '', 0],
        'savedir'    : [''  , 'savedir=', ''],
        'wk'        : [''  , 'wk=', 2.0],
        'bstart'    : [''  , 'bstart=', 1],
        'bstop'     : [''  , 'bstop=' , 0],
        'output'    : [''  , 'output=', './ldos.dat'],
        'froot'     : [''  , 'froot=' , './KS_'],
        'fext'      : [''  , 'fext='  , '.dat'],
        'submit'    : [''  , 'submit=', ''],
        'estart'    : [''  , 'estart=', -30.0],
        'estop'     : [''  , 'estop=' ,  10.0],
        'de'        : [''  , 'de='    ,  0.01],
        'degauss'   : [''  , 'degauss=' ,  0.03],
    }

    read = cl.Args(argv, arguments, usage)
    read.convert_ints("tnum bstart bstop".split())
    read.convert_floats("rcut wk estart estop de degauss".split())
    result = read.get_opts()
    dirs = read.get_args()

    if result['tnum'] == 0 or result['tspecies'] == 0 or result['rcut'] == 0:
        print "ERROR: Required Values Missing!"
        print usage
        sys.exit(0)
    return result, dirs


def get_oldest_file(files, _invert=False):
    """ Find and return the oldest file of input file names.
    Only one wins tie. Values based on time distance from present.
    Use of `_invert` inverts logic to make this a youngest routine,
    to be used more clearly via `get_youngest_file`.
    """
    gt = operator.lt if _invert else operator.gt
    # Check for empty list.
    if not files:
        return None
    # Raw epoch distance.
    now = time.time()
    # Select first as arbitrary sentinel file, storing name and age.
    oldest = files[0], now - os.path.getctime(files[0])
    # Iterate over all remaining files.
    for f in files[1:]:
        age = now - os.path.getctime(f)
        if gt(age, oldest[1]):
            # Set new oldest.
            oldest = f, age
    # Return just the name of oldest file.
    return oldest[0]


def get_youngest_file(files):
    return get_oldest_file(files, _invert=True)

if __name__ == '__main__':
    main(sys.argv[1:])
