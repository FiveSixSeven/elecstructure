#! /bin/bash

# Bash wrapper script for the print-ks.py and xsf.py scripts.

#----------------------------------
#INPUTS
#1) print-ks.py $PRINKS_OPT -d <Loop-thru-dirs> $PRINTKS_BANDS
#2) xsf.py $PRINTXSF_OPT -d <Loop-thri-dirs> $@
#----------------------------------
PRINKS_OPT="--submit=''"
PRINKS_BANDS=""
XSF_OPT=""
#----------------------------------

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function check_error {
   if [ $? -ne 0 ]; then
      echo "Errors Encoutnered in Print KS, exiting"
      exit 2
   fi
}

#----------------------------------
# Command Line
#----------------------------------
usage()
{
cat << EOF

Bash wrapper script for the print-ks.py and xsf.py scripts.
To modify options for either indivdual python script you must modify this bash script

Usage:

ARGS:       Bands list (ALl if nothing listed)

OPTIONS:
   -d       Directory to run script [current directory]
   -s       Save Kohn Sham States, large amount of memory! [No Save]
   -h       Help
EOF
}

#Defaults
SAVE=0
DIRS='.'

while getopts “sh” OPTION
do
     case $OPTION in
         h)
             usage
             shift
             exit 1
             ;;
         s)
             SAVE=1
             shift
             ;;
         d)
             DIRS=$1
             shift
             ;;
         ?)
             usage
             exit 2
             ;;
     esac
done

if [ $SAVE -eq '0' \]; then
   echo " All KS*.dat Files will be deleted"
else
   echo " All KS*.dat Files will be SAVE, High Memory"
fi
#******************************************************************************
# Main Loop
#******************************************************************************
for dir in $DIRS; do

   echo "*****************************************************************"
   echo "   $dir                               "
   echo "*****************************************************************"

   echo "-----------"
   python $DIR/print-ks.py $PRINTKS_OPT -d $dir $PRINTKS_BANDS
   check_error

   echo "-----------"
   python $DIR/xsf.py $XSF_OPT -d $dir $@
   check_error

   #remove the KS.dat file to save space
   if [ $SAVE -eq '0' ]; then
      rm $dir/KS*.dat
   fi

done
#******************************************************************************
