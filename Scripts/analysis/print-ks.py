#! /usr/bin/env python

#Imports
#---------------------------------------------
import os
import sys
import re
import time
import operator
import imp
script_dir = os.path.dirname(os.path.realpath(__file__))
package_dir = os.path.join(script_dir, os.pardir, os.pardir)
sys.path.append(os.path.join(script_dir, os.pardir, 'main'))
import ElecStructClass as ESC
inp = imp.load_source('inputfile', os.path.join(package_dir, 'Modules', 'Input', 'inputfile.py'))
cl = imp.load_source('Cline', os.path.join(package_dir, 'Modules', 'Input', 'Cline.py'))

#---------------------------------------------

def main(argv):

    printks_exe = os.path.join(package_dir, 'Src', 'QE-5.0.2-PrintOrbitals-Single', 'bin', 'cp.x')
    print "\n Print Kohn-Sham states calculation script"

    #Main Directory
    maindir = os.getcwd()

    #Command Line options, arguments
    inv, bands = startup(argv)

    #**********************************************************************
    #Loop through the directories
    #**********************************************************************
    tempfile = 'ksprint'
    for curdir in inv['dirs']:
        try:
            os.chdir(curdir)
            print "\n Directory:", curdir
        except OSError:
                    print " Warning:", curdir, "is Not a Directory"

        #Find the correct cp.*.out file and the corresponding cp.*.in file (lastfile)
        if inv['cpin']:
            if os.path.isfile(inv['cpin']):
                lastfile = inv['cpin']
            else:
                print " {} file Not found, skipping directory".format(inv['cpin'])
                os.chdir(maindir)
                continue
        else:

            found = []
            for name in os.listdir(os.curdir):
                p = re.compile('cp.*\.out\d+')
                m = p.search(name)
                if m:
                    found.append(name)

            if not found:
                print " No cp.out files found, skipping directory"
                os.chdir(maindir)
                continue
            else:
                #find the corresponding input to the found output file
                lastfile = get_youngest_file(found)
                p = re.compile('(cp.*)\.out(\d+)')
                m = p.search(lastfile)
                if m:
                    lastfile = m.group(1) + '.in' + m.group(2)
                else:
                    print " Corresponding Input file to {} Not found ".format(lastfile)
                    os.chdir(maindir)
                    continue

        if not lastfile:
            print "   No outdir found, skipping directory"
            os.chdir(maindir)
            continue

        #read-in youngest file
        print " Using {} -> {}".format(lastfile, tempfile + '.in')
        with open(lastfile) as fo:
            lastfile_lines = fo.readlines()

        #Get the number of bands, if not on command line
        if not inv['outdir']:
            #open lastfile, find prefix and ndr (if there)
            prefix = ''
            ndr = ''
            #Find the calculation type prefix and ndr in lastfile
            for line in lastfile_lines:
                p = re.compile("^\s*prefix\s*=\s*(?:'|\")(.*)(?:'|\"),?\s*$")
                m = p.search(line)
                if m:
                    prefix = m.group(1)

                p = re.compile('^\s*ndr\s*=\s*(.*),?\s*$')
                m = p.search(line)
                if m:
                    ndr = m.group(1)

                p = re.compile("^\s*outdir\s*=\s*(?:'|\")(.*)(?:'|\"),?\s*$")
                m = p.search(line)
                if m:
                    outdir = m.group(1)

            if not prefix:
                prefix = 'cp'
            if not ndr:
                ndr = 50
                if not outdir:
                    outdir = '.'
            #Open Correct data-file.xml get the number of bands
            name = os.path.join(outdir, prefix + '_' + str(ndr) + '.save')
        else:
            name = inv['outdir']

        try:
            path = os.path.join(os.curdir, name, 'data-file.xml')
            print " Bands from {}".format(path)
            ks = ESC.xml(path)
            if not bands:
                tot_bands = int(ks.nbnd)
        except (NameError, IOError):
            print "  Warning: data-file.xml Not Found, skipping directory"
            os.chdir(maindir)
            continue

        #rewrite calculation and ndw as well as append the KSOUT information on the new tempfile array
        lastfile_lines_new = []
        ndw_found = False
        for line in lastfile_lines:
            #Calculation input
            pcalc = re.compile("(^\s*calculation\s*=)\s*['\"](.*)['\"],?\s*$")
            mcalc = pcalc.search(line)
            pndw = re.compile("(^\s*ndw\s*=)\s*(.*),?\s*$")
            mndw = pndw.search(line)
            #if calculation input
            if mcalc:
                lastfile_lines_new.append(mcalc.group(1) + " 'cp'\n")
            #ndw, if it exists
            elif mndw:
                lastfile_lines_new.append(mndw.group(1) + " 60\n")
                ndw_found = True
            else:
                lastfile_lines_new.append(line)
        lastfile_lines_new.append("\n\nKSOUT\n")
        if bands:
            lastfile_lines_new.append("{} select\n".format(len(bands)))
            lastfile_lines_new.append(" ".join(bands))
        else:
            lastfile_lines_new.append("{} all\n".format(tot_bands))

        #if the ndw was not found, we MUST add after CONTROL
        if not ndw_found:
            for index, line in enumerate(lastfile_lines_new):
                if re.search('CONTROL', line):
                    lastfile_lines_new.insert(index + 1, '   ndw            =  60\n')

        #write the new tempfile array to file
        with open(tempfile + '.in', 'w') as fa:
            for line in lastfile_lines_new:
                fa.write(line)

        print " Running calculation..."

        cmd = '{} {} < {} > {}'.format(inv['submit'], printks_exe, tempfile + '.in', tempfile + '.out')
        print cmd
        os.system(cmd)
        os.chdir(maindir)

def startup(argv):
    """
    Read in Command Line options and arguments, set them up and retrun a dictionary
    """

    usage = """

    Output Kohn-Sham States from a previous Electronic calculation using
    QE-5.0.2-PrintOrbitals-Single (No MD Steps)

    Usage:


    Optional:
    ------------

        BANDS                : Individual Kohn-Sham States to produce [All]

        -d         <NAME>   : Directory, glob, list

        --cpin     <NAME>   : Output from last cp.x calculation [search for youngest]
        --outdir   <NAME>   : Savedir to read [construct from cpin]
        --submit   <NAME>   : Submission (aprun -n 4, mpirun -n 4, ..) []

    """

    arguments = {
        'dirs'      : ['d:', '', ''],
        'outdir'    : ['n:', '', 0],
        'cpin'      : [''  , 'cpin='   , ''],
        'submit'    : [''  , 'submit=' , ''],
    }

    read = cl.Args(argv, arguments, usage)
    result = read.get_opts()
    args = read.get_args()

    if not result['dirs']:
        print " Note: Using Current Directory!"
        result['dirs'] = ['./']
    else:
        result['dirs'] = result['dirs'].split()
    return result, args


def get_oldest_file(files, _invert=False):
    """ Find and return the oldest file of input file names.
    Only one wins tie. Values based on time distance from present.
    Use of `_invert` inverts logic to make this a youngest routine,
    to be used more clearly via `get_youngest_file`.
    """
    gt = operator.lt if _invert else operator.gt
    # Check for empty list.
    if not files:
        return None
    # Raw epoch distance.
    now = time.time()
    # Select first as arbitrary sentinel file, storing name and age.
    oldest = files[0], now - os.path.getctime(files[0])
    # Iterate over all remaining files.
    for f in files[1:]:
        age = now - os.path.getctime(f)
        if gt(age, oldest[1]):
            # Set new oldest.
            oldest = f, age
    # Return just the name of oldest file.
    return oldest[0]


def get_youngest_file(files):
    return get_oldest_file(files, _invert=True)

if __name__ == '__main__':
    main(sys.argv[1:])
