#! /usr/bin/env python

import os, sys, re, time, operator, shutil, imp, math, commands
script_dir = os.path.dirname(os.path.realpath(__file__))
package_dir = os.path.join(script_dir, os.pardir, os.pardir)
cl  = imp.load_source('Cline', os.path.join(package_dir, 'Modules', 'Input', 'Cline.py'))

def main(argv):

    print "--------------------------------------------"
    print "         Multiple Submission Script         "
    print "--------------------------------------------\n"

    #Main Directory
    maindir = os.getcwd()
        
    #Main script ES
    es = os.path.join(package_dir, 'ES')

    #Command Line options, arguments
    inv, inputfile = startup(argv)

    print " Starting Step:        ", inv['start']
    print " Total Number of Steps:", inv['num']
    print " MD intervals:         ", inv['int']
    print " Jobs per Submission:  ", inv['persub']
    print " Submit:               ", not inv['nosub']
    print " Submission Name:      ", inv['subname']
    print ""

    #Read in the inputfile
    try:
        with open(inputfile) as f:
            input_lines = f.readlines()
    except IOError:
        print "ERROR: Input file not found"

    #Open log
    submit_log = 'submit.log'
    if os.path.isfile(submit_log):
        os.remove(submit_log)
    os.system('touch {0}'.format(submit_log))

    #loop over the all the steps
    adjnum = math.ceil(inv['num']/float(inv['persub']))
    ncount = 0
    for i in range(int(adjnum)):
        ncount += 1
        curstart = i*inv['persub']*inv['int'] + inv['start']

        #check to see if total number of jobs has been exceeded
        if ncount*inv['persub'] > inv['num']:
            #adjpersub
            adjpersub = inv['persub'] - (ncount*inv['persub'] - inv['num'])
        else:
            adjpersub = inv['persub']

        print "\n {})".format(ncount)
        print " Start Number {:8d}, Total Number {:8d}, Interval Number {:8d}"\
                                                            .format(curstart, inv['num'], adjpersub)

        #Re-write the input-file
        with open(inputfile, 'w') as fin:
            for line in input_lines:

                #Find the the names start, int , nsteps
                found = False
                names = 'start int nsteps'.split()
                for name, val  in zip(names, (curstart, inv['int'], adjpersub)):
                    p = re.compile('(\s*{}\s*=\s*)(.*),?'.format(name))
                    m = p.search(line)
                    if m:
                        fin.write( m.group(1) + str(val) + '\n')
                        found = True
                        break
                if not found:
                    fin.write(line)

        #Run the Electronic Structure Calculation
        cmd = ' {} --force {}' .format(es, inputfile)
        print cmd + ' >> {}'.format(submit_log) #Will be eventually appended
        status, output = commands.getstatusoutput(cmd)

        #Submit the file
        if not inv['nosub']:

            #loop through the output, find the PBS Submission File
            submit_file = ''
            for line in output.split('\n'):
                if re.search('Creating PBS Submission file:', line):
                    submit_file = line.strip().split()[4]

            if submit_file:
                #Change to the directory and submit
                os.chdir(os.path.join(os.curdir, 'Submit-Files'))
                cmd = " {} {}".format(inv['subname'], submit_file)
                print cmd
                os.system(cmd)
                os.chdir(maindir)
            else:
                print " WARNING: No Submit File Created, Skipping Submission!"

        with open(submit_log, 'a') as fo:
            fo.writelines(output)

    print "\n\nProgram Complete\n"



def startup(argv):
    """
    Read in Command Line options and arguments, set them up and retrun a dictionary
    """

    usage = """

    Submit Multiple Electronic Structure Calculations at Varying Steps and Intervals

    Usage:

    Required
    ------------

        --start    <NUM>    : Starting Molecular Dynamics Step 
        --int      <NUM>    : Interval to move through the MD trajectory
        --num      <NUM>    : Total number of Electronic Structure calculations
        --persub   <NUM>    : Number of jobs to run per submission
         
        IN-FILE             : Input file for the Electronic Structure Submission

    Optional
    ------------
        --nosub             : Turns off the automatic submission
        --subname  <NAME>   : Automatic submission executable [qsub]
    """
    
    arguments = {
        'start'     : ['', 'start='   , ''],
        'int'       : ['', 'int='     , 0],
        'num'       : ['', 'num='     , 0],
        'persub'    : ['', 'persub='  , ''],
        'nosub'     : ['', 'nosub'    , False],
        'subname'   : ['', 'subname=' , 'qsub'],
    }

    read = cl.Args(argv, arguments, usage)
    result = read.get_opts()
    args = read.get_args()
    
    #Find Required 
    if not result['start']:
        print "ERROR: start value not defined, determines the start MD step!"
        print usage
        sys.exit(1)
    elif not result['int']:
        print "ERROR: int value not defined, determines the interval in the MD steps!"
        print usage
        sys.exit(1)
    elif not result['persub']:
        print "ERROR: persub value not defined, number of jobs per submission!"
        print usage
        sys.exit(1)

    read.convert_ints("start int num persub".split())

    if len(args) == 0:
        print "ERROR: Inputfile must be defined"
        print usage
        sys.exit(1)
    elif len(args) > 1:
        print "ERROR: Multiple input files not allowed"
        print usage
        sys.exit(1)

    return result, args[0]


if __name__ == '__main__':
    main(sys.argv[1:])
