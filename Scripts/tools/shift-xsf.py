#! /usr/bin/env python

from __future__ import print_function, division
import os
import sys
sys.path.insert(0, '/global/scratch2/sd/cswartz/Cl-ElectStruct/elecstructure/Modules')
import QE.xsf as X


def main(catom, states):
    for file in states:
        if os.path.isfile(file):
            print("File :", file)
            plot = X.Xsf(file)
            plot.build(file+'-mod', cid=catom)
        else:
            print("Warning:", file, "is not a file, skipping")



if __name__ == "__main__":
    main(sys.argv[1:])
