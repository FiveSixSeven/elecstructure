#! /bin/bash

#Mainloop
for dir in $@; do

   if [ ! -d $dir ]; then
      continue
   fi

   cd $dir

   rm -v KS_*dat KS-*dat

done
