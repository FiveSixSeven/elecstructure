#! /bin/usr/env python
"""
A Group of Classes for the Electronic Structure Calculations

Classes:
    2) ElecStruct   - Main class used to set-up the electronic structure calculations
    1) xml          - class to parse the xml Quantum Espresso save file data-file.xml
    2) eigfile      - Return the eigenvalues for a particular step, prefix and dir

    
    Written by:
    Charles Swartz
    August 2013
"""

import re, sys, os, imp, copy, shutil
import xml.etree.ElementTree as ET
script_dir = os.path.dirname(os.path.realpath(__file__))
package_dir = os.path.join(script_dir, os.pardir, os.pardir)
inp = imp.load_source('inputfile', os.path.join(package_dir, 'Modules', 'Input', 'inputfile.py'))


#----------------------------------------------------------------------------
# Main Utilily modules
#----------------------------------------------------------------------------
class ElecStruct(object):
    """
    Note: All methods and attributes starting with '_' are not meant to be over-written
    or used directly by future classes
    """

    def __init__(self, inputfile, force):
        """
        Instance Initialization 
            1)Read input file 
            2)set inheritable and on-inheritable attributes
        """
        #If there will be user interaction
        self.user = not force           

        #read input file
        self.vars = inp.namelist(inputfile)

        #set Main attributes 
        self._setattrs()

        #read calcfile
        self._read_calcfile()



    def _setattrs(self):
        """
        Set-up main attributes, Non-Inheritable attributes
        """
        #Major attributes
        self.steps = []                 #Number of different MD steps for ES calculation
        self._maindir = os.getcwd()     #Main Directory of running script

        #Job-Run Options
        self._preamble = []             #Submission Preamble
        self._exeopts = []              #Executable options (aprun, mpirun) for eac calc [calcrun]
        self._exe = []                  #Executable for each calc [calcrun]
        self._progopts = []             #Program Options for each calc [calcrun]

        #Required Main Directories, NOT inside the temp directory
        self._reqd = {
            'work'    : 'Work-Dirs',
            'dat'     : 'Data-Files',
            'submit'  : 'Submit-Files'
        }

        #Templates and Files
        self.templates = {}              #All Templates [calcrun] 
        self.files = {}                  #All Files (created by create_files)
        self._templates_order = []       #Templates in the calc order, from calcfile file [calcrun]
        self._fileroots_order = []       #InOut-Roots in the calc order, from calcfile file [calcrun]
                                        

        #Required values this will be extended by the individual calculation
        self.reqv = {
            'name'          : '',
            'calcnum'       : 0,
            'calcfile'      : '',
            'runfile'       : '',
            'prefile'       : '',
            'posfile'       : '',
            'start'         : 0,
            'nsteps'        : 0,
            'int'           : 0,
            'tempdir'       : ''
        }

        #Initial Consistency check for input vars (self.vars) and req vars (self.reqv)
        self.concheck()

        #Find executables (exe1, exe2, ...), 
        #Needs a second read through using calcnum executables
        for i in range(int(self.reqv['calcnum'])):
            self.reqv['exe' + str(i+1)] = None
        self.concheck()


        #set up self.steps 
        scount = 0
        while scount < self.vars['nsteps']:
            self.steps.append(self.vars['start'] + self.vars['int']*scount)
            scount += 1

        #convert both self.vars['anums'] and self.vars['atoms'] into lists
        #Small workaround: inputfile will return an int type if there is only one value
        if isinstance(self.vars['anums'], int):
            self.vars['anums'] = [self.vars['anums']]
        else:
            self.vars['anums'] = [int(i) for i in self.vars['anums'].strip().split()]
        self.vars['atoms'] = self.vars['atoms'].split()

        #Check that we have specified the reqv[anums] and reqv[atoms] correctly
        self._atomcheck()




    def concheck(self):
        """
        Check that all required variables in self.reqv are defined in in self.vars
        Will be check during the initialization and can also be check after inheritance
        """
        for name in self.reqv:
            if name not in self.vars.getall():
                raise ElecError(name, 'Not defined in inputfile!')
            



    def create_dirs(self):
        """
        Create Main Directories in self._reqd
        """
        print "\n Creating Directories..."
        for key, val in self._reqd.items():
            if not os.path.isdir(val):
                print "  {}".format(val)
                os.mkdir(val) 




    def _read_calcfile(self):
        """
        Read the Calculation File self.vars['calcfile']
        """
        try:
            with open(self.vars['calcfile']) as f:
                ncount = 0 
                for line in f:
                    #Loop over blank, comment lines
                    if re.search('(^#|^\s*$)', line.strip()):
                        continue
                    else:
                        temp = [ i.strip() for i in line.split(':')]
                        if len(temp) == 4:
                            #templates must have .template extension
                            if not re.search('.*\.template$', temp[0]):
                                raise ElecError('calcfile Error', 
                                                    'templates must have .template extension')
                            if temp[0] in self.templates.keys(): 
                                raise ElecError('calcfile Error', 
                                                    'template {} appears twice'.format(temp[0]))
                            #Set up all calcfile info
                            self.templates[temp[0]] = ''
                            self._templates_order.append(temp[0])
                            root, ext = os.path.splitext(temp[0])
                            self._fileroots_order.append(root)
                            self._exeopts.append(temp[1])
                            #use number to get the exe name
                            self._exe.append(self.vars['exe'+ str(temp[2])].strip())
                            self._progopts.append(temp[3])
                            ncount += 1

                        else:
                            raise ElecError('calcfile Error', 'Incorrect format of calcfile')

        except IOError:
                raise ElecError('calcfile Error', 'Calcrule {} does Not Exist'\
                                                                .format(self.vars['calcfile']))
        if ncount != self.vars['calcnum']:
            raise ElecError('calcfile Error', \
            'calcnum {}, calculations in calcfile {}'.format(self.vars['calcnum'], ncount))




    def read_templates(self):
        """
        Read in all templates defined in self.templates.
        Templates will be saved as a list of strings in the value portion of self.templates
        """
        #read in templates
        print "\n Reading Templates..."
        for key in self._templates_order:
            print "  {}".format(key)
            filename = os.path.join(self.vars['tempdir'], key)
            try:
                with open(filename) as f:
                    self.templates[key] = f.readlines()
            except IOError:
                raise ElecError('Template Error', '{} Does Not Exist'.format(filename))




    def show_templates(self, **exclude):
        """
        Show a side by side comparison of all of the template files

        Inputs:
            1) exclude - a dict of variables not to display,
        these excluded varaiables SHOULD idly be the ones 
        that are being replaced by self.create_file.
        They will be printed out as ALTERED Values
        """
        #if templates are not already read in do so
        if not self.templates:
            self.read_templates()

        namelists = ("CONTROL", "SYSTEM", "ELECTRONS", "IONS", "CELL")
        cards = ("ATOMIC_SPECIES", "K_POINTS")

        #Show variables that will Not be check, those that should be altered by the program
        if exclude:
            print "\n Variables that WILL be altered..."
            for k, v in exclude.items():
                print "  {:20s} = {}".format(k, v)

        #create tvars (template variables) dict, where each tempkey (temlate name) points to another 
        #dictionary whose keys are either QE namelist and point to a third another dictonary of 
        #variables and values, or QE cards and point to a list of the variables
        #
        # tvars: { 
        #     temp1 : { 
        #         namelist1 : {
        #            variable1 : value1, 
        #            variable2 : value2, 
        #            ...
        #         },
        #         namelist2 : {
        #            variable1 : value1, 
        #            variable2 : value2, 
        #            ...
        #         },
        #         ...,
        #         card1 : [
        #            line1
        #            line2
        #         ],
        #         card2 : [
        #            line1
        #            line2
        #         ]
        #     },
        #     temp2,
        #     .....
        # }
        #
        print "\n Variables that will NOT be altered (please confrim)..."
        tvars = {}
        for tempkey in self._templates_order:

            #add template to tvars and make iterable 
            tvars[tempkey] = {}
            temp = iter(self.templates[tempkey])

            #search through the template
            for line in temp:
                if not line.strip():
                    continue
                try:
                    #check name lists
                    for name in namelists:
                        #check if begining of a namelist
                        if re.search("^\s*&{}\s*$".format(name), line, re.IGNORECASE):
                            tvars[tempkey].update({name : {} })
                            line = temp.next()
                            #loop trhough the namelist, forget fortran comment line (!)
                            while not re.search('^\s*/\s*$', line):
                                if not re.search('^\s*!', line):
                                    var, val = self._get_single_variable(line)
                                    if var not in exclude.keys():
                                        tvars[tempkey][name][var] = val 
                                line = temp.next()

                    #check cards
                    for card in cards:
                        #Check the begining of a card
                        p = re.compile("^\s*{}\s*(.*)$".format(card), re.IGNORECASE)
                        m = p.search(line)
                        if m:
                            tvars[tempkey].update({card : [] })
                            #check for on the card line addative (ex {bohr})
                            if m.groups()[0]:
                                tvars[tempkey][card].append(m.groups()[0].strip())
                            line = temp.next()
                            while not re.search('^\s*$', line):
                                if not re.search('^\s*!', line):
                                    tvars[tempkey][card].append(line.strip()) 
                                line = temp.next()
                except StopIteration:
                    pass


        #print out all variables
        for name in namelists:

            #create a master list of all variables in the namelists from
            #ALL templates
            master = []
            for tempkey in tvars.keys():
                try:
                    for v in tvars[tempkey][name].keys():
                        master.append(v)
                except KeyError:
                    #in case the key doesn't exist in one of the templates
                    pass
            master = set(master)
            if not len(master):
                continue

            #print namelist on a new line
            print "  " + str(name).ljust(20),

            #print templates on the same line
            for tempkey in self._templates_order:
                print str(tempkey).center(20),
            print ""

            #print variables : value_temp1  value_temp2  value_temp3 ...
            for v in master:
                print "   " +  str(v).ljust(19),
                for tempkey in self._templates_order:
                    try:
                        #truncate the line if to long
                        if len(tvars[tempkey][name][v]) > 18:
                            ptemp = '-'+str(tvars[tempkey][name][v][-16:]) 
                            print ptemp.center(20),
                        else:
                            print str(tvars[tempkey][name][v]).center(20),
                    except KeyError:
                        print "--".center(20),
                print ""

            self.user_confrim(extra="  ", end=True)
            print ""





    def _get_single_variable(self, line):
        """
        Return the value of a single input variable and value from a single line
        """
        p = re.compile('^\s*(.*)\s*=\s*(.*),?\s*$')
        m = p.search(line)
        if m:
            #remove possible whitespace
            var = m.groups()[0].strip()
            val = m.groups()[1].strip()
            #remove possible ending comma
            val = val.strip(',')
            #remove possible single quotes
            val = val.strip("'")
            #remove possible double quotes
            val = val.strip('"')

            return var, val

        return None




    def _get_single_card(self, card, tempkey, warn=True):
        """
        Return the value(s) of a single input card from a single template

        Input:
            1) card - name of card
            2) tempkey - template key for self.templetes
            3) warn - This will display a warning if the card is not found
        """
        #if templates are not already read in do so
        if not self.templates:
            self.read_templates()

        #check to be sure tempkey is a template
        if tempkey not in self.templates.keys():
            raise IntError("_get_single_card", "{} not a defined template".format(tempkey))

        retcard = []
        p = re.compile(card)
        for i, v in enumerate(self.templates[tempkey]):
            m = p.search(v)
            #found card, read contents
            if m:
                ncount = i + 1 #one beyond the card name
                while True:
                    line = self.templates[tempkey][ncount].strip()
                    if line:
                        retcard.append(line)
                        ncount += 1
                    else:
                        return retcard

        if warn:
            "\n WARNING: card {} not found in {}".format(card, tempkey)
            return False




    def _atomcheck(self):
        """
        Check to make sure that both the self.vars['atoms'] and self.vars['anums']
        are equal.
        """
        #TODO read each template and scan for the number of species
        if len(self.vars['atoms']) != len(self.vars['anums']):
            raise ElecError("Atomic Setup", "'atoms' and 'anums' Must Match")




    def check_atom_order(self):
        """
        Read through ALL templates to determine if the Atoms in ATOMIC_SPECIES card are in
        the same order as self.vars['atoms']

        If the ATOMIC_SPECIES tag is not, ignor template file

        """
        print "\n Checking all templates atomic order", ', '.join(self.vars['atoms']), '...'
        species = {}
        for temp in self.templates:
            val = self._get_single_card('ATOMIC_SPECIES', temp, warn=False)
            labels = []
            if val is not None:
                for line in val:
                    labels.append(line.split()[0])
                species.update({temp : labels })
               
        #create erro message if needed
        msg = "\n"
        for key in species:
            msg += "  {:20s} => ".format(key) + ' '.join(species[key]) + '\n'

        #check number
        for v in species.values():
            if len(v) != len(self.vars['atoms']):
                raise ElecError('Atomic Label order', msg)

        #Check order    
        for i,v in enumerate(self.vars['atoms']):
            for key in species:
                if species[key][i] != v:
                    raise ElecError('Atomic Label order', msg)
            print "  {:4s}".format(v)




    def find_atomic_positions(self):
        """
        Find the atomic positons for all steps in self.steps.
        return a dictionary pinting to a list of lines (with newlines)
        for each step (self.steps)

        Important Note: The return type is a dictionary with the step numbers as keys
        return['390'] = [ 'O 1.0 1.0 1.0\n' 'H 2.0 2.0 2.0\n' ...]
        This is done so that it is easy to copy these step values latter

        """
        print "\n Searching for atomic positions..."

        #apo is a DICTIONARY where the key is the step number and the value is a list of
        #strings with the positions of each atom starting with the atomic symbol
        apos = {}
        with open(self.vars['posfile']) as fpos:

            while True:

                line = fpos.readline()
                if not line or len(apos) == len(self.steps):
                    break
                #step, tiem line has ONLY two elements
                if len(line.strip().split()) == 2:
                    step, time = line.strip().split()
               
                if int(step) in self.steps:
                    print "  step: {}".format(step)
                  
                    #for each atom number in self.vars
                    for index, na in enumerate(self.vars['anums']):
                        for i in range(na):
                            #read next line and check for errors
                            line = fpos.readline()
                            if len(line.strip().split()) == 2:
                                raise ElecError('Position File', 'Incorrect number of atoms!')
                            else:
                                #add the atom label to the fron of the line
                                line = '{:4s}'.format(self.vars['atoms'][index]) + line
                                try:
                                    apos[step].append(line)
                                except KeyError:
                                    apos[step] = [line]

        for test in self.steps:
            if str(test) not in apos:
                raise ElecError('Position File', 'step {} not found in position file!'.format(test))
        return apos




    def create_steps(self, positions, swap, delete):
        """
        """
        print "\n Creating Files..."
        ncount = 1 
        temp = self.steps[:]
        for step in temp:
        
            files = []
        
            print "\n  {}) Step {}:".format(ncount, step)
            print "  ---------------------------\n"
            for tempkey in self._templates_order:
                files.append(self._create_file(step, tempkey, positions[str(step)], **swap))
            self._build_calc(step, files, delete=delete)
            ncount += 1




    def _create_file(self, step, tempkey, atoms, show=True, **replace):
        """
        Write files using the key of the template as the rootname of the file, 
        appended with step number. Because different files may need different 
        replace values this function only works on one file at a time

        Inputs:
            1) step from position file
            2) self.templates dict key
            3) atoms as a list of list, None if not needed (found with self.find_atomic_positions)
            4) replace = a dict of all values that need to be replaced

        """
        if tempkey not in self.templates:
            raise IntError("create_file", "{} not found in template list".format(tempkey))

        #if templates are not already read in do so
        if self.templates[tempkey] is None:
            self.read_templates()

        #InputFile name creation
        root, ext = os.path.splitext(tempkey)
        filekey = root + '.in' + str(step)
        self.files[filekey] = copy.deepcopy(self.templates[tempkey])
                                
        if len(replace.keys()) != 0:
            print "   {:10s}".format(filekey)
            #replace those values in the replace dictionary
            for key, values in replace.items():
                found = False
                for index, line in enumerate(self.files[filekey][:]):
                    #nnote the use of re.escape
                    p = re.compile('(\s*{}\s*=).*'.format(re.escape(key)))
                    m = p.search(line)
                    if m:
                        found = True
                        if show:
                            print "    {:13s} => {}".format(key, values)
                        self.files[filekey][index] = m.groups()[0] + " {}".format(values) + '\n'
                if not found:
                    print "    {:13s} Not Found".format(key)
            print ""
        else:
            print "   {:10s}".format(filekey)

        #replace atomic coordinates
        for index, line in enumerate(self.files[filekey][:]):
            if atoms and re.search('ATOMIC_POSITIONS', line):
                ncount = index
                for pos in atoms:
                    self.files[filekey].insert(ncount + 1, pos)
                    ncount += 1
      
        return filekey

      



    def _write_all_files(self, filekeys):
        """
        Once all files have been created/alterd via self.create_file,
        write all files at once.

        Important Note: This will wirte files to the CUrrent Directory

        Input:
            1) filekeys all files that need to be written

        This method is ususally called from the build_calc method
        """

        print "\n   Writing files..."
        for filekey in filekeys:
            openfile = os.path.join(os.curdir, filekey)
            print "   {}".format(filekey)
            with open(openfile, 'w') as fo:
                fo.writelines(self.files[filekey])





    def _build_calc(self, step, filekeys, delete=False):
        """
        Build partiuclar step calculation

        Input:
            1) step - current step
            2) filekeys - from created files
            3) delete - will prompt user to overwite old step dir, self.user 
            must be True!

        This will setup the basics of the the calculation.
        1)Check Main Directories
        2)Create Temp Directory, chdir
        3)create all files from self.files
        4)create qsub from self.qsub
        5)submit file
        6)chdir back to maindir
        """
        def remove_step(step, msg):
            print msg
            del self.steps[self.steps.index(step)]

        try:
            #create directory, change dir
            curdir = os.path.join(self._maindir, self._reqd['work'], \
                               "step{}".format(step))

            #check if the cridur exists, if so check to see what to do next
            if os.path.isdir(curdir):
                print "   WARNING:\n   {} already exists!".format(curdir)

                #Check to see if we should replace old directories
                if delete:
                    print "   Directory will be overwritten",  
                    if self.user:
                        if self.user_confrim(extra="- "):
                            print "   ...Removing old step{} directory".format(step)
                            shutil.rmtree(curdir)
                        else:
                            remove_step(step, "   ...skipping step {}".format(step))
                            return False
                    else:
                        print "   ...Removing old step{} directory".format(step)
                        shutil.rmtree(curdir)
                else:
                    remove_step(step, "   ...skipping step {}".format(step))
                    return False
                print ""

            print "   Creating Work Dir:", curdir
            os.mkdir(curdir)
            os.chdir(curdir)

            #Write all files in curdir
            self._write_all_files(filekeys)

        except Exception, ierr:
            #delete the index from the self.steps list
            sys.stderr.write('ERROR: {}\n'.format(str(ierr)))
            os.chdir(self._maindir)
            sys.exit(1)

        os.chdir(self._maindir)
         




    def qsub(self):
        """
        Create and write the submit.sh PBS file to the self._reqd['submit'] directory
        using the rules defined in the Submission Run File 'runfile'

        THe general set-up of the submit script:
        -----------------------
        <preamble-file>
        cd <Work-Dir>

        for num in {1};do
            cd step$num

            <Submission-Run-File>

            cd -
        done
        -----------------------

        Match all runtime !!<name>!! tokens (name, input, output, exeopts, exe, progopts) 
        in the submission rules file and substitute their corresponding values.

        Example
        !!input1!!      --> cp.in$num
        !!exeopts1!!    --> aprun -n 128
        !!exe1!!        --> cp.x
        !!progopts1!!   --> -ntg 4
        """
        def runmatch(matchobj):
            """
            matching function
            """
            m1 = matchobj.group(1)
            m2 = matchobj.group(2)
            if m1 == 'input':
                return self._fileroots_order[int(m2)-1] + r'.in$num'
            elif m1 == 'output':
                return self._fileroots_order[int(m2)-1] + r'.out$num'
            elif m1 == 'exeopts':
                return self._exeopts[int(m2)-1]
            elif m1 == 'exe':
                return self._exe[int(m2)-1].strip()
            elif m1 == 'progopts':
                return self._progopts[int(m2)-1]
            
        qsub_total = ''
        
        #Check and see if there are actually steps in the self.steps list
        if len(self.steps) == 0:
           print "\n WARNING: No Steps to run, skipping the PBS submission script\n"
           return

        #Set submit name
        steps_label = "{}-{}".format(self.steps[0], self.steps[-1])
        try:
            submit_name = self.vars['prefix'] + self.vars['name'] + steps_label
        except KeyError:
            submit_name = self.vars['name'] + steps_label

        #Open the preamble file, must exist even if #! /bin/bash
        try:
            with open(self.vars['prefile']) as f:
                self._preamble = f.readlines()
        except IOError as ierr:
            print ierr
            raise ElecError('Preamble File', 'File {} not found!'.format(self.vars['prefile']))
        #Check for the #PBS -N  -> Job name
        for index, line in enumerate(self._preamble):
            if re.search('#PBS\s*-N', line):
                self._preamble[index] = line.strip() + ' ' + submit_name + '\n'

        #Loop header
        qsub_header = "\ncd {0}\n\nfor num in {1};do\n   cd step$num\n   pwd\n\n" .\
                                        format(os.path.join(self._maindir, self._reqd['work']), \
                                                        ' '.join([str(i) for i in self.steps]))

        #Read through the Submission rule file, replace all runtime !<name>! tokens
        try:
            with open(self.vars['runfile']) as f:
                qsub_body = ''
                #loop through each line
                for line in f:
                    temp_line = '   ' 
                    #loop each line element look for a match
                    for element in line.strip().split():
                        replace = re.sub('!!([a-zA-Z]+)(\d+)!!', runmatch, element)
                        if replace:
                            temp_line += replace + ' '
                    qsub_body += temp_line + '\n'

        except IndexError:
            raise ElecError('Submission File', 'Incorrect Format')
        except IOError:
            raise ElecError('Submission File', 'File {} not found!'.format(self.vars['runfile']))
                        
        #loop footer
        qsub_footer = "\n   cd -\ndone"
                
        #total qsub
        preamble_string = reduce(lambda x, y: x+y, self._preamble)
        qsub_total = preamble_string + qsub_header + qsub_body + qsub_footer

        #Combined all qsub elements
        outfile = os.path.join(self._maindir, self._reqd['submit'], \
                                                            'submit{}.sh'.format(steps_label))
        #Display the created submit file
        print "\n\n\n PBS Submission File..."
        for line in qsub_total.split('\n'):
            print "   |" + line
        self.user_confrim(extra="  ", end=True)

        with open(outfile, 'w') as f:
           f.write( qsub_total)
           print "\n Creating PBS Submission file:", outfile, '\n'
        


    def pause(self, extra=""):
        """
        pause the progam to show input
        """
        if self.user:
            try:
                input= raw_input( extra + "Press enter to continue:")
                len(input)
            except NameError:
                pass



    def user_confrim(self,  extra="", end=False):
        """
        Ask user if the preceedign was correct if so continue and return True, 
        if not then either return False and continue or exit the program
        Only "n/N" will count as a negative
        """
        if self.user:
            input = raw_input(extra + "Is this correct? [y]:")
            input = input.strip()
            if input == 'n' or input == 'N':
                if end:
                    print "\n\nUser Requested Exit!!\n"
                    sys.exit(0)
                return False
        return True
      

#----------------------------------------------------------------------------
# Main Utilily modules
#----------------------------------------------------------------------------
class xml(object):
    """
    A class for parsing the Quantum Espresso data-file.xml save file.

    Important: 
        
    1) This class will only work for Electronic Structure Calculations.
    Please don't use this for a particular step (STEP0, STEPM)

    2) Everything is returned as a string or a list of strings

    Atoms:
    nat         = The total number of atoms (all species)
    ntyp        = The total number of species
    atoms       = ATOMIC_POSITION card (as read-in)
    atoms_raw   = All information from atomic positions (tag, tau, species)
    species     = The ATOMIC_SPECIES card

    Cell/Configuration
    step        = The last step
    aprim       = The primitive  lattice vectors
    brav        = Bravais lattice type
    lat         = Lattice constant
    tot_charge  = Total charge of system

    Bands:
    nbnd        = Number of bands
    nspin       = Spin polarization 

    PlaneWaves:
    ecutwfc     = Kinetic energy cuttoff
    fft         = fft grid list

    """

    def __init__(self, filename, show=False):
        tree = ET.parse(filename)
        root = tree.getroot()

        #Atoms Information
        self.nat = 0                #Number of atoms
        self.ntyp = 0               #Number of species
        self.atoms = []             #ATOMIC_POSITIONS card
        self.atoms_raw = []         #atom infomation (tag, tau, spcies)
        self.species = []           #ATOMIC_SPEICES, card

        #Cell/Configuration Information
        self.step = 0               #Last Step
        self.aprim = []             #Primitive vectors
        self.brav = ''              #Name of the lattice
        self.lat = 0                #Lattice Parameter

        #Bands
        self.nbnd = 0              #Number of Bands
        self.nspin = 0              #Number of spin polarizations

        #Planewaves
        self.ecutwfc = 0            #Planewave Energy cutoff
        self.fft = []               #FFT Grid

        #System Info
        for child in root:

            #Status
            if child.tag == 'STATUS':
                self.step = child.find('STEP').attrib['ITERATION']

            #Cell
            if child.tag == 'CELL':
                self.brav = child.find('BRAVAIS_LATTICE').text.strip()
                self.lat  = child.find('LATTICE_PARAMETER').text.strip()
                lat_vec_tag = 'DIRECT_LATTICE_VECTORS'
                lat_vec_index = ('a1', 'a2', 'a3')
                self.aprim = [ child.find(lat_vec_tag).find(X).text.strip() for X in lat_vec_index ]
            

            #Ions
            if child.tag == 'IONS':
                self.nat  = child.find('NUMBER_OF_ATOMS').text.strip()
                self.ntyp = child.find('NUMBER_OF_SPECIES').text.strip()
                #find all atoms
                for sub in child:
                    
                    p = re.compile('ATOM\.(.*)')
                    m = p.search(sub.tag)
                    if m:
                        self.atoms_raw.append(sub.attrib)
                        self.atoms.append(self.atoms_raw[-1]['SPECIES'] + self.atoms_raw[-1]['tau'])

                    p = re.compile('SPECIE\.(.*)')
                    m = p.search(sub.tag)
                    if m:
                        self.species.append([sub.find('ATOM_TYPE').text.strip(),  \
                                             sub.find('MASS').text.strip(), sub.find('PSEUDO').text.strip()])
                if show:
                    print "Number of atoms:", self.nat
                    print "Number of atoms:", self.ntyp


            #Bands:
            if child.tag == 'BAND_STRUCTURE_INFO':
                self.nbnd = child.find('NUMBER_OF_BANDS').text.strip()
                self.nspin = child.find('NUMBER_OF_SPIN_COMPONENTS').text.strip()


            #PlaneWave
            if child.tag == 'PLANE_WAVES':
                self.ecutwfc  = child.find('WFC_CUTOFF').text.strip()
                self.fft.append(child.find('FFT_GRID').attrib['nr1'])
                self.fft.append(child.find('FFT_GRID').attrib['nr2'])
                self.fft.append(child.find('FFT_GRID').attrib['nr3'])

            #Occupations
            if child.tag == 'OCCUPATIONS':
                occ  = [float(i) for i in child.find('INPUT_OCC_UP').text.strip().split() ]

        self.tot_charge = int(2*int(self.nbnd) - sum(occ))


class EigFile(object):

    def __init__(self, step, prefix, dir):
        """
        Return the eigenvalues
        
        Inputs:
            1) current step number
            2) prefix name
            3) directory name
        """

        self.eigs = []
        with open(os.path.join(dir, prefix + '.eig')) as fin:
            while True:
                line = fin.readline()
                if not line:
                    break
                p = re.compile('STEP')
                m = p.search(line)
                if m:
                    #found correct step
                    if int(line.strip().split()[1]) == step:
                        #read NEXT TWO lines
                        line = fin.readline()
                        line = fin.readline()
                        while not p.search(line) and line:
                            self.eigs.extend([float(i) for i in line.strip().split() ])
                            line = fin.readline()
                        #break out of main loop
                        break
                    #incorrect step
                    else:
                        continue

    def get(self):
        if self.eigs:
            return self.eigs
        else:
            return None


#----------------------------------------------------------------------------
# Exceptions
#----------------------------------------------------------------------------

class ElecError(Exception):
    """
    General User Error Exception Class for the Electronic Structure
    """

    def __init__(self, name, msg, chdir=False):
        print "\nUSER ERROR => {}, ".format(str(name)) + msg
        if chdir:
            os.chdir(os.pardir)

class IntError(Exception):
    """
    General Internal Error Exception Class for the Electronic Structure
    """
    def __init__(self, name, msg, chdir=False):
        print "\nINTERNAL ERROR => {}, ".format(str(name)) + msg
        if chdir:
            os.chdir(os.pardir)
