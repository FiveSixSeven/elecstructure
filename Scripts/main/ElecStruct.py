#! /usr/bin/env python

import os, sys, imp
import ElecStructClass as ESC
script_dir = os.path.dirname(os.path.realpath(__file__))
package_dir = os.path.join(script_dir, os.pardir, os.pardir)
inp = imp.load_source('inputfile', os.path.join(package_dir, 'Modules', 'Input', 'inputfile.py'))
cl  = imp.load_source('Cline', os.path.join(package_dir, 'Modules', 'Input', 'Cline.py'))

def main(argv):
    """
    """

    #Command line
    inv, inputfile = startup(argv)

    #Electronic Structure Class, main attrs, read inputput/calcfile files
    run = ESC.ElecStruct(inputfile, inv['force'])

    #welcome
    welcome(run, inv['asis'], inv['delete'], not inv['force'])

    #Create the main directory
    run.create_dirs()

    #-------------------------------------------
    #Input variables
    #-------------------------------------------
    if not inv['asis']:
        new_input ={ 
           'pseudo_dir'   : None,
           'ibrav'        : None,
           'celldm(1)'    : None,
           'ecutwfc'      : None,
           'prefix'       : None,
           'atoms'        : None,
           'anums'        : None
        }
        swap = {
            'prefix'       : "'{}'".format(run.vars['prefix']),
            'outdir'       : "'./'",
            'pseudo_dir'   : "'{}'".format(run.vars['pseudo_dir']),
            'ndr'          : 50,
            'ndw'          : 50,
            'nat'          : sum(run.vars['anums']),
            'ntyp'         : len(run.vars['atoms']),
            'ibrav'        : run.vars['ibrav'],
            'celldm(1)'    : run.vars['celldm(1)'], 
            'ecutwfc'      : run.vars['ecutwfc'],
        }
    else:
        new_input = {}
        swap = {}
    run.reqv.update(new_input)
    run.concheck()
    #-------------------------------------------

    #-------------------------------------------
    # Template-to-file creation
    #-------------------------------------------
    run.read_templates()

    #show/check templates 
    if not inv['asis']:
        run.check_atom_order()
    run.show_templates(**swap)

    #Atomic positions
    positions = run.find_atomic_positions()
    #-------------------------------------------

    #-------------------------------------------------------------
    #Set-up, create sub-dir, create/write files in sub-dir
    #-------------------------------------------------------------
    run.create_steps(positions, swap, inv['delete'])
    #-------------------------------------------------------------

    #-------------------------------------------------------------
    # PBS submit script creation
    #-------------------------------------------------------------
    run.qsub()
    #-------------------------------------------------------------

    print " Program Complete\n"


def startup(argv):
    """
    Read in Command Line options and arguments, set them up and retrun a dictionary
    """

    usage = """

    Set-up Electronic Structure Calculations, using previous a MD trajectories.


    Usage:

    Required:
    ------------
        ARGS                : Inputfile (only one)

    Optional:
    ------------
        --asis              : Use templates without alterations
        --delete            : Prompt deletion of pre-existing step directories  
        --force             : Enable No User Interaction Mode
         
    """
    
    arguments = {
        'asis'      : [''  , 'asis'   ,  False],
        'delete'    : [''  , 'delete' ,  False],
        'force'     : [''  , 'force'  ,  False],
    }

    read = cl.Args(argv, arguments, usage)
    result = read.get_opts()
    dirs = read.get_args()

    if len(dirs) > 1:
        print "WARNING: Only the first inputfile will be used"
    
    return result, dirs[0]



def welcome(run, asis, delete, user):
  
    print ""
    print " ------------------------------------------------------"
    print " |          Electronic Structure Calculation          |"
    print " |                  Qunatum Espresso                  |"
    print " |                                                    |"
    print " |                   Charles Swartz                   |"
    print " ------------------------------------------------------\n"

    print "  A general set-up script for electronic structure"
    print "  calculations using molecular dynamics trajectories "
    print "  generated using the Quantum Espresso code package. "
    print "  This program requires the following:"
    print "     1) a calculation-rule file"
    print "     2) a submission-rule file"
    print "     3) a submission preamble file\n"

    #runtime options
    print " Run-time Options:"
    if asis:
        print "  -All Template files will be read as-is"
    else:
        print "  -Template files will be altered"
    if user:
        print "  -Executed in 'User' Mode"
    else:
        print "  -Executed in 'Non-User' Mode"
    if delete and user:
        print "  -Previously existing step files will be prompted for deletion"
    else:
        print "  -Previously existing step files will be skipped"

    print "\n Input Variables: "
    for k in run.vars.getall().keys():

        if isinstance(run.vars.get(k), list):
            print "  {:20s} = ".format(k), ' '.join([str(i) for i in run.vars.get(k)])
        else:
            print "  {:20s} = {}".format(k, run.vars.get(k))

    if run.user:
        run.pause(extra=" ")


#*************************************************
#*************************************************
if __name__ == '__main__':
    main(sys.argv[1:])
