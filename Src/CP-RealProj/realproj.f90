!-------------------------------------------------------------------------------
!Program To calculate Real-Space Projection of the Kohn-Sham Orbitals
!
!IMPORTANT: This is post-processing program designed to work with 
!a modified version of Quantum-Esspresso that prints out KS orbitals
!for each band in fortran binary files called 'KS_<num>.dat'.
!
! 'Format' of the binary:
!     1 - 1             step number (As an error check)
!     2 - nr1*nr2*nr3   |psi|^2 
!
!
!Written By Charles Swartz Fri July 1 2012, converted to parallel on 
!Fri Aug 3 2012
!
!
!TODO Add nspin implementation
!
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
PROGRAM realproj
   !
   USE omp_lib
   !
   implicit none
   !
   include 'mpif.h'
   !
   !Constants
   integer, parameter         :: DP  = SELECTED_REAL_KIND(15,99)
   integer, parameter         :: max_ntyp = 20
   real(DP), parameter        :: ao=0.52917721_DP
   !
   !Atom species derived data types
   type atom_positions
      integer                 :: num
      character(len=3)        :: tag
      real(DP), allocatable   :: r(:,:)
   end type atom_positions
   !
   type(atom_positions), allocatable   :: tau(:)
   !
   !
   !-----------------------------
   !Main Program Variables
   real(DP), allocatable   :: pdos(:),          &  !Final projected DOS (root only)
                              eigs(:)              !Eigenvalues to be read-in (root only) 
   real(DP)                :: radius,           &  !Cut-off Radius for integration
                              aprim(3, 3),      &  !Primative lattice vectors
                              Estart,           &  !Starting EigenValue (plotting)
                              Estop,            &  !Ending EigenValue (plotting)
                              dE,               &  !EigenValue interval (plotting)
                              degauss,          &  !Parameter for gaussian Broadening
                              wk,               &  !Spin weight for the bands
                              stime, etime         !Program time
   !
   !
   integer                 :: tindex,           &  !Species Index of TARGET atom,
                                                   !  **NOT Relative total atoms!!**
                              tspecies,         &  !Species Number of TARGET atom
                              grid(3),          &  !FFT grid dimensions
                              nstep,            &  !Step Number, (will be double-checked)
                              ntyp,             &  !Total number of species (up to max_ntyp)
                              nsp(max_ntyp),    &  !Number of Atoms for individual species
                              nat,              &  !Total Number of atoms
                              nbnd,             &  !Total number of bands from QE
                              bstart,           &  !Starting Band for the calculation
                              bstop,            &  !Starting Band for the calculation
                              ierr
   !
   !
   character(len=3)        :: sptag(max_ntyp)      !Species Tag: Atomic symbol
   character(len=150)      :: fileroot,         &  !File Root for Kohn-Sham binary files
                              fileext,          &  !File extension for Khohn-Sham binary files 
                              output,           &  !Name of the projection output file
                              eigfile,          &  !Name of the *.eig file produced by QE
                              celfile,          &  !Name of the *.eig file produced by QE
                              posfile              !Name of the *.pos file produced by QE
   !
   !-----------------------------
   !
   !-----------------------------
   !For parallel execution
   integer                 :: myid,       &  !Absolute Index of the processor 0 .... (nproc -1)
                              nproc          !total number of processors
   !-----------------------------
   !
   NAMELIST /input/ nstep, tspecies, tindex, tindex, ntyp, nsp, sptag, grid, nbnd, bstart, bstop, &
         radius, dE, degauss, wk, Estart, Estop, output, eigfile, celfile, posfile, fileroot, fileext

   Call MPI_INIT(ierr)
   Call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
   Call MPI_COMM_SIZE(MPI_COMM_WORLD, nproc, ierr)
   !
   !Header
   if (myid == 0) call printout(0)

   !Initialize everthing
   call startup()

   !Main Processor loop
   call main_loop()
   
   call write_files() 

   !End Program
   call terminate()


   CONTAINS

   !=------------------------------------------------------------------------=!
      SUBROUTINE main_loop()
   !=------------------------------------------------------------------------=!
      !
      implicit none
      !
      complex(DP), allocatable   :: rho(:,:,:),       &  !ix, iy, iz, value at point
                                    rho_1d(:)            !Read-in rho value
                                    !space(:,:,:,:)       !ix, iy, iz, point with it's coordinates
      !
      real(DP)                :: tpdos,         &  !Temp value for pdos
                                 space(3),      &  !space point, Not saved
                                 norm,          &  !Normalization temp value
                                 unitvec(3,3),  &  !Unit cell x, y, z
                                 dr(3),         &  !discretization units
                                 disp(3),       &  !Displacment vectors
                                 radius2,       &  !Radius sqaures
                                 dist2,         &  !Distance Squared
                                 ans               !tranfer value amoung processors
      !
      integer                 :: ix, iy, iz,    &  !FFT indexes
                                 n_in_sphere,   &  !Number of FFT grid points inside cutoff
                                 rhounit,       &  !|psi^2| file unit
                                 tstep,         &  !read in step-value
                                 i, ierr
      !
      character(len=256)      :: infile
      character(len=3)        :: x1
      !
      !----------------------------------------------
      !For parallel execution
      integer                 :: me,         &  !relative index 
                                 per_proc,   &  !Operations per processor (ideal)
                                 remain,     &  !diffence in per_proc and nproc
                                 np,         &  !processr index
                                 status(MPI_STATUS_SIZE)
      !----------------------------------------------
      !
      rhounit = 100 
      !
      !Number jobs per processor (IMPORTANT)
      per_proc = Ceiling(nbnd/real(nproc))
      remain = per_proc * nproc - nbnd
      !
      radius2 = (radius)**2
      allocate( rho(grid(1), grid(2), grid(3)), rho_1d(grid(1)*grid(2)*grid(3)) )
      !
      !Set up space increments 
      unitvec(1,1:3) = (/1, 0, 0/)
      unitvec(2,1:3) = (/0, 1, 0/)
      unitvec(3,1:3) = (/0, 0, 1/)
      !
      do i=1,3,1
         dr(i) = sqrt(sum(matmul(aprim, unitvec(i,1:3))**2 ))/dble(grid(i))
      enddo
      !
      if (myid == 0) then
         write(*,'(/, 2X, "Grid Intervals:")')
         write(*,'(4X, 3(F5.3, 2X))') (dr(i),i=1,3)
      endif
      !
      if (myid == 0) write(*, '(/, 2X, "Starting Calculation...")')
      !
      !-------------------------------------------
      proc_loop: do np=1,per_proc,1
         !
         me = myid + (np-1) * nproc + 1
         if (me > nbnd) exit
         !
         tpdos = 0.0_DP   
         norm = 0.0_DP
         !
         write(x1, '(I0)'), me 
         infile = TRIM(fileroot)//TRIM(x1)//TRIM(fileext)
         open(unit=rhounit, file=TRIM(infile), iostat=ierr, form="unformatted")
         !
         if (ierr /= 0) then
            write(*,*) "ERROR: Cannot open", infile
            stop
         endif
         !
         !read(rhounit) tstep
         !
         !if (tstep /= nstep) then
         !   write(*,*) "ERROR: File ", trim(infile), &
         !                           &" does not start at the correct step", tstep, nstep
         !   stop
         !endif
         !
         !Read-in rho and reshape
         read(rhounit) rho_1d
         !rho = reshape(rho_1d, grid)
         call array_1D_to_3D(rho_1d, rho, grid, grid(1)*grid(2)*grid(3) )
         !
         !Grid Points Loop
         n_in_sphere = 0
         do ix=1, (grid(1))
            do iy=1,(grid(2))
               do iz=1,(grid(3))
                  !
                  !Space Grid
                  space(1:3) = (/(ix-1)*dr(1), (iy-1)*dr(2), (iz-1)*dr(3) /)
                  !
                  !Normalization should be equal to 1
                  norm = norm + dble(rho(ix,iy,iz))**2
                  !
                  !
                  do i=1,3,1
                     disp(i) = space(i) - tau(tspecies)%r(tindex,i)
                     disp(i) = disp(i) - NINT(disp(i)/aprim(i,i))*aprim(i,i)
                  enddo
                  !
                  dist2 = disp(1)**2 + disp(2)**2 + disp(3)**2
                  !
                  !Count only if below the cutoff radius
                  if ( dist2 < (radius2) ) then
                     !
                     tpdos = tpdos + dble(rho(ix,iy,iz))**2
                     n_in_sphere = n_in_sphere + 1
                     !
                  endif
                  !
               enddo
            enddo
         enddo
         !
         !normalize the overlap
         !Recall: psi is defined per gridpts
         tpdos = tpdos/dble(n_in_sphere)
         !
         !Send to Root process
         if (myid /= 0) then
            Call MPI_SEND(tpdos, 1, MPI_DOUBLE_PRECISION, 0, me, MPI_COMM_WORLD, ierr)
         endif
         !
         !Receive by Root Process
         if(myid == 0 ) then
            !
            pdos(me) = tpdos
            !
            if (np == per_proc) then !For the last loop, may have unfilled procs
               !
               do i =1,(nproc-1-remain),1 !from the other processors
                  Call MPI_RECV(ans, 1, MPI_DOUBLE_PRECISION, MPI_ANY_SOURCE, MPI_ANY_TAG, &
                                 MPI_COMM_WORLD, status, ierr)
                  pdos(status(MPI_TAG)) = ans
               enddo
               !
            else
               !
               do i =1,(nproc-1),1 !from the other processors
                  Call MPI_RECV(ans, 1, MPI_DOUBLE_PRECISION, MPI_ANY_SOURCE, MPI_ANY_TAG, &
                                 MPI_COMM_WORLD, status, ierr)
                  pdos(status(MPI_TAG)) = ans
               enddo
               !
            endif
         endif
         !
         write(6,'(3X, "KS State : ", I5, ",  Integrated Charge: ", 2X, F5.3)') me, &
                              & norm/dble(grid(1)*grid(2)*grid(3))
         !
         close(rhounit)
         !
      enddo proc_loop
      !
      !call MPI_Barrier( MPI_COMM_WORLD )
      !
   !----------------------------------------------
      END SUBROUTINE main_loop
   !=------------------------------------------------------------------------=!

   !=------------------------------------------------------------------------=!
      SUBROUTINE startup()
   !=------------------------------------------------------------------------=!
         !
         implicit none
         !
         integer           :: i, ierr
         !
         !
         !Read the input file
         if (myid == 0) then
            stime = MPI_Wtime()
            read(5,nml=input,IOSTAT=ierr)
            !if (ierr /= 0) then
            !   call printout(2)
            !   stop 
            !endif
         endif
         !
         !Broadcast all values to all nodes
         Call MPI_BCAST(nstep, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(tindex, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(tspecies, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(ntyp, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(nsp, max_ntyp, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
         do i=1,ntyp
            Call MPI_BCAST(sptag(i), 1, MPI_CHARACTER, 0, MPI_COMM_WORLD, ierr)
         enddo
         Call MPI_BCAST(grid, 3, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(nbnd, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(bstart, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(bstop, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(radius, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(dE, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(degauss, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(wk, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(estart, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(estop, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(output, 256, MPI_CHARACTER, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(eigfile, 256, MPI_CHARACTER, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(celfile, 256, MPI_CHARACTER, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(posfile, 256, MPI_CHARACTER, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(fileroot, 256, MPI_CHARACTER, 0, MPI_COMM_WORLD, ierr)
         Call MPI_BCAST(fileext, 256, MPI_CHARACTER, 0, MPI_COMM_WORLD, ierr)
         !
         if (ierr /= 0 ) then
            print *, 'ERROR: MPI_BCAST,  ID:', myid
            stop
         endif
         !
         !-----------------------------
         !Only on the root Processor
         if (myid == 0) then
            allocate ( eigs(nbnd) )
            allocate ( pdos(nbnd) )
            call get_eigs()
            do i=1,nbnd
               write(44,*) eigs(i)
            enddo
         endif
         !-----------------------------
         !
         !-----------------------------
         !Set-up on all processors
         nat = 0
         do i = 1, ntyp
            nat = nat + nsp(i) 
         enddo
         !
         allocate(tau(ntyp))
         do i = 1, ntyp
            tau(i)%num = nsp(i)      
            tau(i)%tag = trim(sptag(i))
            allocate( tau(i)%r(tau(i)%num,3) )
         enddo
         !-----------------------------
         !
         call get_cel()
         call get_pos()
         !
         if (myid == 0 ) call printout(1)
         !

      END SUBROUTINE startup
   !=------------------------------------------------------------------------=!

   !=------------------------------------------------------------------------=!
      SUBROUTINE write_files()
   !=------------------------------------------------------------------------=!
         !
         implicit none
         !
         real(DP)             :: E, printval
         integer              :: i, ns, ierr, ounit, oiunit, nE
         !
         ounit = 11
         oiunit = 12
         !
         nE=abs(Estop - Estart)/dE + 1 !add one for zero
         !
         !
         if(myid == 0)then
            !
            !Open Main output file
            open(unit=ounit, file=output, IOSTAT=ierr, STATUS='UNKNOWN')
            if (ierr /= 0 ) then
               write (*,*) '  ERROR: There was some issue opeing the' // trim(output) // ' file'
               stop
            endif
            !Open raw output file (no broadening)
            open(unit=oiunit, file=TRIM(output)//'-index', IOSTAT=ierr, STATUS='UNKNOWN')
            if (ierr /= 0 ) then
               write (*,*) '  ERROR: There was some issue opeing the' // trim(output) // '-index file'
               stop
            endif
            !
            !Write the index file (Easier to find peak states) before broadening
            do ns=1, nbnd,1
               write(oiunit,*) ns, pdos(ns)
            enddo
            close(oiunit)

            !Final packaging of the pdos and E values
            do i=1,nE,1
               !
               E = Estart + (i-1)*dE
               !
               printval = 0.0_DP
               do ns=1,nbnd,1
                  printval = printval +  wk * w0gauss ( (E - eigs(ns))/ degauss) * pdos(ns)
               enddo
               !
               printval = printval/degauss
               !
               write(ounit,*) E , printval/dble(grid(1)*grid(2)*grid(3))
               !
            enddo
            !
            close(ounit)
            !
            etime = MPI_Wtime()
            Write(*,*) '    Total time: ', etime - stime
            !
         endif
         !
      END SUBROUTINE write_files
   !=------------------------------------------------------------------------=!

   !=------------------------------------------------------------------------=!
      SUBROUTINE get_pos()
   !=------------------------------------------------------------------------=!
         !
         implicit none
         !  
         real(DP), allocatable   :: temppos(:,:,:),   &  !Temp Position (nsp, maxnum, dim)
                                    temppos_1d(:)        !Temp Position (nsp*maxnum*dim)
         real(DP)                :: time
         integer                 :: i, j, k, snum, posunit, ierr, maxnum
         character(len=25)       :: dummy
         !
         posunit = 1
         !
         !Get the max species number
         maxnum = 0 
         do i=1,ntyp
            if (tau(i)%num > maxnum) then
               maxnum = tau(i)%num
            endif
         enddo
         !
         allocate(temppos(ntyp,maxnum,3))
         allocate(temppos_1d(ntyp*maxnum*3))
         !
         !Read only on the first node
         if (myid == 0) then
            !
            open(posunit, file=posfile, iostat=ierr)
            if ( ierr /= 0) then
               write(*,*) 'ERROR Cannot open *.pos file'
               stop
            endif
            !
            pos_loop: do
               read(posunit,*, iostat=ierr) snum, time
               !
               if (ierr /= 0) then
                  write(*,*) 'ERROR: End of *.pos File Reached'
                  stop
               endif
               !
               !Read in temppos
               do i= 1, ntyp
                  do j = 1, tau(i)%num
                     read(posunit, *) (temppos(i,j,k), k=1,3)
                  enddo
               enddo
               if (snum == nstep) exit
            enddo pos_loop
            !
            
            close(posunit)
            !
            !Reshape for broadcast
            temppos_1d = reshape( temppos, (/ntyp*maxnum*3/) )
            !
         endif
         !
         !Broadcast and convert back to 3d
         Call MPI_BCAST(temppos_1d, ntyp*maxnum*3, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
         temppos = reshape( temppos_1d, (/ntyp, maxnum, 3/) )
         !
         do i= 1, ntyp
            do j = 1, tau(i)%num
               tau(i)%r(j,1:3) = temppos(i,j,1:3)
            enddo
         enddo
         !
      END SUBROUTINE get_pos
   !=------------------------------------------------------------------------=!

   !=------------------------------------------------------------------------=!
      SUBROUTINE get_eigs()
   !=------------------------------------------------------------------------=!
         implicit none
         !  
         integer, parameter   :: cpcols = 10
         !
         real(DP)             :: time, value
         integer              :: i, j, k, flines, remain, eigunit, ncount, snum ,ierr
         character(len=40)    :: dummy
         !
         eigunit = 2
         !
         flines = nbnd/cpcols
         remain = mod(nbnd, cpcols)
         !
         if (myid == 0) then
            !
            !Open the eigfile
            open(eigunit, file=trim(eigfile), iostat=ierr, status='unknown')
            if ( ierr /= 0) then
               write(*,*) 'ERROR Cannot open *.eig file'
               stop
            endif
            ! 
            do
               !
               !Read the header
               read(eigunit,*) dummy, snum, time
               read(eigunit,*) dummy
               !
               !read full lines
               ncount = 0
               do j=1, flines
                  read(eigunit,*) (eigs(ncount+k),k=1,cpcols)
                  ncount = ncount + cpcols
                  !
               enddo
               !
               !read last line
               read(eigunit,*) (eigs(ncount+j),j=1,remain)
               !
               !Exit if its the correct step
               if (nstep == snum) exit
               !
            enddo
         endif
         !
         close(eigunit)
         !
      END SUBROUTINE get_eigs
   !=------------------------------------------------------------------------=!

   !=------------------------------------------------------------------------=!
      SUBROUTINE get_cel()
   !=------------------------------------------------------------------------=!
         !
         implicit none
         !  
         real(DP)             :: time, cell(3,3), cell_1d(9)
         integer              :: i, j, k, snum, celunit, ierr
         character(len=25)    :: dummy
         !
         celunit = 3
         !
         if (myid == 0) then
            open(celunit, file=celfile, iostat=ierr)
            if ( ierr /= 0) then
               write(*,*) 'ERROR Cannot open *.cel file'
               stop
            endif
            !
            cel_loop: do
               read(celunit,*, iostat=ierr) snum, time
               !
               if (ierr < 0) then
                  write(*,*) 'ERROR: End of *.cel File Reached'
                  stop
               endif
               !
               do i= 1, 3
                     read(celunit, *) (cell(i,j),j=1,3)
               enddo
               !
               if (snum == nstep) then
                  cell_1d = reshape(cell, (/9/))
                  exit
               endif
               !
            enddo cel_loop
         endif
         !
         Call MPI_BCAST(cell_1d, 9, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
         cell = reshape( cell_1d, (/3, 3/) )

         do i= 1, 3
               aprim(i,1:3) = cell(i,1:3)
         enddo
         !
         close(celunit)
         !
      END SUBROUTINE get_cel
   !=------------------------------------------------------------------------=!

   !=------------------------------------------------------------------------=!
      SUBROUTINE array_1D_to_3D(array1, array3, grid, grid_tot )
   !=------------------------------------------------------------------------=!
         !
         implicit none
         !
         complex(DP),intent(in)  :: array1(:)
         complex(DP),intent(out) :: array3(:,:,:) 
         integer, intent(in)  :: grid(3), grid_tot       !grid dimensions and totals

         integer              :: i,                   &
                                 loopx,loopy,loopz       !index for 1D -> 3D array conversion
         !
         !
         !Convert From temp (1D) vector to rho (3D) Array 
         !Quantum Espresso outputs the DATA file as 1D vectors, converts to a
         !3D array in chegens.f90
         do i=1,grid_tot
            loopz = INT( ABS(i - 1)/(grid(1)*grid(2)) ) + 1
            loopy = INT( ABS( (i - 1) - (loopz - 1)*grid(1)*grid(2) ) / grid(1) ) + 1
            loopx = i - (loopz-1)*grid(1)*grid(2) - (loopy-1) * grid(1)
            array3(loopx, loopy, loopz) =  array1(i)
         enddo
         !
         return
         !
      END SUBROUTINE array_1D_to_3D
   !=------------------------------------------------------------------------=!

   !=------------------------------------------------------------------------=!
      SUBROUTINE printout(icase)
         !
         implicit none
         !
         integer, intent(in)     :: icase
         !
         integer                 :: i, j
         !
         select case (icase)
            !
            case(0)
               !
               write(*, '(/, 1X, 50("-") )')
               write(*, '(5X, "Realspace projection of Kohn Sham Orbitals" )')
               write(*, '(14X, "Charles. W. Swartz VI" )')
               write(*, '(1X, 50("-") )')
               !
            case(1)
               !
               write(*, '(/, 2X, "General Infomation:")')
               write(*, '(3X, "Step:               ", I5)') nstep
               write(*, '(3X, "nbnd:               ", I5)') nbnd
               write(*, '(3X, "nbnd start:         ", I5)') bstart
               write(*, '(3X, "nbnd stop:          ", I5)') bstart
               write(*, '(3X, "nytp:               ", I5)') ntyp
               do i = 1, ntyp
                  write(*, '(3X, "nsp("I2"):            ", I5)') i, nsp(i)
               enddo
               do i =1,3
                  write(*, '(3X, "grid("I2"):           ", I5)') i, grid(i)
               enddo
               !
               write(*,*)
               do i = 1, ntyp
                  write(*, '(2X, "Species ", I2)') i
                  write(*, '(3X, "Type:  ", A3)') adjustr(tau(i)%tag)
                  write(*, '(3X, "Num:   ", I3)') tau(i)%num
               enddo 
               !
               write(*, '(/, 2X, "Integration Target:")')
               write(*, '(3X, "Cut-off Radius:    ", F6.3)') radius
               write(*, '(3X, "Species:            ", A5)') adjustr(tau(tspecies)%tag)
               write(*, '(3X, "Index:              ", I5)') tindex
               write(*, '(3X, "Coordinates (bohr):  ")') 
               write(*, '(3X, 3(F10.5))') tau(tspecies)%r(tindex,1:3)
               !
               write(*, '(/, 2X, "Plotting Infomation:")')
               write(*, '(3X, "Estart:           ", F7.2)') estart
               write(*, '(3X, "Estop:            ", F7.2)') estop
               write(*, '(3X, "E Spacing:        ", F7.3)') de
               write(*, '(3X, "degauss:          ", F7.5)') degauss
               ! 
               write(*, '(/, 2X, "Files:")')
               write(*, '(3X, "Position File:    ", 5X, A)') trim(posfile)
               write(*, '(3X, "Eigenvalue File:  ", 5X, A)') trim(eigfile)
               write(*, '(3X, "Cell File:        ", 5X, A)') trim(celfile)
               write(*, '(3X, "Output File:      ", 5X, A)') trim(output)
               write(*, '(3X, "KS File Root:     ", 5X, A)') trim(fileroot)
               write(*, '(3X, "KS File .ext:     ", 5X, A)') trim(fileext)
               !
               write(*, '(/, 2X, "Unit Cell (bohr):")')
               do i=1,3
                  write(*,'(3X,3(F7.3))') (aprim(i,j), j=1, 3)
               enddo

         end select

      END SUBROUTINE printout
   !=------------------------------------------------------------------------=!
   !=------------------------------------------------------------------------=!

   !=------------------------------------------------------------------------=!
      real(DP) function w0gauss (x)
   !=------------------------------------------------------------------------=!
         !
         implicit none
         !
         REAL(DP), PARAMETER  :: pi                = 3.14159265358979323846_DP
         REAL(DP), PARAMETER  :: sqrtpi            = 1.77245385090551602729_DP
         REAL(DP), PARAMETER  :: sqrtpm1           = 1.0_DP / sqrtpi        
         !
         real(DP) :: x
         ! output: the value of the function
         ! input: the point where to compute the function
         !
         real(DP) :: arg
         !
         arg = min (200.d0, x**2)
         w0gauss = exp ( - arg) * sqrtpm1
         !
      end function w0gauss 
   !=------------------------------------------------------------------------=!

   !=------------------------------------------------------------------------=!
      SUBROUTINE terminate()
   !=------------------------------------------------------------------------=!
         !
         implicit none
         !
         integer           :: ierr
         !
         if (myid == 0) then
            deallocate(eigs)
            deallocate(pdos)
         endif
         !
         !
         Call MPI_FINALIZE(ierr)
         !
      END SUBROUTINE terminate
   !=------------------------------------------------------------------------=!

END PROGRAM realproj
