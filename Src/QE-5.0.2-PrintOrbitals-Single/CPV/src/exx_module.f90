MODULE exx_module
!
! Comment1
!----------------------------------------------------------------------------------------------------------------


!----------------------------------------------------------------------------------------------------------------
!
USE cell_base,          ONLY: h                  !h matrix for converting between r and s coordinates via r = h s
USE cell_base,          ONLY: ainv               !h^-1 matrix for converting between r and s coordinates via s = h^-1 r)
USE cell_base,          ONLY: omega              !cell volume (in au^3)
USE cell_base,          ONLY: isotropic          !if .TRUE. then "volume" option is chosen for cell_dofree
USE cell_base,          ONLY: ibrav              !ibrav determines the symmetry of the simulation cell. See manual for acceptable values ... 
USE cell_base,          ONLY: ref_at             !reference cell parameters in ref_alat unit
USE constants,          ONLY: pi                 !pi in double-precision
USE constants,          ONLY: fpi                !4.0*pi in double-precision
USE control_flags,      ONLY: lwfnscf
USE control_flags,      ONLY: lwfpbe0            !if .TRUE. then PBE0 calculation using Wannier orbitals is turned on ... 
USE control_flags,      ONLY: lwfpbe0nscf
USE control_flags,      ONLY: thdyn              !if .TRUE. then variable cell calculation is turned on ..   
USE electrons_base,     ONLY: nbsp               !number of elctron bands ...
USE electrons_base,     ONLY: nspin              !spin unpolarized (npsin=1) vs. spin polarized (nspin=2) specification
USE fft_base,           ONLY: dffts              !FFT derived data type
USE fft_base,           ONLY: dfftp              !FFT derived data type 
USE input_parameters,   ONLY: ref_alat           !alat of reference cell ..
USE input_parameters,   ONLY: ref_cell           !.true. if reference cell parameters are in use, .false. otherwise ... 
USE io_global,          ONLY: stdout             !print/write argument for standard output (to output file)
USE kinds,              ONLY: DP                 !double-precision kind (selected_real_kind(14,200))
USE mp,                 ONLY: mp_sum             !MPI collection with sum
USE mp_global,          ONLY: nproc_image        !number of processors
USE mp_global,          ONLY: me_image           !processor number (0,1,...,nproc_image-1)
USE mp_global,          ONLY: me_bgrp            !processor number (0,1,...,nproc_image-1)
USE mp_global,          ONLY: intra_image_comm   !standard MPI communicator
USE parallel_include
USE wannier_base,       ONLY: neigh         ! maximum number of neighbors set in the calculation to initialize many arrays  ...
USE wannier_base,       ONLY: dis_cutoff    ! radius to obtain WF pairs 
USE wannier_base,       ONLY: exx_ps_rcut_s ! radius of the poisson sphere for self orbital
USE wannier_base,       ONLY: exx_me_rcut_s ! radius of the ME sphere for self orbital
USE wannier_base,       ONLY: exx_ps_rcut_p ! radius of the poisson sphere for pair orbital
USE wannier_base,       ONLY: exx_me_rcut_p ! radius of the ME sphere for pair orbital
USE wannier_base,       ONLY: vnbsp 
!
!
IMPLICIT NONE
!
SAVE
!
! PUBLIC variables 
!
INTEGER, PARAMETER, PUBLIC          :: lmax=6  ! maximum angular momentum ... 
INTEGER, PARAMETER, PUBLIC          :: nord2=3 ! order of expansion ( points on one side) ...
!
INTEGER, PUBLIC                     :: n_exx   ! index of exx steps ...
INTEGER, PUBLIC                     :: sc_fac  ! scaling factor to parallelize on (nproc_image=integer multiple of nbsp)
                                               ! sc_fac=INT(nproc_image/nbsp); the parallelization can be 
                                               ! generalized for any number of procesors by using an array of sc_fac(1:nbsp)
INTEGER, PUBLIC                     :: np_in_sp_s, np_in_sp_me_s  ! number of grid points in the PS sphere and the ME sphere for self orbital in Full Grid 
INTEGER, PUBLIC                     :: np_in_sp_p, np_in_sp_me_p  ! number of grid points in the PS sphere and the ME sphere for pair orbital in Full Grid
!
INTEGER, PUBLIC                     :: my_nbspx               ! parallelization/distribution of orbitals over processors 
INTEGER,  ALLOCATABLE, PUBLIC       :: my_nbsp(:), my_nxyz(:) ! parallelization/distribution of orbitals over processors 
INTEGER,  ALLOCATABLE, PUBLIC       :: index_my_nbsp (:, :), rk_of_obtl (:), lindex_of_obtl(:) ! parallelization/distribution of orbitals over processors 
!
! conversion between 3D index (i,j,k) and 1D index 
! odthothd_in_sp(3, 1:np_in_sp_p) is for inner sphere (1st shell)
! odthothd_in_sp(3, np_in_sp_p+1:np_tmp_1) is for 2nd shell
! odthothd_in_sp(3, np_tmp_1+1:np_tmp_2) is for 3rd shell
! odthothd_in_sp(3, np_tmp_2:np_in_sp_me_s) is for 4th shell
INTEGER,  ALLOCATABLE, PUBLIC       :: odtothd_in_sp(:,:)     ! 1D to 3D index converter in local grid within largest ME sphere
INTEGER,  ALLOCATABLE, PUBLIC       :: thdtood_in_sp(:,:,:)   ! 3D to 1D index converter in local grid within largest ME sphere
INTEGER,  ALLOCATABLE, PUBLIC       :: thdtood(:,:,:)         ! 3D to 1D index converter in global grid (nr1,nr2,nr3 => nr1*nr2*nr3) 
!
REAL(DP), ALLOCATABLE, PUBLIC       :: rhopr(:,:)
!
REAL(DP), ALLOCATABLE, PUBLIC       :: xx_in_sp(:)            ! distance between centre of the simulation box and grid points along X direction .. 
REAL(DP), ALLOCATABLE, PUBLIC       :: yy_in_sp(:)            ! distance between centre of the simulation box and grid points along Y direction .. 
REAL(DP), ALLOCATABLE, PUBLIC       :: zz_in_sp(:)            ! distance between centre of the simulation box and grid points along Z direction .. 
!
REAL(DP), ALLOCATABLE, PUBLIC       :: sc_xx_in_sp(:)         ! scaled distance between centre of the simulation box and grid points along X direction .. 
REAL(DP), ALLOCATABLE, PUBLIC       :: sc_yy_in_sp(:)         ! scaled distance between centre of the simulation box and grid points along Y direction .. 
REAL(DP), ALLOCATABLE, PUBLIC       :: sc_zz_in_sp(:)         ! scaled distance between centre of the simulation box and grid points along Z direction .. 
!
REAL(DP), ALLOCATABLE, PUBLIC       :: selfv(:,:,:)           ! self potential stored in Poisson sphere as guess potential ...
REAL(DP), ALLOCATABLE, PUBLIC       :: pair_dist(:,:,:)       ! pair distance stored in order to extrapolate guess potential ...    
REAL(DP), ALLOCATABLE, PUBLIC       :: pairv(:,:,:,:)         ! pair potential stored in Poisson sphere as guess potential ...
!
REAL(DP), ALLOCATABLE, PUBLIC       :: exx_potential(:, :)    ! EXX potential which is passed/added to the DFT-GGA potential ... 
!
REAL(DP), ALLOCATABLE, PUBLIC       :: clm(:,:)
REAL(DP), ALLOCATABLE, PUBLIC       :: coeke(:,:)
REAL(DP), ALLOCATABLE, PUBLIC       :: vwc(:,:)
!
!==========================================================================
!
! PRIVATE variables 
!
INTEGER, PRIVATE :: nr1,nr2,nr3                           !real space grid dimensions (global first, second, and third dimensions of the 3D grid)
INTEGER, PRIVATE :: nr1r,nr2r,nr3r                        !reduced real space grid dimensions (global first, second, and third dimensions of the 3D grid)
INTEGER, PRIVATE :: ierr                                  !error
REAL(DP), PRIVATE :: h_in(3,3)                            !cell parameter to initialize ... 
!
! PUBLIC subroutines
!
PUBLIC :: exx_initialize
PUBLIC :: exx_finalize
PUBLIC :: getnpinsp 
PUBLIC :: exx_setup 
PUBLIC :: exx_setup_nscf
PUBLIC :: fornberg 
!
! PRIVATE subroutines
!
PRIVATE :: setclm 
!
!
CONTAINS
!
!
!--------------------------------------------------------------------------------------------------------------
SUBROUTINE exx_initialize()
    !
    IMPLICIT NONE
    !
    INTEGER ::  i, iobtl, gindex_of_iobtl, irank, proc, tmp_iobtl 
    !
    ! Start of calculation banner...
    !
    WRITE(stdout,'(/,3X,"Exact Exchange Using Wannier Function Initialization")')
    WRITE(stdout,'(3X,"----------------------------------------------------")')
    !
    WRITE(stdout,'(/,3X,"parameters used in exact exchange calculation",/ &
                   &,5X,"radius to compute pairs:",F6.1,1X,"A.U.",3X,"maximum number of neighbors:",I5)')dis_cutoff,neigh
    !
    WRITE(stdout,'(/,3X,"parameters used to solve Poisson equation",/ &
                   &,5X,"radius for self potential:",F6.1,1X,"A.U."   &
                   &,3X,"radius for pair potential:",F6.1,1X,"A.U.",/ &
                   &,5X,"Poisson solver discretized using",I4,3X,"points in each dimension")')exx_ps_rcut_s,exx_ps_rcut_p,2*nord2+1
    !
    WRITE(stdout,'(/,3X,"parameters used for multipole expansion",/   &
                   &,5X,"radius for self potential:",F6.1,1X,"A.U."   &
                   &,3X,"radius for pair potential:",F6.1,1X,"A.U.",/ &
                   &,5X,"maximum angular momentum:",I4)')exx_me_rcut_s,exx_me_rcut_p,lmax
    !
    ! Error messages for inconsistencies with current version of code...
    !
    IF(nproc_image.LT.nbsp) THEN
      !
      CALL errore('exx_module','EXX calculation is not working when &
                 & number of MPI tasks (nproc_image) less then number of bands (nbsp)',1)
      ! 
    ELSE IF(nproc_image.GE.nbsp) THEN
      !
      IF(MOD(nproc_image,nbsp).NE.0) CALL errore('exx_module','EXX calculation is not working when &
                   & number of MPI tasks (nproc_image) is not integer multiple of number of bands (nbsp)',1)
      !
    END IF
    !
    IF(MOD(dffts%nnr,dffts%nogrp).NE.0) CALL errore('exx_module','EXX calculation is not working when & 
                  & (nr1xnr2) is not integer multiple of number of number of task groups, see file exx_psi.f90',1)
    !
    IF((nproc_image.GT.nbsp).AND.(MOD(dffts%nnr,2).NE.0)) CALL errore('exx_module','EXX calculation is not working when & 
                  & (nr1xnr2) is odd number, see file exx_psi.f90',1)
    !
    IF(MOD(nbsp,2*dffts%nogrp).NE.0) CALL errore('exx_module','EXX calculation is not working when & 
                  & number of bands (nbsp) is not integer multiple of two times the number of task groups, see file exx_psi.f90',1)
    !
    IF(dfftp%nr3*dffts%nogrp.GT.nproc_image) CALL errore('exx_module','EXX calculation is not working when & 
                  & nr3 times number of taskgroups is greater than number of MPI tasks (nproc_image)',1)
    !
    IF(thdyn) THEN
      !
      IF(.NOT. isotropic) CALL errore('exx_module','Variable cell calculation using EXX &
                                      & is only possible for ibrav=1 and cell_dofree="volume"',1) 
      IF(ibrav.NE.1) CALL errore('exx_module','Variable cell calculation using EXX & 
                                 & is only possible for ibrav=1 and cell_dofree="volume"',1) 
      !
    ELSE
      !
      IF(ibrav.EQ.4.OR.ibrav.EQ.5.OR.ibrav.EQ.12.OR.ibrav.EQ.13.OR.ibrav.EQ.14) THEN 
         CALL errore('exx_module','fixed volume calculation using EXX & 
                     & is only possible for orthogonal cells',1) 
      END IF
      !
    END IF
    !
    IF ( lwfpbe0nscf ) THEN
      CALL errore('exx_module','Non self-consistent EXX calculation is possibly not working',1)
    END IF
    !
    !Index of EXX step is initialized ..
    ! 
    n_exx=0
    !
    !Grid sizes are initialized ..
    !
    nr1=dfftp%nr1; nr2=dfftp%nr2; nr3=dfftp%nr3 
    nr1r=nr1/2; nr2r=nr2/2; nr3r=nr3/2 
    IF(MOD(nr1,2).EQ.1) nr1r=(nr1+1)/2
    IF(MOD(nr2,2).EQ.1) nr2r=(nr2+1)/2
    IF(MOD(nr3,2).EQ.1) nr3r=(nr3+1)/2
    !
    ALLOCATE( clm(0:lmax, 0:lmax) )
    CALL setclm(lmax, clm)
    !
    ALLOCATE( coeke(-nord2:nord2, 3))
    ! coeke is computed in subroutine fornberg
    ! fornberg is called in exx_gs.f90
    !
    ! ALLOCATE exx_potential
    !
    IF (nproc_image .LT. nbsp) THEN
        !
        ALLOCATE( exx_potential(dffts%nr1*dffts%nr2*dffts%npp(me_bgrp+1),nbsp) )
        !
    ELSE
        !
        IF ( dffts%have_task_groups ) THEN
            !
            ALLOCATE( exx_potential(dffts%nnr,nproc_image/dffts%nogrp) )
            !
            IF(MOD(nproc_image,dffts%nogrp).NE.0) CALL errore &
               & ('exx_module','EXX calculation is not working when &
               & number of MPI tasks (nproc_image) is not integer multiple of number of taskgroups',1)
            !
        ELSE
            !
            ALLOCATE( exx_potential(dffts%nr1x*dffts%nr2x*dffts%npp(me_bgrp+1),nproc_image) ) !
            !
        END IF
        !
    END IF
    !
    exx_potential=0.0_DP
    !
    ! Compute number of grid points in Poisson and multipole spheres and store
    ! information to interchange between global and local (in sphere) grid indices .. 
    !
    ! Here, if present reference cell parameters will be used. 
    ! Otherwise the current cell parameters from cell_base will be used. 
    ! For variable cell calculations the number of points will vary after
    ! restart if reference cell parameters are not present. 
    !
    h_in=h
    !
   !IF(ref_cell.EQV..TRUE.) h_in=ref_at*ref_alat 
    !
    CALL getnpinsp(exx_ps_rcut_s, exx_me_rcut_s, np_in_sp_s, np_in_sp_me_s )
    CALL getnpinsp(exx_ps_rcut_p, exx_me_rcut_p, np_in_sp_p, np_in_sp_me_p )
    CALL exx_setup( )
    !
    WRITE(stdout,'(/,3X,"number of grid points in Poisson spehere ",/ &
                   &,5X,"self potential:",I8,3X,"pair potential:",I8)')np_in_sp_s,np_in_sp_p
    WRITE(stdout,'(/,3X,"number of grid points in multipole expansion spehere: ",/ &
                   &,5X,"self potential:",I8,3X,"pair potential:",I8)')np_in_sp_me_s,np_in_sp_me_p
    IF ( thdyn ) THEN
      !
      WRITE(stdout,'(/,3X,"This is a variable cell calculation. In this implementation",/ &
                     &,3X,"the number of grid points used in Poisson sphere and",/ &
                     &,3X,"multipole expansion sphere are kept constant as computed",/ &
                     &,3X,"from the initial simulation cell ...")')
      !
     !IF ( .NOT. ref_cell ) WRITE(stdout,'(/,3X,"WARNING :: Number of grid points used in Poisson and",/ &
     !               &,3X, multipole expansion spheres will change when the calculation will restart.",/ &
     !               &,3X,"Use of reference cell parameters is recommended.")')
      !
    END IF
    !
    ! Variables for parallelizion are initialized ....
    !
    WRITE(stdout,'(/,3X,"Task of",I7,3X,"orbitals is distributed over",I7,3X,"number of MPI processes")')nbsp,nproc_image
    !
    IF(nproc_image.LE.nbsp) THEN
        sc_fac = 1
    ELSE
        sc_fac = nproc_image/nbsp
    END IF
    !
    IF(nproc_image .LE. nbsp) THEN
        my_nbspx   = nbsp / nproc_image
        IF( MOD(nbsp, nproc_image) /= 0)THEN
            my_nbspx = my_nbspx + 1
        ENDIF
    ELSE
        my_nbspx   = (nbsp / nproc_image) + 1
    ENDIF
    !
    !print *, 'my_nbspx =', my_nbspx
    !
    ALLOCATE( my_nxyz ( nproc_image ) )
    ALLOCATE( my_nbsp ( nproc_image ) )
    !
    IF(nproc_image .LE. nbsp) THEN
        my_nbsp(:) = nbsp/nproc_image
        IF( MOD(nbsp, nproc_image) /= 0)THEN
            DO i = 1, nproc_image
                IF( (i-1) < MOD(nbsp, nproc_image) ) my_nbsp(i) = my_nbsp(i)+1
            END DO
        ENDIF
    ELSE
        my_nbsp(:) = 1  
    END IF
    !
    ! ** Note that in this case .. 
    ! this is incorrect:   my_nxyz(:) = nr1*nr2*dffts%npp(me_image+1)
    my_nxyz(:) = nr1*nr2*dffts%npp
    !
    !DEBUG
    !WRITE(stdout,'("my_nbsp")')
    !WRITE(stdout,'(20I5)')my_nbsp
    !WRITE(stdout,'("my_nxyz")')
    !WRITE(stdout,'(20I7)')my_nxyz
    !DEBUG
    !
    ALLOCATE( index_my_nbsp (my_nbspx, nproc_image) )
    ALLOCATE( rk_of_obtl ( nbsp ) )
    ALLOCATE( lindex_of_obtl( nbsp ) )
    !
    IF(nproc_image .LE. nbsp) THEN
        index_my_nbsp(:, :) = nbsp + 1
        do irank = 1, nproc_image
            do iobtl = 1, my_nbsp(irank)
                gindex_of_iobtl = iobtl
                do proc = 1, irank - 1, 1
                    gindex_of_iobtl = gindex_of_iobtl + my_nbsp(proc)
                enddo
                if( gindex_of_iobtl <= nbsp)then
                    index_my_nbsp(iobtl, irank) = gindex_of_iobtl
                endif
            enddo
        enddo
    ELSE
        DO proc = 1, nproc_image
            index_my_nbsp(1, proc) = proc
        END DO
    END IF
    !
    !DEBUG
    !WRITE(stdout,'("index_my_nbsp")')
    !WRITE(stdout,'(20I5)')index_my_nbsp(1,:)
    !WRITE(stdout,'(20I5)')index_my_nbsp(2,:)
    !DEBUG
    !
    do iobtl = 1, nbsp
        rk_of_obtl(iobtl) = 0
        tmp_iobtl = iobtl
        do proc = 1, nproc_image
            tmp_iobtl = tmp_iobtl - my_nbsp(proc)
            if(tmp_iobtl <= 0)THEN
                rk_of_obtl(iobtl) = proc - 1
                !print *, 'lrk_of_iobtl=', proc-1, rk_of_obtl(iobtl) 
                exit
            endif
        enddo
    enddo
    !
    !DEBUG
    !WRITE(stdout,'("rk_of_obtl")')
    !WRITE(stdout,'(20I5)')rk_of_obtl
    !DEBUG
    !
    do iobtl = 1, nbsp
        lindex_of_obtl(iobtl) = iobtl
        do proc = 1, nproc_image
            if(lindex_of_obtl(iobtl) <= my_nbsp(proc))exit
            lindex_of_obtl(iobtl) = lindex_of_obtl(iobtl) - my_nbsp(proc)
        enddo
    enddo
    !
    !DEBUG
    !WRITE(stdout,'("lindex_of_obtl")')
    !WRITE(stdout,'(20I5)')lindex_of_obtl
    !DEBUG
    !
    ! Allocate other variables ....
    !
    IF ( lwfpbe0nscf .or. lwfnscf ) ALLOCATE( rhopr( dfftp%nnr, nspin ) )
    !
    IF ( lwfpbe0nscf ) THEN
        ALLOCATE( vwc(3, vnbsp) )
        ALLOCATE( pair_dist( 3, neigh, my_nbspx), stat=ierr )
        pair_dist (:,:,:) = 0.0_DP
        ALLOCATE( pairv( np_in_sp_p, 3, neigh, my_nbspx), stat=ierr )
        pairv (:,:,:,:) = 0.0_DP
    ENDIF
    !
    WRITE(stdout,'(/,3X,"----------------------------------------------------")')
    !
    RETURN
    !
END SUBROUTINE exx_initialize
!
!--------------------------------------------------------------------------------------------------------------
!
SUBROUTINE exx_finalize()
    !
    IF( ALLOCATED( clm )     )        DEALLOCATE( clm)
    IF( ALLOCATED( coeke)    )        DEALLOCATE( coeke)
    IF( ALLOCATED( exx_potential ) )  DEALLOCATE( exx_potential )
    IF( ALLOCATED( rhopr ) )          DEALLOCATE( rhopr )
    IF( ALLOCATED( vwc)    )          DEALLOCATE( vwc )
    IF( ALLOCATED( selfv ) )          DEALLOCATE( selfv )
    IF( ALLOCATED( pairv ) )          DEALLOCATE( pairv )
    IF( ALLOCATED( pair_dist ) )      DEALLOCATE( pair_dist )
    IF( ALLOCATED( my_nxyz ) )        DEALLOCATE( my_nxyz )
    IF( ALLOCATED( my_nbsp ) )        DEALLOCATE( my_nbsp )
    IF( ALLOCATED( index_my_nbsp ) )  DEALLOCATE( index_my_nbsp)
    IF( ALLOCATED( rk_of_obtl ) )     DEALLOCATE( rk_of_obtl)
    IF( ALLOCATED( lindex_of_obtl ) ) DEALLOCATE( lindex_of_obtl )
    !
    !
    IF( ALLOCATED( odtothd_in_sp ) )  DEALLOCATE( odtothd_in_sp )
    IF( ALLOCATED( thdtood_in_sp ) )  DEALLOCATE( thdtood_in_sp )
    IF( ALLOCATED( thdtood  ))        DEALLOCATE( thdtood)
    IF( ALLOCATED( xx_in_sp ))        DEALLOCATE( xx_in_sp )
    IF( ALLOCATED( yy_in_sp ))        DEALLOCATE( yy_in_sp )
    IF( ALLOCATED( zz_in_sp ))        DEALLOCATE( zz_in_sp )
    IF( ALLOCATED( sc_xx_in_sp ))     DEALLOCATE( sc_xx_in_sp )
    IF( ALLOCATED( sc_yy_in_sp ))     DEALLOCATE( sc_yy_in_sp )
    IF( ALLOCATED( sc_zz_in_sp ))     DEALLOCATE( sc_zz_in_sp )
    !
    RETURN
    !
END SUBROUTINE exx_finalize


!======================================================================
! Set up the real-space grid for exact exchange.
! Define two spheres, possion solver is called for potential inside the inner one 
! Multipole expansion is used for points inside the outer one and all other points
! are set to zero.
!
! Adapted from PARSEC by Lingzhu Kong. http://parsec.ices.utexas.edu/
!========================================================================

SUBROUTINE getnpinsp( exx_ps_rcut, exx_me_rcut, np_in_sp, np_in_sp_me )

    IMPLICIT NONE

    REAL(DP) :: exx_ps_rcut, exx_me_rcut
    INTEGER  :: np_in_sp, np_in_sp_me
               
    REAL(DP) :: x,y,z,dqs(3),dq(3),dist
    INTEGER  :: i,j,k,np_in_sp2
    ! --------------------------------------------------------------------
    np_in_sp=0;np_in_sp2=0;np_in_sp_me= 0
    !
    DO k = 1,nr3
        DO j = 1, nr2
            DO i =1, nr1
                !
                ! distances between Grid points and center of the simulation cell in S space
                ! In S space, coordinate of the center of the simulation cell is 0.5,0.5,0.5 for any cell
                dqs(1) = (DBLE(i)/DBLE(nr1)) - 0.5_DP 
                dqs(2) = (DBLE(j)/DBLE(nr2)) - 0.5_DP
                dqs(3) = (DBLE(k)/DBLE(nr3)) - 0.5_DP
                !
                ! Here we are computing distances between Grid points and center of the simulation cell, so no MIC is needed ...
                ! Compute distance between grid point and the center of the simulation cell in R space 
                !
                dq(1)=h_in(1,1)*dqs(1)+h_in(1,2)*dqs(2)+h_in(1,3)*dqs(3)   !r_i = h s_i
                dq(2)=h_in(2,1)*dqs(1)+h_in(2,2)*dqs(2)+h_in(2,3)*dqs(3)   !r_i = h s_i
                dq(3)=h_in(3,1)*dqs(1)+h_in(3,2)*dqs(2)+h_in(3,3)*dqs(3)   !r_i = h s_i
                !
                dist = DSQRT(dq(1)*dq(1)+dq(2)*dq(2)+dq(3)*dq(3))
                !
                IF(dist .LE. exx_ps_rcut)then
                    !
                    np_in_sp = np_in_sp + 1
                    !
                    ELSEIF(dist .LE. exx_me_rcut)then
                    !
                    np_in_sp2 = np_in_sp2 + 1
                    !
                END IF
                !
                np_in_sp_me = np_in_sp+np_in_sp2
                !
            END DO !i
        END DO !j
    END DO !k
    !
    RETURN
END SUBROUTINE getnpinsp

!======================================================================
!
SUBROUTINE exx_setup( )

    IMPLICIT NONE

    ! ====================================================================
    ! INPUT VARIABLES

    ! LOCAL VARIABLES
    INTEGER  :: np, npsp1, npsp2, npsp3, npsp4, np_tmp_1, np_tmp_2 
    REAL(DP) :: x,y,z, dq(3),dqs(3),dist, rcut_sp2, rcut_sp3
    INTEGER  :: i,j,k, ierr, tmp
    ! --------------------------------------------------------------------
    !
    !! This part would be necessary if number of points in PS and ME sphere
    !! change every step
    !IF(n_exx.GT.0) THEN 
    !    IF( ALLOCATED( odtothd_in_sp ) )  DEALLOCATE(odtothd_in_sp )
    !    IF( ALLOCATED( thdtood_in_sp ) )  DEALLOCATE(thdtood_in_sp )
    !    IF( ALLOCATED( thdtood  ))        DEALLOCATE(thdtood)
    !    IF( ALLOCATED( xx_in_sp ))        DEALLOCATE(xx_in_sp )
    !    IF( ALLOCATED( yy_in_sp ))        DEALLOCATE(yy_in_sp )
    !    IF( ALLOCATED( zz_in_sp ))        DEALLOCATE(zz_in_sp )
    !END IF
    !
    ALLOCATE( odtothd_in_sp(3, np_in_sp_me_s ), stat=ierr )
    ALLOCATE( thdtood_in_sp(nr1, nr2, nr3), stat=ierr )
    ALLOCATE( thdtood(nr1, nr2, nr3), stat=ierr )
    ALLOCATE( xx_in_sp(1:np_in_sp_me_s), stat=ierr )
    ALLOCATE( yy_in_sp(1:np_in_sp_me_s), stat=ierr )
    ALLOCATE( zz_in_sp(1:np_in_sp_me_s), stat=ierr )
    ALLOCATE( sc_xx_in_sp(1:np_in_sp_me_s), stat=ierr )
    ALLOCATE( sc_yy_in_sp(1:np_in_sp_me_s), stat=ierr )
    ALLOCATE( sc_zz_in_sp(1:np_in_sp_me_s), stat=ierr )
    !
    xx_in_sp=0.0_DP; sc_xx_in_sp=0.0_DP
    yy_in_sp=0.0_DP; sc_yy_in_sp=0.0_DP
    zz_in_sp=0.0_DP; sc_zz_in_sp=0.0_DP
    !
    thdtood_in_sp = 0; odtothd_in_sp = 0; thdtood = 0
    !
    !this is the cutoff acording to the size of sphere
    !
    rcut_sp2=MIN(exx_me_rcut_p,exx_ps_rcut_s)
    rcut_sp3=MAX(exx_me_rcut_p,exx_ps_rcut_s)
    np_tmp_1=MIN(np_in_sp_s,np_in_sp_me_p)
    np_tmp_2=MAX(np_in_sp_s,np_in_sp_me_p)
    np    = 0
    npsp1 = 0; npsp2 = 0; npsp3 = 0; npsp4 = 0
    !
    DO k = 1,nr3
        DO j = 1, nr2
            DO i =1, nr1
                !
                np = np + 1
                !
                thdtood(i,j,k) = np
                !
                ! distances between Grid points and center of the simulation cell in S space
                ! center of the box coordinate in S space is 0.5,0.5,0.5 for any cell
                dqs(1) = (DBLE(i)/DBLE(nr1)) - 0.5_DP 
                dqs(2) = (DBLE(j)/DBLE(nr2)) - 0.5_DP
                dqs(3) = (DBLE(k)/DBLE(nr3)) - 0.5_DP
                !
                ! Here we are computing distances between Grid points and center of the simulation cell, so no MIC is needed ...
                ! Compute distance between grid point and the center of the simulation cell in R space 
                !
                dq(1)=h_in(1,1)*dqs(1)+h_in(1,2)*dqs(2)+h_in(1,3)*dqs(3)   !r_i = h s_i
                dq(2)=h_in(2,1)*dqs(1)+h_in(2,2)*dqs(2)+h_in(2,3)*dqs(3)   !r_i = h s_i
                dq(3)=h_in(3,1)*dqs(1)+h_in(3,2)*dqs(2)+h_in(3,3)*dqs(3)   !r_i = h s_i
                !
                dist = DSQRT(dq(1)*dq(1)+dq(2)*dq(2)+dq(3)*dq(3))
                !
                IF(dist .LE. exx_ps_rcut_p)THEN
                    npsp1 = npsp1 + 1
                    thdtood_in_sp(i,j,k)  = npsp1
                    odtothd_in_sp(1,npsp1) = i
                    odtothd_in_sp(2,npsp1) = j
                    odtothd_in_sp(3,npsp1) = k
                    !
                    xx_in_sp(npsp1) = dq(1); sc_xx_in_sp(npsp1) = dqs(1)
                    yy_in_sp(npsp1) = dq(2); sc_yy_in_sp(npsp1) = dqs(2)
                    zz_in_sp(npsp1) = dq(3); sc_zz_in_sp(npsp1) = dqs(3)
                    !
                    ELSEIF(dist .LE. rcut_sp2)THEN
                    !
                    npsp2 = npsp2 + 1
                    tmp = npsp2 + np_in_sp_p
                    thdtood_in_sp(i,j,k)  = tmp
                    odtothd_in_sp(1, tmp) = i
                    odtothd_in_sp(2, tmp) = j
                    odtothd_in_sp(3, tmp) = k
                    !
                    xx_in_sp(tmp) = dq(1); sc_xx_in_sp(tmp) = dqs(1)
                    yy_in_sp(tmp) = dq(2); sc_yy_in_sp(tmp) = dqs(2)
                    zz_in_sp(tmp) = dq(3); sc_zz_in_sp(tmp) = dqs(3)
                    !
                    ELSEIF(dist .LE. rcut_sp3)THEN
                    !
                    npsp3 = npsp3 + 1
                    tmp = npsp3 + np_tmp_1
                    thdtood_in_sp(i,j,k)  = tmp
                    odtothd_in_sp(1, tmp) = i
                    odtothd_in_sp(2, tmp) = j
                    odtothd_in_sp(3, tmp) = k
                    !
                    xx_in_sp(tmp) = dq(1); sc_xx_in_sp(tmp) = dqs(1)
                    yy_in_sp(tmp) = dq(2); sc_yy_in_sp(tmp) = dqs(2)
                    zz_in_sp(tmp) = dq(3); sc_zz_in_sp(tmp) = dqs(3)
                    !
                    ELSEIF(dist .LE. exx_me_rcut_s)THEN
                    !
                    npsp4 = npsp4 + 1
                    tmp = npsp4 + np_tmp_2
                    thdtood_in_sp(i,j,k)  = tmp
                    odtothd_in_sp(1, tmp) = i
                    odtothd_in_sp(2, tmp) = j
                    odtothd_in_sp(3, tmp) = k
                    !
                    xx_in_sp(tmp) = dq(1); sc_xx_in_sp(tmp) = dqs(1)
                    yy_in_sp(tmp) = dq(2); sc_yy_in_sp(tmp) = dqs(2)
                    zz_in_sp(tmp) = dq(3); sc_zz_in_sp(tmp) = dqs(3)
                    !
                ENDIF
            ENDDO
        ENDDO
    ENDDO

    if( npsp1 .ne. np_in_sp_p)then
        write(stdout, *)&
            'number of points in the 1st shell does not match', npsp1, np_in_sp_p
        write(stdout, *)'STOP in exx_setup'
        return
    endif

    if( npsp2 .ne. np_tmp_1-np_in_sp_p)then
        write(stdout,*)&
            'number of points in the 2nd shell does not match', npsp2, np_tmp_1-np_in_sp_p
        write(stdout, *)'STOP in exx_setup'
        return
    endif

    if( npsp3 .ne. np_tmp_2-np_tmp_1)then
        write(stdout,*)&
            'number of points in the 3rd shell does not match', npsp3, np_tmp_2-np_tmp_1
        write(stdout, *)'STOP in exx_setup'
        return
    endif

    if( npsp4 .ne. np_in_sp_me_s-np_tmp_2)then
        write(stdout,*)&
            'number of points in the 4th shell does not match', npsp4, np_in_sp_me_s-np_tmp_2
        write(stdout, *)'STOP in exx_setup'
        return
    endif

    RETURN

END SUBROUTINE exx_setup

!======================================================================
!
SUBROUTINE exx_setup_nscf( nnrtot, lpole, clm, factor, wc, vwc, nbsp, vnbsp )

    IMPLICIT NONE

    ! ====================================================================
    ! INPUT VARIABLES
    INTEGER  nnrtot, lpole, nbsp, vnbsp
    REAL(DP) factor, wc(3, nbsp), vwc(3, vnbsp)
    REAL(DP) clm(0:lpole, 0:lpole)

    ! ====================================================================
    !      integer  odtothd_in_sp(3,np_in_sp), odtothd_in_sp2(3,np_in_sp2)
    !      integer  thdtood_in_sp(nr1, nr2, nr3),thdtood(nr1,nr2,nr3)
    ! ====================================================================
    ! LOCAL VARIABLES
    INTEGER   np, npsp, npsp2
    REAL(DP)  x,y,z, dist, exx_ps_rcut, exx_me_rcut
    INTEGER   i,j,k, ierr, tmp,np_in_sp
    REAL(DP) :: lenA,lenB,lenC                       !length of lattice  A, B, C in a.u.
    REAL(DP) :: centerx,centery,centerz              !coordinate of the center of the simulation box
    REAL(DP) :: hx,hy,hz                             !grid spacing along lattice directions
    ! --------------------------------------------------------------------

    exx_ps_rcut=exx_ps_rcut_s
    exx_me_rcut=exx_me_rcut_s
    np_in_sp=np_in_sp_s

    ! should work for all type of cells
    !
    lenA=DSQRT(h(1,1)*h(1,1)+h(2,1)*h(2,1)+h(3,1)*h(3,1))
    lenB=DSQRT(h(1,2)*h(1,2)+h(2,2)*h(2,2)+h(3,2)*h(3,2))
    lenC=DSQRT(h(1,3)*h(1,3)+h(2,3)*h(2,3)+h(3,3)*h(3,3))
    ! 
    hx = lenA / nr1  !grid spacing in Lattice 1 direction
    hy = lenB / nr2  !grid spacing in Lattice 2 direction
    hz = lenC / nr3  !grid spacing in Lattice 3 direction
    !
    centerx = 0.5_DP * lenA 
    centery = 0.5_DP * lenB
    centerz = 0.5_DP * lenC
    !
    ! For points in the 1st sphere, one needs to know if its finite-difference neighbors
    ! are inside or outside. We set the thdtood_in_sp to be np_in_sp + 1 for outside neighbors
    ALLOCATE( odtothd_in_sp(3, np_in_sp_me_s ), stat=ierr )
    ALLOCATE( thdtood_in_sp(nr1, nr2, nr3), stat=ierr )
    ALLOCATE( thdtood(nr1, nr2, nr3), stat=ierr )
    ALLOCATE( xx_in_sp(1:np_in_sp_me_s), stat=ierr )
    ALLOCATE( yy_in_sp(1:np_in_sp_me_s), stat=ierr )
    ALLOCATE( zz_in_sp(1:np_in_sp_me_s), stat=ierr )
    !
    xx_in_sp=0.0_DP
    yy_in_sp=0.0_DP
    zz_in_sp=0.0_DP
    !
    thdtood_in_sp = 0
    odtothd_in_sp = 0
    thdtood = 0
    !
    np    = 0
    npsp  = 0
    npsp2 = 0
    do k = 1,nr3
        do j = 1, nr2
            do i =1, nr1
                np = np + 1
                thdtood(i,j,k) = np

                x = i * hx -centerx
                y = j * hy -centery
                z = k * hz -centerz
                dist = sqrt(x*x + y*y + z*z)

                if(dist .le. exx_ps_rcut)then
                    npsp = npsp + 1
                    thdtood_in_sp(i,j,k)  = npsp
                    odtothd_in_sp(1,npsp) = i
                    odtothd_in_sp(2,npsp) = j
                    odtothd_in_sp(3,npsp) = k

                    xx_in_sp(npsp) = x
                    yy_in_sp(npsp) = y
                    zz_in_sp(npsp) = z
                    elseif(dist .le. exx_me_rcut)then
                    npsp2 = npsp2 + 1
                    tmp = npsp2 + np_in_sp
                    thdtood_in_sp(i,j,k)  = tmp
                    odtothd_in_sp(1, tmp) = i
                    odtothd_in_sp(2, tmp) = j
                    odtothd_in_sp(3, tmp) = k

                    xx_in_sp(tmp) = x
                    yy_in_sp(tmp) = y
                    zz_in_sp(tmp) = z

                endif
            enddo
        enddo
    enddo

    write(6,*)' npsp in exx_setup =', npsp, npsp2
    if( npsp .ne. np_in_sp)then
        write(6, *)'number of points in the 1st sphere does not match', npsp, np_in_sp
        write(6, *)'STOP in exx_setup'
        return
    endif

    if( npsp2+npsp .ne. np_in_sp_me_s)then
        write(6,*)'number of points in the 2nd sphere does not match', npsp2+npsp, np_in_sp_me_s
        write(6, *)'STOP in exx_setup'
        return
    endif

    !========================================================================
    call fornberg(2,nord2,coeke(:,1),ierr)

    if (ierr .ne. 0) then
        write(6,*) ' ERROR: Wrong parameter in call of Fornberg'
        write(6,*) ' STOP in init_var'
        return
    endif

    !      renormalize coekes with respect to the grid spacing
    coeke(:,3) = -coeke(:,1)/(hz*hz*factor)
    coeke(:,2) = -coeke(:,1)/(hy*hy*factor)
    coeke(:,1) = -coeke(:,1)/(hx*hx*factor)
    !==========================================================================

    CALL setclm(lpole, clm)

    call getwc(wc, vwc, vnbsp, nbsp, h(1,1), h(2,2), h(3,3))

    RETURN

END SUBROUTINE exx_setup_nscf


!==========================================================================
SUBROUTINE getwc(wc, vwc, vnbsp, nbsp, a1, a2, a3)

    IMPLICIT NONE

    INTEGER     vnbsp, nbsp, ir
    REAl(DP)    wc(3, nbsp), vwc(3, vnbsp), a1, a2, a3

    do ir = 1, nbsp
        read(407, *) wc(1,ir), wc(2,ir), wc(3,ir)
    end do

    do ir = 1, vnbsp
        read(408,*)vwc(1,ir), vwc(2,ir), vwc(3,ir)
    enddo

    do ir = 1, vnbsp
        if (vwc(1, ir) < 0) then
            vwc(1,ir) = vwc(1,ir) + a1
        end if
        if (vwc(2, ir) < 0) then
            vwc(2,ir) = vwc(2,ir) + a2
        end if
        if (vwc(3, ir) < 0) then
            vwc(3,ir) = vwc(3,ir) + a3
        end if
    end do

    do ir = 1, nbsp
        if (wc(1, ir) < 0) then
            wc(1,ir) = wc(1,ir) + a1
        end if
        if (wc(2, ir) < 0) then
            wc(2,ir) = wc(2,ir) + a2
        end if
        if (wc(3, ir) < 0) then
            wc(3,ir) = wc(3,ir) + a3
        end if
    end do

    RETURN
END SUBROUTINE getwc

! ==================================================================
! SUBROUTINE to set the various clm coefficients. Separated so as to
! not clutter up the above code. calculates clm = (l-m)!/(l+m)! for 
! l = 0,lpole, m=0,l. 
! evaluating these to 20 decimal places is certainly as accurate 
! as doing the calculations in the code but less work.
! ==================================================================
SUBROUTINE setclm(lpole, clm)

    implicit none
    ! INPUT: order of mutipole expansion
    integer lpole
    ! OUTPUT: clm coefficients
    real*8 clm(0:lpole, 0:lpole)

    clm(0,0) = 1.00000000000000000000e+00
    if (lpole .ge. 1) then
        clm(1,0) = 1.00000000000000000000e+00
        clm(1,1) = 5.00000000000000000000e-01
    endif
    if (lpole .ge. 2) then
        clm(2,0) = 1.00000000000000000000e+00
        clm(2,1) = 1.66666666666666666670e-01
        clm(2,2) = 4.16666666666666666670e-02
    endif
    if (lpole .ge. 3) then
        clm(3,0) = 1.00000000000000000000e+00
        clm(3,1) = 8.33333333333333333330e-02
        clm(3,2) = 8.33333333333333333330e-03
        clm(3,3) = 1.38888888888888888890e-03
    endif
    if (lpole .ge. 4) then
        clm(4,0) = 1.00000000000000000000e+00
        clm(4,1) = 5.00000000000000000000e-02
        clm(4,2) = 2.77777777777777777780e-03
        clm(4,3) = 1.98412698412698412700e-04
        clm(4,4) = 2.48015873015873015870e-05
    endif
    if (lpole .ge. 5) then
        clm(5,0) = 1.00000000000000000000e+00
        clm(5,1) = 3.33333333333333333330e-02
        clm(5,2) = 1.19047619047619047620e-03
        clm(5,3) = 4.96031746031746031750e-05
        clm(5,4) = 2.75573192239858906530e-06
        clm(5,5) = 2.75573192239858906530e-07
    endif
    if (lpole .ge. 6) then
        clm(6,0) = 1.00000000000000000000e+00
        clm(6,1) = 2.38095238095238095240e-02
        clm(6,2) = 5.95238095238095238100e-04
        clm(6,3) = 1.65343915343915343920e-05
        clm(6,4) = 5.51146384479717813050e-07
        clm(6,5) = 2.50521083854417187750e-08
        clm(6,6) = 2.08767569878680989790e-09
    endif
    if (lpole .ge. 7) then
        clm(7,0) = 1.00000000000000000000e+00
        clm(7,1) = 1.78571428571428571430e-02
        clm(7,2) = 3.30687830687830687830e-04
        clm(7,3) = 6.61375661375661375660e-06
        clm(7,4) = 1.50312650312650312650e-07
        clm(7,5) = 4.17535139757361979580e-09
        clm(7,6) = 1.60590438368216145990e-10
        clm(7,7) = 1.14707455977297247140e-11
    endif
    if (lpole .ge. 8) then
        clm(8,0) = 1.00000000000000000000e+00
        clm(8,1) = 1.38888888888888888890e-02
        clm(8,2) = 1.98412698412698412700e-04
        clm(8,3) = 3.00625300625300625300e-06
        clm(8,4) = 5.01042167708834375500e-08
        clm(8,5) = 9.63542630209296875960e-10
        clm(8,6) = 2.29414911954594494280e-11
        clm(8,7) = 7.64716373181981647590e-13
        clm(8,8) = 4.77947733238738529740e-14
    endif
    if (lpole .ge. 9) then
        clm(9,0) = 1.00000000000000000000e+00
        clm(9,1) = 1.11111111111111111110e-02
        clm(9,2) = 1.26262626262626262630e-04
        clm(9,3) = 1.50312650312650312650e-06
        clm(9,4) = 1.92708526041859375190e-08
        clm(9,5) = 2.75297894345513393130e-10
        clm(9,6) = 4.58829823909188988550e-12
        clm(9,7) = 9.55895466477477059490e-14
        clm(9,8) = 2.81145725434552076320e-15
        clm(9,9) = 1.56192069685862264620e-16
    endif

    RETURN
END SUBROUTINE setclm  

!===============================================================
!
!Coefficients for the first & second order numerical derivative
!under the centered finite difference scheme.
!Bengt Fornberg,  Exxon Res. & Eng. Co., NJ 08801
!'bfornbe@erenj.com'
!David M. Sloan,  Dept. of Mathematics, U. of Strathclyde,
!Glasgow G1 1HX, Scotland,  'd.sloan@strath.ac.uk'
!Acta Numerica 94,  Cambridge Univ. Press (1994)
!
!---------------------------------------------------------------
SUBROUTINE fornberg(iddd,norder,coe,ierr)

    use kinds, only : DP
    implicit none
    !
    !     Input/Output variables:
    !
    !     order of expansion of derivative. 
    !     it is the number of neighbors used ON ONE SIDE.
    !     the maximum order implemented is 20.
    integer, intent(in) :: norder

    !     iddd - order of the derivative (iddd = 1 or 2)
    integer, intent(in) :: iddd

    !     coe - coefficients for the derivative
    real(dp), intent(out) :: coe(-norder:norder)

    !     ierr - error flag
    integer, intent(out) :: ierr
    !     
    !     Work variables:
    !
    !     counters 
    integer i
    !     ---------------------------------------------------------------
    !
    !     First order derivative
    !
    ierr = 0
    if(iddd.eq.1) then
        !
        select case (norder)
        case (1)
            coe(1) =  0.50000000000000D+00
        case (2)
            coe(1) =  2.d0/3.d0
            coe(2) = -1.d0/12.d0
        case (3)
            coe(1) =  3.d0/4.d0
            coe(2) = -3.d0/20.d0
            coe(3) =  1.d0/60.d0
        case (4)   
            coe(1) =  4.d0/5.d0
            coe(2) = -1.d0/5.d0
            coe(3) =  4.d0/105.d0
            coe(4) = -1.d0/280.d0
        case (5)     
            coe(1) =  0.8333333333D+00
            coe(2) = -0.2380952381D+00
            coe(3) =  0.5952380952D-01
            coe(4) = -0.9920634921D-02
            coe(5) =  0.7936507937D-03
        case (6)    
            coe(1) =  0.8571428571D+00
            coe(2) = -0.2678571429D+00
            coe(3) =  0.7936507937D-01
            coe(4) = -0.1785714286D-01
            coe(5) =  0.2597402597D-02
            coe(6) = -0.1803751804D-03
        case (7)    
            coe(1) =  0.8750000000D+00
            coe(2) = -0.2916666667D+00
            coe(3) =  0.9722222222D-01
            coe(4) = -0.2651515152D-01
            coe(5) =  0.5303030303D-02
            coe(6) = -0.6798756799D-03
            coe(7) =  0.4162504163D-04
        case (8)    
            coe(1) =  0.8888888889D+00
            coe(2) = -0.3111111111D+00
            coe(3) =  0.1131313131D+00
            coe(4) = -0.3535353535D-01
            coe(5) =  0.8702408702D-02
            coe(6) = -0.1554001554D-02
            coe(7) =  0.1776001776D-03
            coe(8) = -0.9712509713D-05
        case (9)      
            coe(1) =  0.9000000000D+00
            coe(2) = -0.3272727273D+00
            coe(3) =  0.1272727273D+00
            coe(4) = -0.4405594406D-01
            coe(5) =  0.1258741259D-01
            coe(6) = -0.2797202797D-02
            coe(7) =  0.4495504496D-03
            coe(8) = -0.4627725216D-04
            coe(9) =  0.2285296403D-05
        case (10)    
            coe(1) =  0.9090909091D+00
            coe(2) = -0.3409090909D+00
            coe(3) =  0.1398601399D+00
            coe(4) = -0.5244755245D-01
            coe(5) =  0.1678321678D-01
            coe(6) = -0.4370629371D-02
            coe(7) =  0.8814714697D-03
            coe(8) = -0.1285479227D-03
            coe(9) =  0.1202787580D-04
            coe(10)= -0.5412544112D-06
        end select

        coe(0) = 0.d0
        do i = 1,norder
            coe(-i) = -coe(i)
        enddo
        !
        !     Second order derivative
        !
    else if (iddd.eq.2) then

        select case (norder)
        case (1)
            coe(0) = -0.20000000000000D+01
            coe(1) =  0.10000000000000D+01
        case (2) 
            coe(0) = -0.25000000000000D+01
            coe(1) =  0.13333333333333D+01
            coe(2) = -0.83333333333333D-01
        case (3)
            coe(0) = -0.27222222222222D+01
            coe(1) =  0.15000000000000D+01
            coe(2) = -0.15000000000000D+00
            coe(3) =  0.11111111111111D-01
        case (4)
            coe(0) = -0.28472222222222D+01
            coe(1) =  0.16000000000000D+01
            coe(2) = -0.20000000000000D+00
            coe(3) =  0.25396825396825D-01
            coe(4) = -0.17857142857143D-02
        case (5)
            coe(0) = -0.29272222222222D+01
            coe(1) =  0.16666666666667D+01
            coe(2) = -0.23809523809524D+00
            coe(3) =  0.39682539682540D-01
            coe(4) = -0.49603174603175D-02
            coe(5) =  0.31746031746032D-03
        case (6)
            coe(0) = -0.29827777777778D+01
            coe(1) =  0.17142857142857D+01
            coe(2) = -0.26785714285714D+00
            coe(3) =  0.52910052910053D-01
            coe(4) = -0.89285714285714D-02
            coe(5) =  0.10389610389610D-02
            coe(6) = -0.60125060125060D-04
        case (7)
            coe(0) = -0.30235941043084D+01
            coe(1) =  0.17500000000000D+01
            coe(2) = -0.29166666666667D+00
            coe(3) =  0.64814814814815D-01
            coe(4) = -0.13257575757576D-01
            coe(5) =  0.21212121212121D-02
            coe(6) = -0.22662522662523D-03
            coe(7) =  0.11892869035726D-04
        case (8)
            coe(0) = -0.30548441043084D+01
            coe(1) =  0.17777777777778D+01
            coe(2) = -0.31111111111111D+00
            coe(3) =  0.75420875420875D-01
            coe(4) = -0.17676767676768D-01
            coe(5) =  0.34809634809635D-02
            coe(6) = -0.51800051800052D-03
            coe(7) =  0.50742907885765D-04
            coe(8) = -0.24281274281274D-05
        case (9)
            coe(0) = -0.30795354623331D+01
            coe(1) =  0.18000000000000D+01
            coe(2) = -0.32727272727273D+00
            coe(3) =  0.84848484848485D-01
            coe(4) = -0.22027972027972D-01
            coe(5) =  0.50349650349650D-02
            coe(6) = -0.93240093240093D-03
            coe(7) =  0.12844298558584D-03
            coe(8) = -0.11569313039901D-04
            coe(9) =  0.50784364509855D-06
        case (10)
            coe(0) = -0.30995354623331D+01
            coe(1) =  0.18181818181818D+01
            coe(2) = -0.34090909090909D+00
            coe(3) =  0.93240093240093D-01
            coe(4) = -0.26223776223776D-01
            coe(5) =  0.67132867132867D-02
            coe(6) = -0.14568764568765D-02
            coe(7) =  0.25184899134479D-03
            coe(8) = -0.32136980666392D-04
            coe(9) =  0.26728612899924D-05
            coe(10)= -0.10825088224469D-06
        end select

        do i = 1,norder
            coe(-i) = coe(i)
        end do
        !
    else
        write(6,*) ' ERROR: invalid derivative order, iddd = ',iddd
        write(6,*) ' STOP in FORNBERG '
        ierr = 1 
        return
    endif

    RETURN
END SUBROUTINE fornberg
!
!
END MODULE exx_module
