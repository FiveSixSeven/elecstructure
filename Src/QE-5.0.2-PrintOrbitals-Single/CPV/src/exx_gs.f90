


SUBROUTINE exx_gs(nfi, c)

!=======================================================================================
! modified from exact_exchange.f90 written by Zhaofeng and Xifan.
! In the original code, each processor holds one wave function. 
! Here I modify it so each processor holds nbsp/nproc_image
! number of wave functions. 
! Lingzhu Kong
! BS / RAD : Nov 2012
! This code will work when number of processors is equal to or greater than number of bands. 
! At present the number of processors has to be integer multiple of the number of bands. 
!=======================================================================================

 USE kinds,                   ONLY  : DP
 USE constants,               ONLY  : fpi
 USE control_flags,           ONLY  : lwfpbe0            !if .TRUE. then PBE0 calculation using Wannier orbitals is turned on ... 
 USE fft_base,                ONLY  : dffts, dfftp
 USE mp,                      ONLY  : mp_barrier, mp_sum
 USE mp_global,               ONLY  : nproc_image, me_image, root_image, intra_image_comm, intra_bgrp_comm, me_bgrp
 USE parallel_include
 USE io_global,               ONLY  : stdout
 USE cell_base,               ONLY  : omega, ainv, h
 USE electrons_base,          ONLY  : nbsp, nbspx, nspin
 USE gvecw,                   ONLY  : ngw
 USE wannier_module,          ONLY  : wfc
 USE exx_module,              ONLY  : my_nbspx, my_nbsp, my_nxyz, index_my_nbsp,rk_of_obtl, lindex_of_obtl
 USE exx_module,              ONLY  : selfv, pairv, pair_dist
 USE exx_module,              ONLY  : exx_potential
 USE exx_module,              ONLY  : n_exx,sc_fac
 USE exx_module,              ONLY  : coeke, nord2, fornberg
 USE exx_module,              ONLY  : thdtood, odtothd_in_sp, thdtood_in_sp
 USE exx_module,              ONLY  : np_in_sp_s   , np_in_sp_me_s   , np_in_sp_p   , np_in_sp_me_p
 USE exx_module,              ONLY  : xx_in_sp,yy_in_sp,zz_in_sp,sc_xx_in_sp,sc_yy_in_sp,sc_zz_in_sp 
 USE energies,                ONLY  : exx
 USE printout_base,           ONLY  : printout_base_open, printout_base_unit, printout_base_close
 USE wannier_base,            ONLY  : neigh, dis_cutoff
 USE mp_wave,                 ONLY  : redistwfr

 IMPLICIT NONE
 COMPLEX(DP)   c(ngw, nbspx)
 
 INTEGER  :: istatus(MPI_STATUS_SIZE)

 INTEGER     ir, ip, i, j,nfi, ierr, nnrtot, nr1s,nr2s,nr3s
 INTEGER     nj_max
 REAl(DP)    sa1,a(3),ha, hb, hc
 REAl(DP)    hcub, centerx, centery, centerz
 REAL(DP)    middle(3,neigh/2)
 ! HK: pair distance
 REAL(DP)    d_pair(neigh/2)
 
 REAL(DP),    ALLOCATABLE ::   vpsil(:,:)
 REAL(DP),    ALLOCATABLE ::   rhol(:),rho_in_sp(:),vl(:)
 REAL(DP),    ALLOCATABLE ::   psi(:,:)

 INTEGER   iobtl, gindex_of_iobtl, irank, rk_of_obtl_trcv, rk_of_obtl_tbs
 INTEGER   obtl_tbs, lindex_obtl_tbs, obtl_trcv, lindex_obtl_trcv 
 REAL(DP)  totalenergy, totalenergyg, tot_energy(nbsp)
 REAL(DP)  selfe, paire(neigh/2)

 INTEGER, allocatable  :: isendreq(:),irecvreq(:,:)

 INTEGER   tran(3), proc, tmp_iobtl, me

 ! BS
 INTEGER                  :: k,jj,ii,ia,ib,ic,my_var,my_var2,my_var3,i_fac,va,cgstep
 INTEGER                  :: ndim,nogrp 
 INTEGER,    ALLOCATABLE  :: overlap(:,:),njj(:)
 REAL(DP),   ALLOCATABLE  :: BS_selfe(:)
 REAl(DP),   ALLOCATABLE  :: wannierc(:,:),wannierc_tmp(:,:)
 REAl(DP),   ALLOCATABLE  :: psil(:),psil_pair_recv(:,:),psil_pair_send(:,:,:)
 REAL(DP),   ALLOCATABLE  :: exx_tmp(:,:),exx_tmp3(:,:)
 INTEGER,    ALLOCATABLE  :: sdispls(:), sendcount(:)
 INTEGER,    ALLOCATABLE  :: rdispls(:), recvcount(:)
 INTEGER,    ALLOCATABLE  :: sdispls1(:), sendcount1(:)
 INTEGER,    ALLOCATABLE  :: rdispls1(:), recvcount1(:)
 ! BS
 !=============================================================================================
 !print *, 'entering exx_gs', 'n_exx', n_exx
 ! 
 call start_clock('exx_gs_setup')

 me = me_image+1

 nr1s=dfftp%nr1; nr2s=dfftp%nr2; nr3s=dfftp%nr3 

 a(1)=DSQRT(h(1,1)*h(1,1)+h(2,1)*h(2,1)+h(3,1)*h(3,1))   ! lattice 1 
 a(2)=DSQRT(h(1,2)*h(1,2)+h(2,2)*h(2,2)+h(3,2)*h(3,2))   ! lattice 2 
 a(3)=DSQRT(h(1,3)*h(1,3)+h(2,3)*h(2,3)+h(3,3)*h(3,3))   ! lattice 3 

 ha = a(1) / DBLE(nr1s)  !grid spacing in Lattice 1 direction
 hb = a(2) / DBLE(nr2s)  !grid spacing in Lattice 2 direction
 hc = a(3) / DBLE(nr3s)  !grid spacing in Lattice 3 direction

 nnrtot = nr1s * nr2s * nr3s
 hcub = omega / DBLE(nnrtot) !nnrtot in parallel

 centerx = 0.5_DP * a(1)
 centery = 0.5_DP * a(2)
 centerz = 0.5_DP * a(3)

 sa1 = 1.0_DP/omega

 !========================================================================
 ! Compute distances between grid points and the center of the simulation cell in R space 
 ! This part needs to be done once in constant volume simulation and
 ! needs to be done every step in variable cell simulationulations ...
 !
 DO i=1,np_in_sp_me_s
   !
   xx_in_sp(i)=h(1,1)*sc_xx_in_sp(i)+h(1,2)*sc_yy_in_sp(i)+h(1,3)*sc_zz_in_sp(i)   ! r = h s
   yy_in_sp(i)=h(2,1)*sc_xx_in_sp(i)+h(2,2)*sc_yy_in_sp(i)+h(2,3)*sc_zz_in_sp(i)   ! r = h s
   zz_in_sp(i)=h(3,1)*sc_xx_in_sp(i)+h(3,2)*sc_yy_in_sp(i)+h(3,3)*sc_zz_in_sp(i)   ! r = h s
   !
 END DO
 !========================================================================
 ! Compute coeke and renormalize
 ! This part needs to be done once in constant volume simulation and
 ! needs to be done every step in variable cell simulationulations ...
 !
 CALL fornberg(2,nord2,coeke(:,1),ierr)
 !
 IF (ierr .ne. 0) THEN
     WRITE(stdout,*) ' ERROR: Wrong parameter in call of Fornberg'
     WRITE(stdout,*) ' STOP in exx_gs'
     RETURN
 END IF
 !
 !RENORMALIZE COEKES WITH RESPECT TO THE GRID SPACING
 coeke(:,3) = -coeke(:,1)/(hc*hc*fpi)
 coeke(:,2) = -coeke(:,1)/(hb*hb*fpi)
 coeke(:,1) = -coeke(:,1)/(ha*ha*fpi)
 !========================================================================
 !
 call stop_clock('exx_gs_setup')
 !
 !-------------------------------------------------------------------------
 ! Get the Wannier center and compute the pair overlap matrix 
 !-------------------------------------------------------------------------
 !
 call start_clock('exx_pairs')
 !
 ndim=MAX(nproc_image,nbsp)
 ALLOCATE (wannierc(3,ndim)); wannierc=0.0_DP 
 ALLOCATE (wannierc_tmp(3,nbsp)); wannierc_tmp=0.0_DP 
 !
 ! Adjust Cartesian coordinates of wannier centres according to periodic boundary conditions...
 ! N.B.: PBC are imposed here in the range [0,1)... 
 !
 DO iobtl=1,nbsp
   !
   wannierc_tmp(1,iobtl)=ainv(1,1)*wfc(1,iobtl)+ainv(1,2)*wfc(2,iobtl)+ainv(1,3)*wfc(3,iobtl)   ! s = h^-1 r
   wannierc_tmp(2,iobtl)=ainv(2,1)*wfc(1,iobtl)+ainv(2,2)*wfc(2,iobtl)+ainv(2,3)*wfc(3,iobtl)   ! s = h^-1 r
   wannierc_tmp(3,iobtl)=ainv(3,1)*wfc(1,iobtl)+ainv(3,2)*wfc(2,iobtl)+ainv(3,3)*wfc(3,iobtl)   ! s = h^-1 r
   !
   wannierc_tmp(1,iobtl)=wannierc_tmp(1,iobtl)-FLOOR(wannierc_tmp(1,iobtl))   ! impose PBC on s in range: [0,1)
   wannierc_tmp(2,iobtl)=wannierc_tmp(2,iobtl)-FLOOR(wannierc_tmp(2,iobtl))   ! impose PBC on s in range: [0,1)
   wannierc_tmp(3,iobtl)=wannierc_tmp(3,iobtl)-FLOOR(wannierc_tmp(3,iobtl))   ! impose PBC on s in range: [0,1)
   !
   wannierc(1,iobtl)=h(1,1)*wannierc_tmp(1,iobtl)+h(1,2)*wannierc_tmp(2,iobtl)+h(1,3)*wannierc_tmp(3,iobtl)   ! r = h s
   wannierc(2,iobtl)=h(2,1)*wannierc_tmp(1,iobtl)+h(2,2)*wannierc_tmp(2,iobtl)+h(2,3)*wannierc_tmp(3,iobtl)   ! r = h s
   wannierc(3,iobtl)=h(3,1)*wannierc_tmp(1,iobtl)+h(3,2)*wannierc_tmp(2,iobtl)+h(3,3)*wannierc_tmp(3,iobtl)   ! r = h s
   !
   wannierc_tmp(:,iobtl)=wannierc(:,iobtl) ! keep a temporary copy to compute pair indices
   !
 END DO
 ! 
 ! make copy of wannier centres when number of processors > number of bands
 ! 
 IF(nproc_image.GT.nbsp) THEN
   !
   DO iobtl=nbsp+1,nproc_image
     !
     ir=MOD(iobtl,nbsp)
     IF(ir.EQ.0 )THEN
       wannierc(:,iobtl)=wannierc(:,nbsp)
     ELSE
       wannierc(:,iobtl)=wannierc(:,ir)
     END IF
     !
   END DO
   !
 END IF
 ! 
 ALLOCATE (overlap(neigh/2,ndim)); overlap=0
 ALLOCATE (njj(ndim)); njj=0
 !
 call exx_index_pair(wannierc_tmp, overlap, njj, nj_max, ndim )
 !
 IF (ALLOCATED(wannierc_tmp))            DEALLOCATE(wannierc_tmp)
 !
 call stop_clock('exx_pairs')
 !
 !-------------------------------------------------------------------------
 !
 ! Allocate variables to store potentials for 3 steps ....
 !
 IF (n_exx.EQ.0) THEN
   !
   IF ( lwfpbe0 ) THEN
       ALLOCATE( selfv( np_in_sp_s, 3,   my_nbspx), stat=ierr ); selfv=0.0_DP
       ALLOCATE( pair_dist( 3, nj_max*2, my_nbspx), stat=ierr ); pair_dist=0.0_DP
       ALLOCATE( pairv( np_in_sp_p, 3, nj_max*2, my_nbspx), stat=ierr ); pairv=0.0_DP
   END IF
   !
 END IF
 !
 !-------------------------------------------------------------------------
 !
 ! update exx step ...
 !
 n_exx = n_exx + 1
 !
 !=========================================================================
 !
 !obtain orbitals on each local processor, stored in psi
 !
 allocate ( psi(  nnrtot, my_nbsp(me ) ) ); psi=0.0_DP
 allocate ( vpsil(nnrtot, my_nbsp(me ) ) ); vpsil=0.0_DP
 !
 call start_clock('r_orbital')
 !
 call exx_psi(c, psi, nnrtot, my_nbsp, my_nxyz, nbsp) 
 !
 call stop_clock('r_orbital')
 !
 !===============================================================================
 !
 !           PAIR AND SELF POTENTIALS ARE CALCULATED IN ONE BIG LOOP 
 !
 !===============================================================================

 !========================================================================
 !                      THE MOST OUTER LOOP STARTS:
 !========================================================================

 allocate( irecvreq(nj_max,0:nproc_image-1) )

 ! obtain psi in sphere for neighbors
 ALLOCATE ( psil_pair_send(np_in_sp_me_p, nj_max, my_nbsp(me )) ); psil_pair_send=0.0_DP
 ALLOCATE ( psil_pair_recv(np_in_sp_me_p, nj_max) ); psil_pair_recv=0.0_DP
 !
 ! initialize totalenergy 
 totalenergy = 0.0_DP
 !
 DO iobtl = 1, my_nbsp(me) ! BS
    ! 
    call start_clock('send_psi')
    ! 
    psil_pair_recv(:,:)=0.0_DP
    !
    !call start_clock('send_psi_barrier')
    !call mp_barrier( intra_image_comm )
    !call stop_clock('send_psi_barrier')
    !
    !========================================================================
    !
    DO j = 1, nj_max
       !
       DO irank = 1, nproc_image
          !
          gindex_of_iobtl =  index_my_nbsp(iobtl, irank)
          !
          IF(nproc_image .LE. nbsp) my_var=nbsp ! BS
          IF(nproc_image .GT. nbsp) my_var=nproc_image ! BS
          IF( gindex_of_iobtl .LE. my_var)then ! BS
            !
            rk_of_obtl_trcv = irank - 1
            obtl_tbs=overlap(j, gindex_of_iobtl) ! BS
            ! 
            IF(obtl_tbs .NE. 0)THEN
               !
               rk_of_obtl_tbs  = rk_of_obtl(obtl_tbs)
               lindex_obtl_tbs = lindex_of_obtl(obtl_tbs)
               ! 
               IF( (me_image .EQ. rk_of_obtl_trcv) .AND. (me_image .eq. rk_of_obtl_tbs ))THEN
                  ! for nproc_image .LT. nbsp
                  ! need to be fixed ??
                  psil_pair_recv(:,j) = psil_pair_send(:, j, lindex_obtl_tbs) ! BS 
                  ! 
               ELSEIF( me_image .EQ. rk_of_obtl_tbs )THEN
                  !
                  ! calculate mid point of two wannier centers
                  call getmiddlewc(wannierc(1,gindex_of_iobtl),wannierc(1,obtl_tbs), &
                  &                centerx, centery, centerz, a(1), a(2), a(3), middle(1,j) )
                  ! HK: get pair distance
                  call get_pair_dist(wannierc(1,gindex_of_iobtl),wannierc(1,obtl_tbs),d_pair(j))
                  ! calculate translation vector from the center of the box 
                  call getsftv( nr1s, nr2s, nr3s, ha, hb, hc, middle(1, j), tran)
                  !
                  ! get the localized psi around the mid point of two wannier centers 
                  call getpsil( nnrtot, np_in_sp_me_p, psi(1, lindex_obtl_tbs), psil_pair_send(1,j,lindex_obtl_tbs), tran) 
                  !
                  CALL MPI_SEND( psil_pair_send(1, j, lindex_obtl_tbs), np_in_sp_me_p, MPI_DOUBLE_PRECISION, & 
                                 rk_of_obtl_trcv, j*irank, intra_image_comm,ierr ) ! BS
                  ! 
               ELSEIF( me_image .EQ. rk_of_obtl_trcv )THEN
                  ! 
                  CALL MPI_IRECV( psil_pair_recv(1,j),                  np_in_sp_me_p, MPI_DOUBLE_PRECISION, &
                                 rk_of_obtl_tbs,  j*irank, intra_image_comm, irecvreq(j,me_image),ierr) ! BS
                  !
               ENDIF
               ! 
            ENDIF
            ! 
          ENDIF
          !
       ENDDO  !irank
       !
    ENDDO  ! j
    !
    !=======================================================================
    call start_clock('send_psi_wait')
    !
    do j = 1, nj_max
       do irank = 1, nproc_image
          gindex_of_iobtl =  index_my_nbsp(iobtl, irank)
          ! 
          IF(nproc_image .LE. nbsp) my_var=nbsp ! BS
          IF(nproc_image .GT. nbsp) my_var=nproc_image ! BS
          ! 
          if( gindex_of_iobtl .LE. my_var)then ! BS
            rk_of_obtl_trcv = irank - 1
            ! 
            obtl_tbs=overlap(j, gindex_of_iobtl) ! BS
            ! 
            if(obtl_tbs .ne. 0)then
               rk_of_obtl_tbs  = rk_of_obtl(obtl_tbs)
               if( (me_image .eq. rk_of_obtl_trcv) .and. (me_image .ne. rk_of_obtl_tbs) )then
                 CALL MPI_WAIT(irecvreq(j,me_image), istatus, ierr)
               endif
            endif
          endif
          ! 
       enddo
    enddo
    !
    call stop_clock('send_psi_wait')
    call stop_clock('send_psi')
    !
    !=======================================================================
    ! after this loop ( do irank ), all the processor got all the overlapping orbitals 
    ! for the i_obtl orbital and ready to calculate pair potential 
    !=======================================================================
    !
    call start_clock('getpairv')
    middle(:,:)=0.0_DP
    gindex_of_iobtl = index_my_nbsp(iobtl, me)
    !
    IF(nproc_image .LE. nbsp) my_var=nbsp ! BS
    IF(nproc_image .GT. nbsp) my_var=nproc_image ! BS
    !
    if( gindex_of_iobtl .LE. my_var)then ! BS
      !
      !second loop starts: calculate overlapping potential with the j_th orbitals
      !
      !call start_clock('getpairv')
      !
      my_var3=njj( gindex_of_iobtl ) ! BS
      !
      do j = 1, my_var3 ! BS
         !
         my_var2=overlap(j,gindex_of_iobtl) ! BS
         !
         IF(my_var2 .NE. 0)THEN ! BS
            !
            call getmiddlewc(wannierc(1,gindex_of_iobtl),wannierc(1,my_var2), &
            &                centerx, centery, centerz, a(1), a(2), a(3), middle(1,j) )
            ! HK: get pair distance
            call get_pair_dist(wannierc(1,gindex_of_iobtl),wannierc(1,my_var2),d_pair(j))
            !
            call getsftv( nr1s, nr2s, nr3s, ha, hb, hc, middle(1, j), tran)
            !      
            ! get the localized psi 
            ALLOCATE ( psil(np_in_sp_me_p) ); psil=0.0_DP ! BS
            call getpsil( nnrtot, np_in_sp_me_p, psi(1, iobtl), psil(1), tran) 
            ! 
            ! BS: the localized density rhol 
            ALLOCATE ( rhol(np_in_sp_me_p) ); rhol=0.0_DP
            ALLOCATE ( rho_in_sp(np_in_sp_p) ); rho_in_sp=0.0_DP
            call getrhol( np_in_sp_me_p, np_in_sp_p, psil(1), psil_pair_recv(1, j), rhol, rho_in_sp, tran, sa1)
            ! 
            ! compute potential (vl) in ME sphere 
            ALLOCATE ( vl(np_in_sp_me_p) ); vl=0.0_DP
            call start_clock('getvofr')
            call getvofr( np_in_sp_me_p, np_in_sp_p,&
                hcub, rho_in_sp, vl, pairv(1,1,j,iobtl), pairv(1,2,j,iobtl),&
                pairv(1,3,j,iobtl), .FALSE., d_pair(j), pair_dist(1,j,iobtl), pair_dist(2,j,iobtl),&
                pair_dist(3,j,iobtl),cgstep)
            call stop_clock('getvofr')
            !WRITE(stdout,'("cgstep",3I6)')gindex_of_iobtl, my_var2, cgstep
            ! 
            ! update vpsil in the global grid (factor 0.25 is for PBE0) 
!$omp parallel do private(ir) 
            do ip = 1, np_in_sp_me_p
               call l2goff (ip,ir,tran)
               vpsil(ir,iobtl) = vpsil(ir,iobtl) - 0.25_DP*vl(ip)*psil_pair_recv(ip,j) ! to remain 
               psil_pair_recv(ip,j) =            - 0.25_DP*vl(ip)*psil(ip)             ! to be sent 
            end do
!$omp end parallel do
            !
            call vvprod(np_in_sp_me_p, rhol, vl, paire(j)) ! BS
            paire(j) = paire(j) * 0.5_DP * hcub
            totalenergy = totalenergy + 2.0_DP*paire(j)
            !
            IF (ALLOCATED(psil))            DEALLOCATE(psil)
            IF (ALLOCATED(rhol))            DEALLOCATE(rhol)
            IF (ALLOCATED(rho_in_sp))       DEALLOCATE(rho_in_sp)
            IF (ALLOCATED(vl))              DEALLOCATE(vl)
            !
         END IF
         !
      enddo !for j
      !
      !print *, 'pair energy  follows '
      !write(*,'(5f15.8)')(paire(j),j=1,njj( gindex_of_iobtl ))
      !
    endif !gindex_of_iobtl <= nbsp
    !
    !
    !call stop_clock('getpairv')
    !
    !
    !=========================================================================
    !                                
    !              SELF POTENTIAL FOR EACH ORBITAL STARTS HERE
    !                                
    !=========================================================================
    !
    !call start_clock('self_v')
    !
    IF(me.GT.(nbsp*(sc_fac-1))) THEN
       !
       gindex_of_iobtl =  index_my_nbsp(iobtl, me)
       !
       call getsftv(nr1s, nr2s, nr3s, ha, hb, hc, wannierc(1, gindex_of_iobtl), tran)
       !
       ! get the localized psi 
       ALLOCATE ( psil(np_in_sp_me_s) ); psil=0.0_DP
       call getpsil( nnrtot, np_in_sp_me_s, psi(1, iobtl), psil(1), tran)
       !
       ! get the localized density rhol  
       ALLOCATE ( rhol(np_in_sp_me_s) ); rhol=0.0_DP
       ALLOCATE ( rho_in_sp(np_in_sp_s) ); rho_in_sp=0.0_DP
       call getrhol( np_in_sp_me_s, np_in_sp_s, psil(1), psil(1), rhol, rho_in_sp, tran, sa1)
       !
       ! compute potential (vl) in ME sphere 
       ALLOCATE ( vl(np_in_sp_me_s) ); vl=0.0_DP
       call start_clock('getvofr')
       call getvofr( np_in_sp_me_s,np_in_sp_s,&
            hcub, rho_in_sp, vl, selfv(1,1,iobtl), selfv(1,2,iobtl),&
            selfv(1,3,iobtl), .TRUE., 0.0, 0.0, 0.0, 0.0,cgstep)
       !
       call stop_clock('getvofr')
       !WRITE(stdout,'("cgstep",3I6)')gindex_of_iobtl, gindex_of_iobtl, cgstep
       !
       ! update vpsil in the global grid (factor 0.25 is for PBE0)
!$omp parallel do private(ir) 
       do ip = 1, np_in_sp_me_s 
          call l2goff (ip,ir,tran)
          vpsil(ir,iobtl) = vpsil(ir,iobtl) - 0.25_DP*vl(ip)*psil(ip) ! PBE0
       end do
!$omp end parallel do 
       !
       ! compute exchange energy in ME sphere 
       call vvprod(np_in_sp_me_s, rhol, vl, selfe)
       selfe = selfe * 0.5_DP * hcub
       !totalenergy = totalenergy + (selfe/DBLE(sc_fac))
       totalenergy = totalenergy + selfe
   
      !IF(me .GT. nbsp) THEN
      !  vpsil(:,iobtl) = 0.0_DP
      !END IF
   
       IF (ALLOCATED(psil))            DEALLOCATE(psil)
       IF (ALLOCATED(rhol))            DEALLOCATE(rhol)
       IF (ALLOCATED(rho_in_sp))       DEALLOCATE(rho_in_sp)
       IF (ALLOCATED(vl))              DEALLOCATE(vl)
       !
    END IF ! me
    !
    !!DEBUG
    !ALLOCATE(BS_selfe(nproc_image)); BS_selfe = 0.0d0 ! BS
    !CALL MPI_ALLGATHER( totalenergy, 1, MPI_DOUBLE_PRECISION, BS_selfe, 1, MPI_DOUBLE_PRECISION, intra_image_comm, IERR)
    !WRITE(stdout,'("total selfe mpi allgather",F30.16)') SUM(BS_selfe) 
    !IF(me_image .EQ. 0) THEN
    !  WRITE(stdout,'("BS_selfe")')
    !  DO i = 1,nproc_image
    !    !WRITE(stdout,'(I5,F30.16)')i,(BS_selfe(i)+BS_selfe(i+nbsp))
    !    WRITE(stdout,'(I5,F30.16)')i,BS_selfe(i)
    !  END DO
    !END IF
    !IF (ALLOCATED(BS_selfe))        DEALLOCATE(BS_selfe)
    !
    !totalenergyg=0.0d0
    !CALL MPI_ALLREDUCE(totalenergy, totalenergyg, 1, MPI_DOUBLE_PRECISION, &
    !&                        MPI_SUM, intra_image_comm, ierr)
    !WRITE(stdout,'("total selfe ",F30.14)')totalenergyg
    !!DEBUG
    !
    !call stop_clock('self_v')
    call stop_clock('getpairv')
    !
    !===============================================================================
    ! After this loop, each processor finished the pair potentials for the 
    ! iobtl orbital, and shall talk to send/recv vpsiforj
    !===============================================================================
    !
    call start_clock('sendv')
    !call start_clock('sendv_barrier') ! DEBUG
    !call mp_barrier(intra_image_comm) ! DEBUG
    !call stop_clock('sendv_barrier')  ! DEBUG
    !
    !
    middle(:,:)=0.d0
    jj = 0
    !
    do j = 1, nj_max
       do irank = 1, nproc_image

          gindex_of_iobtl = index_my_nbsp(iobtl, irank)
          !
          IF(nproc_image .LE. nbsp) my_var=nbsp ! BS
          IF(nproc_image .GT. nbsp) my_var=nproc_image ! BS
          !
          if( gindex_of_iobtl .LE. my_var)then ! BS
            !
            rk_of_obtl_tbs = irank - 1
            !
            obtl_trcv=overlap(j, gindex_of_iobtl) ! BS
            ! 
            if ( obtl_trcv .ne. 0)then
               !
               IF(nproc_image .LE. nbsp) THEN 
                 !
                 rk_of_obtl_trcv  = rk_of_obtl(obtl_trcv)
                 !
               ELSE
                 !
                 DO i=1,sc_fac-1
                   !
                   ib=nbsp*i; ic=nbsp*(i+1)
                   !
                   IF(me.LE.ib .AND. irank.LE.ib) THEN
                     rk_of_obtl_trcv  = rk_of_obtl(obtl_trcv)
                   ELSEIF(me.GT.ib .AND. irank.GT.ib .AND. me.LE.ic .AND. irank.LE.ic) THEN
                     rk_of_obtl_trcv = rk_of_obtl(obtl_trcv) + ib 
                   ELSE
                     rk_of_obtl_trcv  = nproc_image+1
                   END IF
                   !
                 END DO
                 !
               END IF
               !
               lindex_obtl_trcv = lindex_of_obtl(obtl_trcv)
               !
               if( (me_image .eq. rk_of_obtl_trcv) .and. (me_image .eq. rk_of_obtl_tbs ))then
                  ! for nproc_image .LT. nbsp
                  ! BS : need to be fixed ??
                  ! upadate vpsil PBE0 
                  call getmiddlewc(wannierc(1,gindex_of_iobtl),wannierc(1,rk_of_obtl_trcv+1), &
                  &                centerx, centery, centerz, a(1), a(2), a(3), middle(1,j) )
                  ! HK: get pair distance
                  call get_pair_dist(wannierc(1,gindex_of_iobtl),wannierc(1,rk_of_obtl_trcv+1),d_pair(j))
                  !
                  call getsftv( nr1s, nr2s, nr3s, ha, hb, hc, middle(1, j), tran)
!$omp parallel do private(ir) 
                  do ip = 1, np_in_sp_me_p
                     call l2goff (ip,ir,tran)
                     vpsil(ir,lindex_obtl_trcv) = vpsil(ir,lindex_obtl_trcv) + psil_pair_recv(ip,j)
                  end do
!$omp end parallel do
                  !
               elseif( me_image .eq. rk_of_obtl_tbs ) then
                  ! 
                  CALL MPI_SEND( psil_pair_recv(1,j), np_in_sp_me_p, MPI_DOUBLE_PRECISION, &
                               rk_of_obtl_trcv, j*irank, intra_image_comm,ierr) ! BS 
                  ! 
               elseif( me_image .eq. rk_of_obtl_trcv)then
                  ! 
                  !jj = jj + 1
                  call mpi_recv( psil_pair_send(1, j, lindex_obtl_trcv), np_in_sp_me_p, mpi_double_precision, &
                              rk_of_obtl_tbs,  j*irank, intra_image_comm, istatus,ierr) ! bs
                  !WRITE(stdout,'("recv",3I6)')me,gindex_of_iobtl,j*irank ! DEBUG
          
                  !
                  call getmiddlewc(wannierc(1,gindex_of_iobtl),wannierc(1,rk_of_obtl_trcv+1), &
                  &                centerx, centery, centerz, a(1), a(2), a(3), middle(1,j) )
                  ! HK: get pair distance
                  call get_pair_dist(wannierc(1,gindex_of_iobtl),wannierc(1,rk_of_obtl_trcv+1),d_pair(j))
                  !
                  call getsftv( nr1s, nr2s, nr3s, ha, hb, hc, middle(1, j), tran)
                  ! upadate vpsil PBE0 
!$omp parallel do private(ir) 
                  do ip = 1, np_in_sp_me_p
                     call l2goff (ip,ir,tran)
                     vpsil(ir,lindex_obtl_trcv) = vpsil(ir,lindex_obtl_trcv) + psil_pair_send(ip,j,lindex_obtl_trcv) 
                  end do
!$omp end parallel do
                  !
               endif
            endif
            ! 
          endif  ! gindex_of_iobtl <= nbsp
  
       end do ! irank  
    end do  ! j
    !
    call stop_clock('sendv')
    !
 end do ! iobtl

 
 !============================================================================
          !THE MOST OUTER LOOP FOR PAIR POTENTIAL ENDS HERE
 !============================================================================
 
 
 !============================================================================
 call start_clock('totalenergy')
 totalenergyg=0.0d0
 exx=0.0_DP
 CALL MPI_ALLREDUCE(totalenergy, totalenergyg, 1, MPI_DOUBLE_PRECISION, &
 &                        MPI_SUM, intra_image_comm, ierr)

 exx = totalenergyg
 if(nspin .eq. 1)exx = exx + totalenergyg
!WRITE(stdout, '("EXX Energy",F30.14," step",I7)')exx,nfi  ! BS
 call stop_clock('totalenergy')


 ! Local to global distribution of EXX potential
 ! vpsil (local) --> exx_potential (global)
 !
 call start_clock('vl2vg')
 exx_potential=0.0d0
 !
 IF(nproc_image .LE. nbsp) THEN 
   !
   call redistwfr ( exx_potential, vpsil, my_nxyz, my_nbsp, intra_image_comm, -1 )
   !
 ELSE
   !
   !-----------Zhaofeng's vpsil (local) to exx_potential (global) -----------
   !
   nogrp = dffts%nogrp
   !
   ALLOCATE( sdispls(nproc_image), sendcount(nproc_image) ); sdispls=0; sendcount=0
   ALLOCATE( rdispls(nproc_image), recvcount(nproc_image) ); rdispls=0; recvcount=0 
   ALLOCATE( sdispls1(nogrp), sendcount1(nogrp) ); sdispls1=0; sendcount1=0
   ALLOCATE( rdispls1(nogrp), recvcount1(nogrp) ); rdispls1=0; recvcount1=0
   !
   do proc = 1, nproc_image
     !
     if(me <= nogrp*nr3s)then
       sendcount(proc) =nr1s*nr2s/nogrp
     else
       sendcount(proc) = 0
     end if
     !proc 1 holds  the nr1s*nr2s/nogrp information of 1024 orbital(duplicate)
     !proc 640 as well
     !however, 641 to 1024 idle
     if(proc <= nogrp*nr3s) then
       recvcount(proc)=nr1s*nr2s/nogrp
     else
       recvcount(proc)=0
     end if
     !
   end do
   !
   sdispls(1) = 0
   rdispls(1) = 0
   !
   do proc = 2,  nproc_image
     sdispls(proc)=  sdispls(proc-1) + sendcount(proc-1)
     rdispls(proc) = rdispls(proc-1) + recvcount(proc-1)
   end do
   !
   allocate(exx_tmp (dffts%nnr,nproc_image/nogrp)); exx_tmp=0.0_DP
   allocate(exx_tmp3(dffts%nnr,nproc_image/nogrp)); exx_tmp3=0.0_DP
   !
   call mp_barrier( intra_image_comm )
   call MPI_ALLTOALLV(vpsil(1,1), recvcount,rdispls,MPI_DOUBLE_PRECISION, &
        &           exx_tmp, sendcount,sdispls, MPI_DOUBLE_PRECISION, &
        &           intra_image_comm, ierr)
   !
   va = dffts%nnr/nogrp
   DO j=1,nproc_image/nogrp,2
     do i=1,2*nogrp
        ii=((i-1)/2)*va 
        jj=j+mod(i-1,2)
        ia=(i-1-((i-1)/nogrp)*nogrp)*va
        ib=j+(i-1)/nogrp
!$omp parallel do 
        do ir=1,va
          exx_tmp3(ii+ir,jj)=exx_tmp(ia+ir,ib)
        end do
!$omp end parallel do 
     end do
   END DO
   !
   do proc = 1 , nogrp
     sendcount1(proc) = dffts%nnr/nogrp
     recvcount1(proc) = dffts%nnr/nogrp
   end do
   !
   rdispls1(1) = 0
   sdispls1(1) = 0
   !
   do proc = 2, nogrp
      sdispls1(proc) = sdispls1(proc-1) + sendcount1(proc-1)
      rdispls1(proc) = rdispls1(proc-1) + recvcount1(proc-1)
   end do
   !
   do ir=1,nproc_image/nogrp
     call mp_barrier( dffts%ogrp_comm )
     call MPI_ALLTOALLV(exx_tmp3(1,ir), sendcount1, sdispls1, MPI_DOUBLE_PRECISION, &
          &         exx_potential(1,ir),recvcount1, rdispls1, MPI_DOUBLE_PRECISION, &
          &         dffts%ogrp_comm, ierr)
   end do
   !
   do ir=1,nbsp/nogrp
     do i=1,sc_fac-1
       ii=i*nbsp/nogrp
!$omp parallel do 
       do ia=1,dffts%nnr
         exx_potential(ia,ir)=exx_potential(ia,ir)+exx_potential(ia,ir+ii)
       end do
!$omp end parallel do 
     end do
   end do
   !
   !-----------Zhaofeng's vpsil (local) to exx_potential (global) -----------
   !
 END IF ! vl2vg
 !
 call stop_clock('vl2vg')
 !
 !==============================================================================
 IF (ALLOCATED(vpsil ))          DEALLOCATE( vpsil )
 IF (ALLOCATED(psi ))            DEALLOCATE( psi )
 IF (ALLOCATED(irecvreq ))       DEALLOCATE( irecvreq )

 IF (ALLOCATED(wannierc))        DEALLOCATE(wannierc)
 IF (ALLOCATED(overlap))         DEALLOCATE(overlap)
 IF (ALLOCATED(njj))             DEALLOCATE(njj)
 IF (ALLOCATED(psil_pair_send))  DEALLOCATE(psil_pair_send)
 IF (ALLOCATED(psil_pair_recv))  DEALLOCATE(psil_pair_recv)

 IF (ALLOCATED(exx_tmp))         DEALLOCATE(exx_tmp)
 IF (ALLOCATED(exx_tmp3))        DEALLOCATE(exx_tmp3)

 IF (ALLOCATED(sdispls))         DEALLOCATE(sdispls)
 IF (ALLOCATED(rdispls))         DEALLOCATE(rdispls)
 IF (ALLOCATED(sdispls1))        DEALLOCATE(sdispls1)
 IF (ALLOCATED(rdispls1))        DEALLOCATE(rdispls1)
 IF (ALLOCATED(sendcount))       DEALLOCATE(sendcount)
 IF (ALLOCATED(recvcount))       DEALLOCATE(recvcount)
 IF (ALLOCATED(sendcount1))      DEALLOCATE(sendcount1)
 IF (ALLOCATED(recvcount1))      DEALLOCATE(recvcount1)

 !WRITE(*,'("leaving exx_gs")')

 return

 END SUBROUTINE exx_gs

 !====================================================================================
 !====================================================================================
 !====================================================================================

 !==============================================================================
 SUBROUTINE getsftv(nr1s, nr2s, nr3s, ha, hb, hc, wc, tran)
 USE kinds, ONLY  : DP
 IMPLICIT   NONE

 INTEGER  nr1s, nr2s, nr3s, tran(3)
 REAL(DP) wc(3), ha, hb, hc

 INTEGER  i, bcm(3), wcm(3)

 ! BS: this part only works for orthogonal cell .. FIX

 bcm(1) = INT( nr1s/2)
 wcm(1) = INT( wc(1)/ha  ) + 1

 bcm(2) = INT( nr2s/2)
 wcm(2) = INT( wc(2)/hb  ) + 1

 bcm(3) = INT(  nr3s/2 )
 wcm(3) = INT( wc(3)/hc  ) + 1

 DO i = 1, 3
    tran(i) = bcm(i) - wcm(i)
 ENDDO

 RETURN
 END
 !==============================================================================
 !==============================================================================
 SUBROUTINE getmiddlewc(wc1, wc2, cx, cy, cz, a, b, c, mid)
 USE kinds, ONLY  : DP
 IMPLICIT none

 real(DP)  wc1(3), wc2(3), mid(3)
 real(DP)  cx, cy, cz, a, b, c, tmp(3), center(3),acell(3)

 integer i

 ! BS: this part only works for orthogonal cell .. FIX

 center(1) = cx
 center(2) = cy
 center(3) = cz

 acell(1) = a
 acell(2) = b
 acell(3) = c
 
 do i = 1, 3
    mid(i) = wc1(i) + wc2(i)
    tmp(i) = wc1(i) - wc2(i)
    mid(i) = 0.5_DP*( mid(i) - INT(ABS(tmp(i))/center(i))*acell(i) )
 enddo

 RETURN
 END
 !==============================================================================
 ! HK: =========================================================================
 SUBROUTINE get_pair_dist (wc1, wc2, P_dist)
 USE kinds, ONLY  : DP
 USE cell_base,               ONLY  : omega, ainv, h
 IMPLICIT none
 ! HK : P_dist is the pair distance with in the minimum image convention
 real(DP)  wc1(3), wc2(3), P_dist
 real(DP)  dist_vec_in_r(3), dist_vec_in_s(3)
 !
 integer i
 !
 P_dist = 0.D0
 ! HK: dist vector
 DO i = 1,3
 dist_vec_in_r(i) = wc1(i) - wc2(i)
 END DO
 !
 dist_vec_in_s(1)=ainv(1,1)*dist_vec_in_r(1)+ainv(1,2)*dist_vec_in_r(2)+ainv(1,3)*dist_vec_in_r(3)   ! s = h^-1 r
 dist_vec_in_s(2)=ainv(2,1)*dist_vec_in_r(1)+ainv(2,2)*dist_vec_in_r(2)+ainv(2,3)*dist_vec_in_r(3)   ! s = h^-1 r
 dist_vec_in_s(3)=ainv(3,1)*dist_vec_in_r(1)+ainv(3,2)*dist_vec_in_r(2)+ainv(3,3)*dist_vec_in_r(3)   ! s = h^-1 r
 !
 DO i = 1,3
 dist_vec_in_s(i)=dist_vec_in_s(i)-NINT(dist_vec_in_s(i))   ! HK: minimum image
 END DO
 !
 dist_vec_in_r(1)=h(1,1)*dist_vec_in_s(1)+h(1,2)*dist_vec_in_s(2)+h(1,3)*dist_vec_in_s(3)   ! r = h s
 dist_vec_in_r(2)=h(2,1)*dist_vec_in_s(1)+h(2,2)*dist_vec_in_s(2)+h(2,3)*dist_vec_in_s(3)   ! r = h s
 dist_vec_in_r(3)=h(3,1)*dist_vec_in_s(1)+h(3,2)*dist_vec_in_s(2)+h(3,3)*dist_vec_in_s(3)   ! r = h s
 !
 do i = 1, 3
    P_dist = P_dist + dist_vec_in_r(i)*dist_vec_in_r(i)  
 enddo
 !
 P_dist = DSQRT(P_dist)
 !
 RETURN
 END
 !==============================================================================
 !==============================================================================
 SUBROUTINE vvprod(n, v1, v2, prod)
 USE kinds, ONLY  : DP
 IMPLICIT   NONE

 INTEGER  n
 REAL(DP) prod, v1(n), v2(n), vp

 INTEGER  i

 prod = 0.0_DP
 vp = 0.0_DP

!$omp parallel do reduction(+:vp)
 do i = 1, n
    vp = vp + v1(i) * v2(i)
 end do
!$omp end parallel do 

 prod = vp

 RETURN
 END
 !==============================================================================
 !==============================================================================
 SUBROUTINE getpsil( ntot, np_in_sp_me, psi, psi2, tran)

 USE kinds, ONLY  : DP

 IMPLICIT  NONE

 INTEGER  ntot, tran(3), np_in_sp_me
 REAL(DP) psi(ntot), psi2(np_in_sp_me)

 INTEGER  ir, ip, i, j, k, ii, jj, kk
  
!$omp parallel do private(ir) 
 DO ip = 1, np_in_sp_me 
     call l2goff (ip,ir,tran)
     psi2(ip) = psi(ir)
 END DO
!$omp end parallel do 

 RETURN
 END
 !==============================================================================
 !==============================================================================
 SUBROUTINE getrhol( np_in_sp_me, np_in_sp, psi, psi2, rho, rho_in_sp, tran, sa1)

 USE kinds, ONLY  : DP

 IMPLICIT   none

 INTEGER  np_in_sp_me, tran(3),np_in_sp
 REAL(DP) psi(np_in_sp_me), psi2(np_in_sp_me), rho(np_in_sp_me),sa1, rho_in_sp(np_in_sp)
     
 INTEGER  ir, ip, i, j, k, ii, jj, kk
 rho_in_sp(:) = 0.D0
 !
!$omp parallel do 
 DO ip = 1, np_in_sp_me 
    rho(ip) = psi(ip) * psi2(ip) * sa1
    IF( ip.LE.np_in_sp ) THEN
      rho_in_sp( ip ) = rho(ip)
    END IF
 ENDDO
!$omp end parallel do 
 !
 RETURN
 END
 !==============================================================================
 !==============================================================================
 SUBROUTINE l2goff (lind,gind,tran)

 USE exx_module,       ONLY  :  odtothd_in_sp, thdtood
 USE fft_base,         ONLY  : dfftp

 IMPLICIT   none

 INTEGER  tran(3),lind,gind

 INTEGER  ir, ip, i, j, k, ii, jj, kk, nr1s, nr2s, nr3s

 nr1s=dfftp%nr1; nr2s=dfftp%nr2; nr3s=dfftp%nr3 

 i  = odtothd_in_sp(1, lind)
 j  = odtothd_in_sp(2, lind)
 k  = odtothd_in_sp(3, lind)

 ii = i - tran(1)
 jj = j - tran(2)
 kk = k - tran(3)

 if( ii .gt. nr1s)ii = ii - nr1s
 if( jj .gt. nr2s)jj = jj - nr2s
 if( kk .gt. nr3s)kk = kk - nr3s

 if( ii .lt. 1)ii = ii + nr1s
 if( jj .lt. 1)jj = jj + nr2s
 if( kk .lt. 1)kk = kk + nr3s

 gind = thdtood(ii, jj, kk)

 RETURN
 END
 !==============================================================================
