!
! Copyright (C) 2002-2005 FPMD-CPV groups
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!


MODULE kohn_sham_states


   IMPLICIT NONE
   SAVE

   PRIVATE

   ! ...   print KS states to file KS.indx_ksout if ksout true
   LOGICAL :: tksout                        
   CHARACTER(LEN=2 ), PARAMETER :: ks_file       = 'KS'

   INTEGER, ALLOCATABLE :: indx_ksout(:,:)  ! (state inds, spin indxs)
   INTEGER, ALLOCATABLE :: n_ksout(:)       ! (spin indxs)

   PUBLIC :: ks_states_init, ks_states_closeup
   PUBLIC :: n_ksout, indx_ksout, tksout, print_all_states

!  ----------------------------------------------
CONTAINS
!  ----------------------------------------------


   SUBROUTINE ks_states_init( nspin, nprnks, iprnks )

      INTEGER, INTENT(IN) :: nspin, nprnks(:)
      INTEGER, INTENT(IN) :: iprnks(:,:)
   
      INTEGER :: i, ip, k, nstates

      ! ...   Tell the code which Kohn-Sham state should be printed to file
      !
      IF( ALLOCATED( n_ksout    ) ) DEALLOCATE( n_ksout )
      IF( ALLOCATED( indx_ksout ) ) DEALLOCATE( indx_ksout )
      !
      tksout = ANY( nprnks > 0 )
      !
      IF( tksout ) THEN
         nstates = MAXVAL( nprnks )
         ALLOCATE( n_ksout( nspin ) )
         ALLOCATE( indx_ksout( nstates, nspin) )
         n_ksout( 1:nspin ) = nprnks( 1:nspin )
         DO i = 1, nspin
           DO k = 1, nprnks( i )
              indx_ksout( k, i ) = iprnks( k, i )
           END DO
         END DO
      END IF

      RETURN
   END SUBROUTINE ks_states_init

!  ----------------------------------------------

   SUBROUTINE ks_states_closeup()
      IF( ALLOCATED( indx_ksout ) ) DEALLOCATE( indx_ksout )
      IF( ALLOCATED( n_ksout ) ) DEALLOCATE( n_ksout )
      tksout = .FALSE.
      RETURN
   END SUBROUTINE ks_states_closeup

!  ----------------------------------------------
!  ----------------------------------------------

      SUBROUTINE print_all_states(nfi, ctot, iupdwn_tot, nupdwn_tot )

        USE kinds,            ONLY : DP
        USE mp_global,        ONLY : intra_bgrp_comm
        USE io_global,        ONLY : ionode
        USE io_global,        ONLY : stdout
        USE io_files,         ONLY : outdir
        USE electrons_base,   ONLY : nupdwn, iupdwn, nspin, nbnd
        USE mp,               ONLY : mp_sum, mp_barrier, mp_set_displs, mp_gather
        USE mp_global,        ONLY : nproc_image, me_image, root_image, intra_image_comm

        IMPLICIT NONE

        ! ...   declare subroutine arguments
        COMPLEX(DP), INTENT(IN) :: ctot(:,:)
        INTEGER,     INTENT(IN) :: nfi
        INTEGER,     INTENT(IN) :: iupdwn_tot(2)
        INTEGER,     INTENT(IN) :: nupdwn_tot(2)

        ! ...   declare other variables
        INTEGER, allocatable    ::  proc_list(:)
        INTEGER                 ::  i, iss, iks, itot, maxnks, nproc

        CHARACTER(LEN=256) :: filename
        CHARACTER(LEN=10), DIMENSION(2) :: spin_name
        CHARACTER (LEN=6), EXTERNAL :: int_to_char

        IF( tksout ) THEN

          !Charles Swartz---------------------------------------------------------------
          IF (ionode) THEN
             WRITE( stdout,*) 
             WRITE( stdout,*) " ====================================================="
             WRITE( stdout,'( "   Printing Kohn Sham states: (NO MD STEPS)")') 
             WRITE( stdout,*) " ====================================================="
             write(stdout,*) 
             WRITE( stdout,'( "    nspin:", I4)') nspin
             DO i=1,nspin
               WRITE( stdout,'( "    ", I2, ") Num KS:", I4)') i, n_ksout(i)
             ENDDO
          write(stdout,*) 
          END IF
          !-----------------------------------------------------------------------------

          IF( nspin == 2 ) THEN
            spin_name(1) = '_UP_'
            spin_name(2) = '_DW_'
          ELSE
            spin_name(1) = '_'
            spin_name(2) = '_'
          END IF

          !Charles Swartz---------------------------------------------------------------
          !Calculate the proc_list (holds which me_image will sum and write the KS states)
          maxnks = maxval(n_ksout) 
          allocate(proc_list(maxnks))
          nproc = 0
          do i =1, maxnks
            proc_list(i) = nproc
            nproc = nproc + 1
            if (nproc == nproc_image) then
               !wrap back around
               nproc = 0
            endif
          enddo
          !-----------------------------------------------------------------------------

          DO iss = 1, nspin
            IF( tksout ) THEN
              DO i = 1, n_ksout(iss)
                iks = indx_ksout(i, iss)
                IF( ( iks > 0 ) .AND. ( iks <= nupdwn( iss ) ) ) THEN
                  itot = iks + iupdwn_tot(iss) - 1 
                  !Charles Swartz-------------------------------------------------------
                  filename =TRIM(outdir) // TRIM( ks_file ) // trim(spin_name(iss)) // &
                                                      &trim( int_to_char( iks ) )//trim('.dat')
                  CALL print_ks_state(nfi,  ctot( :, itot ), iss, i, filename, proc_list )
                  !---------------------------------------------------------------------
                END IF
              END DO
            END IF
          END DO

          !Charles Swartz---------------------------------------------------------------
          CALL mp_barrier( intra_image_comm )
          write(stdout,*) 
          WRITE( stdout,*) " ====================================================="
          !-----------------------------------------------------------------------------

        END IF

        RETURN
        ! ...
      END SUBROUTINE print_all_states

!Charles Swartz-------------------------------------------------------------------------
      Subroutine print_ks_state(nfi, c, ns, num, filename, proc_list)
         !
         USE kinds
         USE mp,                 ONLY : mp_sum, mp_barrier, mp_set_displs, mp_gather
         USE io_global,          ONLY : stdout, ionode, ionode_id
         USE io_global,          ONLY : stdout
         USE gvecw,              ONLY : ngw
         USE fft_base,           ONLY : dfftp, dffts
         USE fft_interfaces,     ONLY : invfft
         USE mp_global,          ONLY : nproc_image, me_image, root_image, intra_image_comm
         !
         implicit none
         !
         integer, intent(in)           :: nfi
         complex(dp), intent(in)       :: c(:)
         complex(dp), allocatable      :: psi(:), psitot(:)
         !
         real(dp),    allocatable      :: rpsi2(:)
         real(dp)                      :: charge
         !
         integer, intent(in)           :: ns, num, proc_list(:)
         integer                       :: i, ig, nproc, ntot, ksunit
         integer                       :: ngpwpp(nproc_image), displs(nproc_image)
         !
         character(len=*), intent(in)  :: filename
         character(len=5)              :: x1
         !
         !Open binary File
         ksunit = 1010 + me_image

         write(x1, '(I5)') num
         open(unit=ksunit,file=TRIM(filename),status='replace', form='unformatted')
         !WRITE(ksunit) nfi
         !
         !Set up Planes
         DO nproc=1,nproc_image,1
            ngpwpp(nproc)  = dfftp%npp(nproc)*dfftp%nr1*dfftp%nr2
         END DO
         !
         CALL mp_set_displs( ngpwpp, displs, ntot, nproc_image )
         !write(stdout, *) '!!Size of ntot, and psi', ntot
         !
         ! allocate the needed work spaces
         !
         ALLOCATE( psi( dffts%nnr ) )
         !write(stdout, *) '!!dffts%nnr', dffts%nnr
         !
         IF ( me_image == proc_list(num) ) THEN
            ALLOCATE( psitot( ntot ) )
            ALLOCATE( rpsi2 ( ntot ) )
         ELSE
            ALLOCATE( psitot( 1 ) )
            ALLOCATE( rpsi2 ( 1 ) )
         END IF
         !
                  !Convert c0 --> psi
         !write(stdout, *) '!!Size of c', size(c)
         !write(stdout, *) '!!Size of psi', size(psi)
         CALL c2psi( psi, dffts%nnr, c, c, ngw, 1 )
         !write(2000+num,*) 'Size c:', size(c)
         !do i = 1, ngw
         !   write(2000+num, *) c(i)
         !enddo
         !write(3000+num,*) 'Size psi:', size(psi)
         !do i = 1, ngw
         !   write(3000+num, *) psi(i)
         !enddo
         CALL invfft( 'Wave', psi, dffts )
         !
         !
         ! ... gather all psis arrays on the first node, in psitot
         !
         CALL mp_barrier( intra_image_comm )
         !
         CALL mp_gather( psi(:), psitot, ngpwpp, displs, proc_list(num), intra_image_comm )
         !
         !Charge Density from the Wavefunction
         IF( me_image == proc_list(num) ) THEN
            !
            !write(4000+num,*) 'Size psi:', size(psi)
            DO i = 1, ntot
               rpsi2( i ) = DBLE( psitot( i ) )**2
            !   write(4000+num,*) psi(i)
            END DO
            !
            charge = SUM( rpsi2 )
            !
            ! write the node-number-independent array
            !
            WRITE(ksunit) psitot(:)
            !
            WRITE(6,'(5X," ispin:", I4, 4X, "state:", I4,  " integrated charge : ",F14.5)')  &
                                 &ns, num, charge / DBLE(dfftp%nr1*dfftp%nr2*dfftp%nr3)
            !
         END IF
         !
         close(ksunit)
         DEALLOCATE( rpsi2, psi, psitot )
         ! ...
         RETURN
         ! ...
      End Subroutine print_ks_state

!---------------------------------------------------------------------------------------



!  ----------------------------------------------
!  ----------------------------------------------

      SUBROUTINE print_ks_states_old( c, file_name )

        USE kinds
        USE mp, ONLY: mp_sum
        USE io_global, ONLY: ionode, ionode_id
        USE io_global, ONLY: stdout
        USE gvecw, ONLY: ngw
        USE fft_base, ONLY: dfftp, dffts, dfftp
        USE fft_interfaces, ONLY: invfft
        USE xml_io_base, ONLY: write_rho_xml
        USE mp_global,       ONLY: intra_bgrp_comm, inter_bgrp_comm

        IMPLICIT NONE

        COMPLEX(DP),      INTENT(IN) :: c(:)
        CHARACTER(LEN=*), INTENT(IN) :: file_name
        REAL(DP),    ALLOCATABLE :: rpsi2(:)
        COMPLEX(DP), ALLOCATABLE :: psi(:)
        INTEGER   ::  i
        REAL(DP) :: charge

        ALLOCATE( psi( dfftp%nnr ) )
        ALLOCATE( rpsi2( dfftp%nnr ) )

        CALL c2psi( psi, dffts%nnr, c, c, ngw, 1 )
        CALL invfft( 'Wave', psi, dffts )

        DO i = 1, dfftp%nnr
           rpsi2( i ) = DBLE( psi( i ) )**2
        END DO
        charge = SUM( rpsi2 )

        CALL write_rho_xml( file_name, rpsi2, &
                            dfftp%nr1, dfftp%nr2, dfftp%nr3, dfftp%nr1x, dfftp%nr2x, dfftp%ipp, dfftp%npp, &
                            ionode, intra_bgrp_comm, inter_bgrp_comm )
        
        CALL mp_sum( charge, intra_bgrp_comm )

        IF ( ionode ) THEN
          WRITE( 6,'(3X,A15," integrated charge : ",F14.5)')  &
     &      TRIM(file_name), charge / DBLE(dfftp%nr1*dfftp%nr2*dfftp%nr3)
        END IF

        DEALLOCATE( rpsi2, psi )
        ! ...
        RETURN
        ! ...
      END SUBROUTINE print_ks_states_old

!  ----------------------------------------------
!
END MODULE kohn_sham_states
