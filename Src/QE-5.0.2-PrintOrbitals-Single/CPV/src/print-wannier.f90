MODULE print_wannier_functions
   !
   USE cp_main_variables,     ONLY : DP
   USE io_files,              ONLY : outdir
   !
   implicit none
   !
   character(len=4),parameter :: wan_file='WAN_'
   Contains
      !
      Subroutine print_all_wannier(c0)
         !
         USE electrons_base,    ONLY : nspin, nbsp, iupdwn, nupdwn
         USE mp_global,         ONLY : intra_image_comm
         USE io_global,         ONLY : ionode, stdout

         !
         implicit none
         !
         complex(DP), intent(in)       :: c0(:,:)
         complex(DP), allocatable      :: ctot(:,:)
         !
         integer                       :: nupdwn_tot( 2 ), iupdwn_tot( 2 )
         !
         !....declare other variables
         INTEGER ::  i, iss, iks, itot

         CHARACTER(LEN=10), DIMENSION(2) :: spin_name
         CHARACTER (LEN=6), EXTERNAL :: int_to_char
         !....
         !
         IF( nspin == 2 ) THEN
            Write(stdout, *) ' Printing of Wannier Functions not Tested with &
               & nspin different then 1. Skipping Wannier Function Printing'
         ELSE
            !
            nupdwn_tot = nupdwn 
            iupdwn_tot(1) = iupdwn(1)
            iupdwn_tot(2) = nupdwn(1) + 1
            !
            !
            ALLOCATE( ctot( SIZE( c0, 1 ), nupdwn_tot(1) * nspin ) )
            !
            ctot = c0
            !
            !
            IF (ionode) THEN
               WRITE( stdout,*) 
               WRITE( stdout,'( "    Wannier Centers")') 
               WRITE( stdout,'( "    nspin:", I4)') nspin
               DO i=1,nspin
               WRITE( stdout,'( "   ", I2, ") Num KS:", I4)') i, nbsp
               ENDDO
               WRITE( stdout,'( "   ---------------")') 
            END IF
       

            !TODO Make this selective like ksstates.f90 also look at restart.f90
            DO i = 1, nbsp
               CALL print_single_wannier( ctot( :, i ), &
                  TRIM(outdir) // '/' //TRIM(wan_file) // trim(int_to_char(i))//'.dat' )
            END DO
         ENDIF


         RETURN

      End Subroutine print_all_wannier
      !
      Subroutine print_single_wannier(c, file_name)
         !
         USE kinds
         USE mp,                 ONLY : mp_sum, mp_barrier, mp_set_displs, mp_gather
         USE io_global,          ONLY : stdout, ionode, ionode_id
         USE gvecw,              ONLY : ngw
         USE fft_base,           ONLY : dfftp, dffts
         USE fft_interfaces,     ONLY : invfft
         USE mp_global,          ONLY : nproc_image, me_image, root_image, intra_image_comm
         !
         implicit none
         !
         complex(dp), intent(in)       :: c(:)
         complex(dp), allocatable      :: psi(:), psitot(:)
         !
         real(dp),    allocatable      :: rpsi2(:)
         real(dp)                      :: charge
         !
         integer                       :: ounit=210
         integer                       :: i, ig, proc, ntot    
         integer                       :: ngpwpp(nproc_image), displs(nproc_image)
         !
         character (len=*), intent(in) :: file_name
         CHARACTER (LEN=6), EXTERNAL   :: int_to_char
         !
         !Open Wan binary output file
         If (me_image == root_image) &
            open(unit=ounit,file=TRIM(file_name),status='replace', form='unformatted')
         !
         DO proc=1,nproc_image,1
            !ngpwpp(proc)=(dfftp%nwl(proc)+1)/2
            ngpwpp(proc)  = dfftp%npp(proc)*dfftp%nr1*dfftp%nr2
         END DO
         !
         CALL mp_set_displs( ngpwpp, displs, ntot, nproc_image )
         !
         ! allocate the needed work spaces
         !
         ALLOCATE( psi( dffts%nnr ) )
         !
         IF ( me_image == root_image ) THEN
            ALLOCATE( psitot( ntot ) )
            ALLOCATE( rpsi2 ( ntot ) )
         ELSE
            ALLOCATE( psitot( 1 ) )
            ALLOCATE( rpsi2 ( 1 ) )
         END IF
         !
         !Convert c0 --> psi
         CALL c2psi( psi, dffts%nnr, c, c, ngw, 1 )
         CALL invfft( 'Wave', psi, dffts )
         !
         !
         ! ... gather all psis arrays on the first node, in psitot
         !
         CALL mp_barrier( intra_image_comm )
         !
         CALL mp_gather( psi(:), psitot, ngpwpp, displs, root_image, intra_image_comm )
         !
         !Charge Density from the Wavefunction
         IF( me_image == root_image ) THEN
            !
            DO i = 1, ntot
               rpsi2( i ) = DBLE( psitot( i ) )**2
            END DO
            !
            charge = SUM( rpsi2 )
            !
            ! write the node-number-independent array
            !
            WRITE(ounit) psitot
            close(ounit)
            !
         END IF
         !
         !CALL mp_sum( charge, intra_image_comm )
         !
         IF ( ionode ) THEN
            WRITE( stdout,'(3X,A60," integrated charge : ",F14.5)')  &
               &      TRIM(file_name), charge / DBLE(dfftp%nr1*dfftp%nr2*dfftp%nr3)
         END IF

         DEALLOCATE( rpsi2, psi, psitot )
         ! ...
         RETURN
         ! ...
      End Subroutine print_single_wannier

END MODULE print_wannier_functions
