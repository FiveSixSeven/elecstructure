
!==========================================================================
! 
!whether this algorithm works for every system needs tested -- Zhaofeng
!
!This subroutine computes unique pair list for each Wannier orbital
!
!==========================================================================

 SUBROUTINE exx_index_pair(wannierc, overlap_final, nj_final, nj_max, ndim )
 USE kinds,                   ONLY  : DP
 USE electrons_base,          ONLY  : nbsp, nspin, nupdwn, iupdwn
 USE mp_global,               ONLY  : me_image, nproc_image, intra_image_comm
 USE cell_base,               ONLY  : h,ainv 
 USE parallel_include
 USE mp,                      ONLY  : mp_barrier 
 USE wannier_base,            ONLY  : neigh, dis_cutoff
 USE io_global,               ONLY  : stdout
 IMPLICIT NONE
 
 REAl(DP),INTENT(IN)    :: wannierc(3, nbsp)
 INTEGER, INTENT(INOUT) :: nj_max, ndim
 INTEGER, INTENT(INOUT) :: overlap_final(neigh/2,ndim), nj_final(ndim) 
 
 REAl(DP), ALLOCATABLE  :: distance(:)
 INTEGER,  ALLOCATABLE  :: overlap(:, :),overlap2(:),overlap3(:,:),overlap4(:,:),nj(:),my_nj(:)
 INTEGER                :: i, j, k,jj, ip, ir, ierr, num, num1, iobtl,my_nj_proc,sc_fac
 REAl(DP)               :: dij(3),dij2(3)

 INTEGER   gindx_of_i, jj_neib_of_i
 INTEGER   spin_loop, nupdwn_(2),iupdwn_(2)

 !==================================================================
 !
 ALLOCATE( distance(nbsp) ); distance=0.0_DP
 ALLOCATE( overlap(neigh, nbsp)); overlap=0
 ALLOCATE( overlap2(neigh) ); overlap2=0
 ALLOCATE( nj(nbsp)); nj=0
 !
 if(nspin == 1)then
    nupdwn_(1)=nbsp
    nupdwn_(2)=nbsp
    iupdwn_(1)=1
    iupdwn_(2)=1
    spin_loop=1
 else
    nupdwn_(:)=nupdwn(:)
    iupdwn_(:)=iupdwn(:)
 end if
 !
 do i = 1, nbsp
   !
   nj(i) = 0
   distance(:) = 0.0_DP
   !
   if( i < iupdwn(2) ) then
       spin_loop = 1
   else
       spin_loop = 2
   end if
   !
   do j = iupdwn_(spin_loop), iupdwn_(spin_loop) + nupdwn_(spin_loop)-1
     !
     if( j .NE. i )then
       !
       ! Compute distance between wfc i and wfc j (according to the minimum image convention)...
       !
       dij(1)=wannierc(1,i)-wannierc(1,j)   ! r_ij = r_i - r_j   
       dij(2)=wannierc(2,i)-wannierc(2,j)   ! r_ij = r_i - r_j   
       dij(3)=wannierc(3,i)-wannierc(3,j)   ! r_ij = r_i - r_j   
       !
       dij2(1)=ainv(1,1)*dij(1)+ainv(1,2)*dij(2)+ainv(1,3)*dij(3)   ! s_ij = h^-1 r_ij
       dij2(2)=ainv(2,1)*dij(1)+ainv(2,2)*dij(2)+ainv(2,3)*dij(3)   ! s_ij = h^-1 r_ij
       dij2(3)=ainv(3,1)*dij(1)+ainv(3,2)*dij(2)+ainv(3,3)*dij(3)   ! s_ij = h^-1 r_ij
       !
       dij2(1)=dij2(1)-IDNINT(dij2(1))   ! impose MIC on s_ij in range: [-0.5,+0.5]
       dij2(2)=dij2(2)-IDNINT(dij2(2))   ! impose MIC on s_ij in range: [-0.5,+0.5]
       dij2(3)=dij2(3)-IDNINT(dij2(3))   ! impose MIC on s_ij in range: [-0.5,+0.5]
       !
       dij(1)=h(1,1)*dij2(1)+h(1,2)*dij2(2)+h(1,3)*dij2(3)   ! r_ij = h s_ij (MIC)
       dij(2)=h(2,1)*dij2(1)+h(2,2)*dij2(2)+h(2,3)*dij2(3)   ! r_ij = h s_ij (MIC)
       dij(3)=h(3,1)*dij2(1)+h(3,2)*dij2(2)+h(3,3)*dij2(3)   ! r_ij = h s_ij (MIC)
       !
       distance(j)=DSQRT(dij(1)*dij(1)+dij(2)*dij(2)+dij(3)*dij(3))   ! |r_i - r_j| (MIC)
       !
       if( distance(j) .lt.  dis_cutoff )then
         !
         nj(i) = nj(i) + 1
         !
         if(nj(i) .eq. 1 )then
           !
           overlap(nj(i),i) = j
           !
         else if(distance(overlap(nj(i)-1,i)) .le. distance(j))then
           !
           overlap(nj(i),i)=j
           !
         else
           !
           overlap2(:)=0
           do ir=1,nj(i)-1
             !
             if(distance(overlap(ir,i)) < distance(j))then
                overlap2(ir)=overlap(ir,i)
             else
                overlap2(ir)=j
                do ip=ir+1,nj(i)
                   overlap2(ip)=overlap(ip-1,i)
                end do
                GO TO 555
             end if
             !
           end do
           !
  555      continue
           !
           do ir = 1, nj(i)
              overlap(ir,i)=overlap2(ir)
           enddo
           !
         end if
         !
       end  if !if for distance(j)<5.0d0
       !
     end if ! j=/i
     !
   end do  ! j=1,nbsp
   !
   if(nj(i) > neigh)then
      print *, 'increase neigh, stop in exx_pair', nj(i),neigh
      return
   endif
   !
 end do !i = 1, nbsp
 !
 IF (ALLOCATED(distance))         DEALLOCATE(distance)
 !
 ! Now form unique wfc pair overlap matrix...
 !
 ALLOCATE( overlap3(neigh/2, nbsp)); overlap3=0
 !
 num=0; num1=0
 !
 do j=1, neigh/2
   !
   do i=1, nbsp
     !
     do jj=1, nj(i)
       !
       jj_neib_of_i = overlap(jj,i)
       !
       if(jj_neib_of_i .gt. 0)then
         !
         overlap3(j, i) = jj_neib_of_i
         overlap(jj,i) = 0
         num = num + 1
         do k = 1, nj(jj_neib_of_i)
           if(overlap(k,jj_neib_of_i) .eq. i)then
             overlap(k,jj_neib_of_i) = 0
             num1 = num1 + 1
             go to   666
           end if
         end do
         !
       end if
       !
     end do !jj
     !
 666 continue
     !
   end do !i
   !
 end do  !j
 !
 if(num .ne. num1)call errore("exx", "sth wrong with overlap",1)
 !
 do i = 1,nbsp
   num = 0
   do j = 1,neigh/2
     if(overlap3(j,i) .ne. 0)num = num+1
   end do
   nj(i) = num
 end do
 !
 nj_max=MAXVAL(nj)
 !
 IF (ALLOCATED(overlap))          DEALLOCATE(overlap)
 IF (ALLOCATED(overlap2))         DEALLOCATE(overlap2)
 !
 ! BS / RAD : for using more processors than number of bands a new overlap matrix, overlap4, is made
 !  overlap4 contains indices of distributed orbital pairs when number of processors are larger than number of bands (nbsp)  
 !  an example, if orbital 1 is paired with 15 orbitals with following indices 
 !  12   72   94   17  149   87  183  220   11  180   83  223  115  154   92
 !  then the pairs are redistributed, 8 orbitals in proc 1 and 7 orbitals in proc (1+nbsp) 
 !  proc 1      :       12   72   94   17  149   87  183  220 
 !  proc 1+nbsp :       11  180   83  223  115  154   92
 !
 !  likewise, this can be further distributed among larger number of processors
 !  at present nproc_image has to be integer multiple of nbsp 
 !
 IF(nproc_image.GT.nbsp) THEN
   !
   ALLOCATE(my_nj(nproc_image)); my_nj=0
   ALLOCATE(overlap4(nj_max,nproc_image)); overlap4=0
   !
   sc_fac = nproc_image/nbsp
   !
   DO i = 1, nbsp
     !
     IF( MOD(nj(i), sc_fac) .NE. 0) THEN
       my_nj_proc = nj(i)/sc_fac + 1
     ELSE
       my_nj_proc = nj(i)/sc_fac
     END IF
     !
     jj=0; iobtl=i
     !
     DO j = 1, nj(i)
       jj = jj+1
       IF(jj .GT. my_nj_proc)THEN
         jj=1; iobtl=iobtl+nbsp
       END IF
       overlap4(jj,iobtl)=overlap3(j,i)
       my_nj(iobtl)=my_nj(iobtl)+1
     END DO
     !
   END DO
   !
   ! Update final overlap matrix and neighbor list 
   !
   overlap_final=overlap4
   nj_final=my_nj
   nj_max=MAXVAL(my_nj)
   !
   ELSE
   !
   ! Update final overlap matrix and neighbor list 
   !
   overlap_final=overlap3
   nj_final=nj
   nj_max=MAXVAL(nj)
   !
 END IF
 !
 IF (ALLOCATED(overlap3))         DEALLOCATE(overlap3)
 IF (ALLOCATED(overlap4))         DEALLOCATE(overlap4)
 IF (ALLOCATED(nj))               DEALLOCATE(nj)
 IF (ALLOCATED(my_nj))            DEALLOCATE(my_nj)
 !
 WRITE(stdout,'("nj_max nj_min nj_avg :",2I6,F6.2)')nj_max,MINVAL(nj_final),DBLE(SUM(nj_final))/DBLE(ndim)
 !
 IF (me_image .eq. 0) THEN
   open(unit=20,file='pair.dat',status='unknown',form='formatted')
   do i = 1, ndim
      write(20, '(I5," :",I5," :",30I5)')i,nj_final(i),(overlap_final(j,i),j=1, nj_final(i))
   enddo
   close(20)
 END IF
 !
 !DEBUG: print matrix overlap_final
 !DO i = 1, ndim 
 !  WRITE(stdout,'(I5," :",I5,":",16I5)')i,nj_final(i),(overlap_final(k,i),k=1,nj_final(i))
 !END DO
 !
 RETURN
 !
 END SUBROUTINE exx_index_pair


!====================================================================
!  This subroutine finds for each state the valence-state neighbors 
!  Lingzhu Kong
!====================================================================
      SUBROUTINE exx_index_pair_nv(wc, overlap, nj, nj_max)
      USE kinds,                 ONLY  : DP
      USE electrons_base,        ONLY  : nbsp
      USE exx_module,            ONLY  : vwc
      USE mp_global,             ONLY  : me_image, intra_image_comm, nproc_image
      USE cell_base,             ONLY  : h 
      USE parallel_include
      USE wannier_base,          ONLY  : neigh, dis_cutoff, vnbsp
      IMPLICIT NONE
      
! wc is the wannier centers of the initial quasi-particle states
      REAl(DP),INTENT(IN)    ::    wc(3, nbsp)
! number and index of overlapping orbitals for all conduction states
      INTEGER, INTENT(INOUT) ::    overlap(neigh,nbsp), nj(nbsp) , nj_max
      
      INTEGER     i, j, ierr
      REAl(DP)    centerx, centery, centerz, xi, yi, zi, xj, yj, zj, xij, yij, zij, distance

!==================================================================


      print *, 'entering exx_index_pair_nv', dis_cutoff, neigh, vnbsp
      centerx = 0.5 * h(1,1)
      centery = 0.5 * h(2,2)
      centerz = 0.5 * h(3,3)

      overlap(:,:) = 0

      do i = 1, nbsp

         nj(i) = 0
         xi = wc( 1, i )
         yi = wc( 2, i )
         zi = wc( 3, i )

         do j = 1, vnbsp
            xj = vwc(1,j)
            yj = vwc(2,j)
            zj = vwc(3,j)
            xij = xj - xi - INT( (xj-xi)/centerx )*h(1,1) 
            yij = yj - yi - INT( (yj-yi)/centery )*h(2,2)  
            zij = zj - zi - INT( (zj-zi)/centerz )*h(3,3) 
            distance = sqrt( xij*xij + yij*yij + zij*zij)

            if( distance .lt. dis_cutoff )then
                nj(i) = nj(i) + 1
                if(nj(i) > neigh)then
                   print *, 'increase neigh, stop in exx_pair', nj(i), neigh
                   return
                endif
                overlap(nj(i),i) = j
            end if
         enddo  ! j=1,vnbsp
      enddo  

      nj_max = nj(1)
      do i = 2, nbsp
         if(nj(i) > nj_max) nj_max = nj(i)
      enddo

      print *, 'leave exx_index_pair_nv', nj
      return

      END
!==================================================================
